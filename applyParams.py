#!/usr/bin/env python
from xml.etree import ElementTree as ET
import sys

inputFactors = {}
def main():
	# open input file with base XML parameters
	inFile = open("datain/explorationHohenbergBase.xml")
	# open output file that will contain parameter values
	outFile = open("datain/explorationHohenberg.xml","w")
	text = inFile.read()
	#open file with parameter values
	inParam = open("simParametersInput.txt")
	val = inParam.read().split("\n")
	
	text = text.replace("[ageMinHavingChild]", val[0])
	text = text.replace("[ageMaxHavingChild]", val[1])
	text = text.replace("[nbChild]", val[2])
	text = text.replace("[probabilityToMakeCouple]", val[3])
	text = text.replace("[nbJoinTrials]", val[4])
	text = text.replace("[splittingProba]", val[5])
	text = text.replace("[probToAcceptNewResidence]", val[6])
	text = text.replace("[probToAcceptNewResidence]", val[7])
	text = text.replace("[resSatisfactMargin]", val[7])
	text = text.replace("[jobVacancyRate]",val[8])
	outFile.write(text)
	outFile.close()
	
	# open jobvacancyweight file
	outFile = open("datain/hohenberg/JobVacancyWeights.csv","w")
	outFile.write("# Calibration weigths for the new job vacancies created yearly. These weights help decide the type of job that will be created\n")
	
	outFile.write(",".join(val[9:]))
	outFile.write("\n")
	
if __name__ == "__main__":
	main()
	
	