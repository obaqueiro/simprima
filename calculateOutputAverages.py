#
# This program calculates the average value of the output data obtained from the
# Simulation for the Altmark.
# 
# It looks for a list of files matching a defined criteria in the current directory and 
# groups them by year. Then it reads through the file and gets the average of each cell.
#
# For each cell, the average is calculated by dividing each cell value by the total number of files and 
# adding up each result to a grand value. 
#
# It is important that the name of the files are of the form:
# X-YYYY-ZZZZZZZZZ.csv 
# Where X is the run number, YYYY is the Year of the data in the file and ZZZZZZ is the name of the data.
#
import os

# The text to look for the group of the file names. Each of this correspods to a specific type of data output
fileNames = ["PopulationAgeDistribution", "BirthsDeathsCommuting","HouseholdStructure","Employment","SectorOfActivity", "WorkingPlace", "EconomicStatus"]
	
def isNumber(s):
	try:
		if len(s)==0:
			return False
		float(s)
		return True
	except ValueError:
		return False
	
try:
	os.chdir("dataOut\\HohenbergExploration")
	for fName in fileNames:
		#Obtain all the files in the current folder matching the text string
		
		files = os.listdir(".")
		
		files = [file for file in files if fName in file]
		
		#Group files by years
		groupedFiles = {}
		
		year = 2010
		while (True):
			group = [x for x in files if  str(year) in x]
			if len(group) == 0:
				break;
			groupedFiles[year] = group
			year = year + 1
		if len(groupedFiles)==0:
			continue
		if not os.path.exists("average"):
			os.makedirs("average")
		if not os.path.exists("stdev"):
			os.makedirs("stdev")
		#if not os.path.exists("min"):
		#	os.makedirs("min")
		#if not os.path.exists("max"):
		#	os.makedirs("max")
		# calculate average of data for each group and save in file
		for k,v in groupedFiles.items():
			contents = []
			stdContents=[]
			#maxContents=[]
			#minContents=[]
			#current row number being read from file
			
			# create output file
			outFile = open("-".join(["average\\average",str(k),fName,".csv"]),"w")
			outFileStd = open("-".join(["stdev\\stdev",str(k),fName,".csv"]),"w")
			#outFileMax = open("-".join(["max\\max",str(k),fName,".csv"]),"w")
			#outFileMin = open("-".join(["min\\min",str(k),fName,".csv"]),"w")
			# iterate through input files and get average
			denominator = len(v)
			n=0
			mean=0
			M2=0
			for inFileName in v:
				n=n+1
				#open file for reading
				inFile = open (inFileName)
				line = inFile.readline().strip()
				# if contents list is empty then we create the table
				if len(contents)==0:
					while len(line)!=0:
						row = []
						rowStd=[]
						#rowMax=[]
						#rowMin=[]
						if line[0]=="#":
							row.append(line)
							rowStd.append(line)
							#rowMin.append(line)
							#rowMax.append(line)
						else:
							line = line.split(",")
							for item in line:
								#rowMax.append(item)
								#rowMin.append(item)
								if isNumber(item):
									mean = 0
									M2=0
									x = float(item)
									delta = x - mean
									mean = mean + delta/n
									M2 = M2 + delta*(x-mean)
									rowStd.append(str(M2))
									row.append(str(mean))
								else:
									row.append(item)
									rowStd.append(item)
						line = inFile.readline().strip()
						contents.append(row)
						stdContents.append(rowStd)
						#maxContents.append(rowMax)
						#minContents.append(rowMin)
				else:
					rowNumber=0 
					while  len(line)!=0:
						if line[0]!="#":
							line = line.split(",")
							row = contents[rowNumber]
							rowStd = stdContents[rowNumber]
							#rowMax = maxContents[rowNumber]
							#rowMin = minContents[rowNumber]
							# add values to the array
							for index,item in enumerate(line):
								if isNumber(item) and isNumber(row[index]):
									x = float(item)
									mean = float(row[index])
									delta = x - mean
									mean = mean + delta/n
									rowStd[index] = str(float(rowStd[index]) + delta*(x-mean))
									row[index] = str(mean)
									
									#if float(rowMax[index]) < float(item):
									#	rowMax[index]=item
									#if float(rowMin[index]) > float(item):
									#	rowMin[index]=item
						line= inFile.readline().strip()
						rowNumber=rowNumber+1
			# write output
			for row in contents:
				outFile.write(str(k)+",")
				outFile.write(",".join(row))
				outFile.write("\n")
			#for row in maxContents:
			#	outFileMax.write(str(k)+",")
			#	outFileMax.write(",".join(row))
			#	outFileMax.write("\n")
			#for row in minContents:
			#	outFileMin.write(str(k)+",")
			#	outFileMin.write(",".join(row))
			#	outFileMin.write("\n")
			for row in stdContents:
				for index,item in enumerate(row):
					if isNumber(item):
						row[index] = str((float(row[index])/(n-1))**.5)
				outFileStd.write(str(k)+",")
				outFileStd.write(",".join(row))
				outFileStd.write("\n")
			
			outFile.close()
			outFileStd.close()
			#outFileMax.close()
			#outFileMin.close()
				
except ValueError:
	print inFileName, row, line
	
