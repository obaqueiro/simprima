#!/usr/bin/env python
from xml.etree import ElementTree as ET
import sys

municipalities = ["'15090005" ,"'15090245" ,"'15090515","'15090"] 
inputFactors = {}
def main():
	# read  output data from simulation
	outFile = open("output.txt","w")
	
	# write file header 
	outFile.write("#Simulation data obtained from Altmark Model adaptation\n")
	outFile.write("Region\tVariable\tYear\tValue\tType\n")
	
	inFile = open("dataOut/HohenbergExploration/average/average-2007-PopulationAgeDistribution-.csv")
	while True:
		line = inFile.readline()
		if line=='':
			break
		line = line.split(",")
		if line[0] in municipalities:
			outFile.write("\t".join([line[0][1:],"Age0","2007",line[2],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Age6","2007",line[3],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Age10","2007",line[4],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Age18","2007",line[5],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Age25","2007",line[6],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Age45","2007",line[7],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Age65","2007",line[8],"Absolute"])+"\n")
	inFile.close()

	inFile = open ("dataOut/HohenbergExploration/average/average-2007-BirthsDeathsCommuting-.csv")
	while True:
		line = inFile.readline()
		if line=='':
			break;
		line = line.split(",")
		if line[0] in municipalities:
			outFile.write("\t".join([line[0][1:],"Births","2007",line[2],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Deaths","2007",line[3],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Out-Migration","2007",line[3],"Absolute"])+"\n")
			
	inFile.close()
	inFile = open ("dataOut/HohenbergExploration/average/average-2007-HouseholdStructure-.csv")
	
	# ###############0###############################################################################
	# Add the municipality household structure considering municipalities with < 2000 inhabitants  #
	# ##############################################################################################
	# line 13 contains the required data
	lineNo = 0
	while True:
		lineNo = lineNo + 1
		line = inFile.readline()
		if line=='':
			break;

		if lineNo==13:
			line = line.split(",")
			# add data for each municipality being used
			for munic in municipalities:
				outFile.write("\t".join([munic[1:], "Household1Person", "2007", line[1],"Percentage"])+"\n")
				outFile.write("\t".join([munic[1:], "Household2Person", "2007", line[2],"Percentage"])+"\n")
				outFile.write("\t".join([munic[1:], "Household3Person", "2007", line[3],"Percentage"])+"\n")
				outFile.write("\t".join([munic[1:], "Household4Person", "2007", line[4],"Percentage"])+"\n")
				outFile.write("\t".join([munic[1:], "Household5Person", "2007", line[5],"Percentage"])+"\n")
	inFile.close()
	# ##############################################################################################
	# Add the Data about Employment #
	# ##############################################################################################
	inFile = open ("dataOut/HohenbergExploration/average/average-2006-Employment-.csv")
	while True:
		line = inFile.readline()
		if line=='':
			break
		line = line.split(",")
		if line[0] in municipalities:
			outFile.write("\t".join([line[0][1:],"Employed","2006",line[1],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Unemployed","2006",line[2],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Employed15-25","2006",line[3],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Employed25-50","2006",line[5],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Employed50-65","2006",line[7],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Unemployed15-25","2006",line[4],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Unemployed25-50","2006",line[6],"Absolute"])+"\n")
			outFile.write("\t".join([line[0][1:],"Unemployed50-65","2006",line[8],"Absolute"])+"\n")
	inFile.close()
	
	# Add data about Sector of Activity Distribution
	inFile = open ("dataOut/HohenbergExploration/average/average-2006-SectorOfActivity-.csv")
	while True:
		line = inFile.readline()
		if line=='':
			break
		line = line.rstrip()
		line = line.split(",")
		line=filter(None, line)
		if line[0] in municipalities:
			# calculate percentages
			percent = []
			# get total sum
			print line[1:]
			total = sum([float(x) for x in line[1:]]) 
			percent = [str(float(x)/total) for x in line[1:]]
			print percent
			outFile.write("\t".join([line[0][1:],"SoA1","2006",percent[0],"Percentage"])+"\n")
			outFile.write("\t".join([line[0][1:],"SoA2","2006",percent[1],"Percentage"])+"\n")
			outFile.write("\t".join([line[0][1:],"SoA3","2006",percent[2],"Percentage"])+"\n")
			outFile.write("\t".join([line[0][1:],"SoA4","2006",percent[3],"Percentage"])+"\n")
			outFile.write("\t".join([line[0][1:],"SoA5","2006",percent[4],"Percentage"])+"\n")
			outFile.write("\t".join([line[0][1:],"SoA6","2006",percent[5],"Percentage"])+"\n")
			outFile.write("\t".join([line[0][1:],"SoA7","2006",percent[6],"Percentage"])+"\n")
	inFile.close()
	
	#Add data about workplace 
	# Add data about Sector of Activity Distribution
	inFile = open ("dataOut/HohenbergExploration/average/average-2005-WorkingPlace-.csv")
	while True:
		line = inFile.readline()
		if line=='':
			break
		line = line.rstrip();
		line = line.split(",")
		
		if line[0] in municipalities:
			outFile.write("\t".join([line[0][1:],"Workplace","2005",line[1],"Absolute"])+"\n")
	inFile.close()
	
	
	
if __name__ == "__main__":
	main()
	
	