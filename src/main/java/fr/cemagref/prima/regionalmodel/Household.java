/*
 *  Copyright (C) 2011 Cemagref, IAMO
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *sets
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.Individual.Status;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Sylvie Huet <sylvie.huet@cemagref.fr>; 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 * @author Omar Baqueiro Espinosa <baqueiro@iamo.de)>
 * 
 * 
 */
public class Household implements Iterable<Individual> {

    private static final Logger LOGGER = Logger.getLogger(Household.class.getSimpleName());
    private static int cpt = 0;
    private int id;
    /** Municipality of the next residence of the household (when it is going to probabilisticMove)   */
    private transient Municipality nextResMunicipality;
    /**NextSizeOfResidence (-1 means no residence)  */
    private transient int NextResidenceSize;
    private Residence residence;
    /** Defines whether the household owns this residence **/
    private boolean ownsResidence;
    /**
     * Household types: correspond to the code used by StartPopulation when it generates a population
     * <ul><li>0: single
     * <li>1: monoparental family
     * <li>2: couple without children
     * <li>3: couple with children
     * <li>4: complex household (other households)
     */
    public static enum Type {
        SINGLE,
        SINGLE_W_CHILDREN,
        COUPLE,
        COUPLE_W_CHILDREN,
        COMPLEX;
        /**
         * Returns an integer number representing the type of the household. Used
         * only for backwards compatibility
         * @param type The Household Type to be converted
         * @return The integer code equivalent to the provided household type
         * @deprecated Consider changing the calling program to use the Enumerated values instead of integers
         */
        @Deprecated
        static public int getTypeNumber(final Type type) {
            int typeNum = 0;
            switch (type) {
                case SINGLE:
                    typeNum = 0;
                    break;
                case SINGLE_W_CHILDREN:
                    typeNum = 1;
                    break;
                case COUPLE:
                    typeNum = 2;
                    break;
                case COUPLE_W_CHILDREN:
                    typeNum = 3;
                    break;
                default:
                    typeNum = 4;
            }
            return typeNum;
        }
    }
    private Type type;
    /**
     * Total list of members of the household
     */
    protected List<Individual> listOfMembers;
    private Household oldPartner = null;
    private boolean coupleAtParents = false;
    private Individual leader = null;
    /**
     * Year of meeting: iteration number during which the individual of the couple of the household has met each other
     */
    private int meetingYear;
    /**
     * Say if the household wants to change its residence due to
     * a professional migration or household splitting.
     * Warning: if it is false, it doesn't mean that the household doesn't want
     * at all to change its residence.
     */
    private transient boolean needOfResidence;
    /**
     * Municipality where the household lives
     */
    private transient Municipality myVillage;
    /**
     * @param justSuppressed
     *            indicates that the household has to be suppressed at the end of the current iteration
     */
    private transient boolean justSuppressed;

    public Random getRandom() {
        return myVillage.getMyRegion().getMyApplication().getRandom();
    }

    public long getId() {
        return id;
    }

    public Municipality getNextResMunicipality() {
        return nextResMunicipality;
    }

    public void setNextResMunicipality(Municipality nextResMunicipality) {
        this.nextResMunicipality = nextResMunicipality;
    }

    public int getNextSizeOfResidence() {
        return NextResidenceSize;
    }

    public void setNextSizeOfResidence(int nextSizeOfResidence) {
        this.NextResidenceSize = nextSizeOfResidence;
    }

    public Residence getResidence() {
        return residence;
    }

    public void setTransitResidence() {
        this.residence = Residence.createTransitResidence(this);
    }

    public void updateOwnsResidence() {
        // do not assign residence ownership to complex households
        if (this.type == Type.COMPLEX){
            return;
        }
        Individual head = this.determineLeader();        
        this.ownsResidence=myVillage.getMyRegion().getParameters().isResidenceOwner(head);        
    }
    
    public boolean isResidenceOwner() {
        return this.ownsResidence;
    }
    /**
     * Add the household to a residence and notify the old partner if any (now,
     *  they can have a new partner)
     * @param residence
     */
    public void setResidence(Residence residence) {
        if (this.residence != null && this.residence.isTransit()) {
            for (Individual ind : listOfMembers) {
                myVillage.getCounters().incNbHouseholdMoveIn(1);
                myVillage.getCounters().incNbIndividualMoveIn(size());
                ind.setMigrationYear( myVillage.getMyRegion().getCurrentYear());
                // compute again the age to die if it has already passed
                if (ind.getAgeToDie() <= ind.getAge()) {
                    ind.initAgeToDie();
                }
            }
        }
        this.residence = residence;
        residence.add(this);
        if ((oldPartner != null) && (residence != oldPartner.getResidence())) {
            oldPartner = null;
        }
    }

    /**
     * Household types:
     * <ul><li>0: single
     * <li>1: monoparental family
     * <li>2: couple without children
     * <li>3: couple with children
     * <li>4: complex household (other households)
     */
    public Type getHshType() {
        return type;
    }

    public int getMeetingYear() {
        return meetingYear;
    }

    public boolean needsResidence() {
        return needOfResidence;
    }

    public void setNeedsResidence(boolean needOfResidence) {
        this.needOfResidence = needOfResidence;
    }

    /**
     *
     * @return an iterator on members of the Household
     */
    @Override
    public Iterator<Individual> iterator() {
        return listOfMembers.iterator();
    }

    public int indexOf(Individual o) {
        return listOfMembers.indexOf(o);
    }

    /**
     * Get a working copy of the list of members. Usefull for iterating over members
     * while we can remove a member, so that your are sure to get the correct members.
     * @return
     */
    public List<Individual> getCopyOfMembers() {
        List<Individual> members = new ArrayList<Individual>(listOfMembers.size());
        members.addAll(listOfMembers);
        return members;
    }

    /**
     *
     * @return the number of individuals in this household
     */
    public int size() {
        return listOfMembers.size();
    }

    public List<Individual> getAdults() {
        List<Individual> adults = new ArrayList<Individual>();
        for (Individual member : listOfMembers) {
            if (member.isAdult()) {
                adults.add(member);
            }
        }
        return adults;
    }

    public List<Individual> getChildren() {
        List<Individual> children = new ArrayList<Individual>();
        for (Individual member : listOfMembers) {
            if (!member.isAdult()) {
                children.add(member);
            }
        }
        return children;
    }

    public boolean isCoupleAtParents() {
        return coupleAtParents;
    }

    public void setCoupleAtParents(boolean coupleAtParents) {
        this.coupleAtParents = coupleAtParents;
    }

    /**
     * Give the size of the household
     */
    public int getSize() {
        return listOfMembers.size();
    }

    /**
     * Determine the leader of the household. If a leader has already be determined in
     * the current timestep, he is returned.
     * If there is only one adult, he is promoted as leader and returned.
     * If there is two adults, we pick on randomly.
     * @return the selected adult for leadership.
     */
    public Individual determineLeader() {
        if (leader == null) {
            List<Individual> adults = getAdults();
            if (adults.size() == 1) {
                leader = adults.get(0);
            } else {
                leader = adults.get(getRandom().nextInt(0, 1));
            }
        }
        return leader;
    }

    /**
     * Reset the leader of the household, so the method {@link determineLeader()}
     * will rebuild a new leader.
     */
    public void resetLeader() {
        leader = null;
    }

    public Municipality getMyVillage() {
        return myVillage;
    }

    public void setMyVillage(Municipality myVillage) {
        this.myVillage = myVillage;
    }

    public boolean isJustSuppressed() {
        return justSuppressed;
    }

    public void setJustSuppressed(boolean justSuppressed) {
        this.justSuppressed = justSuppressed;
    }

    public Object readResolve() {
        for (Individual ind : listOfMembers) {
            ind.setMyHousehold(this);
        }
        return this;
    }

    /**
     * Constructor for the household at the initialization time
     */
    public Household(Municipality myVill, final int[] household) {
        this.id = ++cpt;
        this.myVillage = myVill;        
        // The first number indicates the family types
        switch (household[0]){
            case 0:
                this.type = Type.SINGLE;                
                break;
            case 1: this.type = Type.SINGLE_W_CHILDREN;
                break;
            case 2: this.type = Type.COUPLE;
                break;
            case 3: this.type = Type.COUPLE_W_CHILDREN;
                break;
            case 4: 
            default:
                this.type = Type.COMPLEX;
             
        }
                        
        this.meetingYear = -1;
        final int hshSize = household.length - 1;
        listOfMembers = new ArrayList<Individual>(hshSize);
        
        boolean child=false;
        for (int curIndiv = 1; curIndiv <= hshSize; curIndiv++) {
            switch (this.type) {
                case SINGLE_W_CHILDREN:                     
                        child = curIndiv>1; // Individuals after 1st parent are children
                    break;
                case COUPLE_W_CHILDREN:
                    child = (curIndiv>2); // Individuals after 2nd parent are children                   
                    break;
                    
                case COUPLE:
                case SINGLE:
                    child = false;
                    break;
                
                case COMPLEX: // force individuals < 18 as children
                    if (household[curIndiv]<15){
                        child=true;
                    }                                                                                
            }

            assert household[curIndiv]>=0:"Individual age must be >=0. Found"+household[curIndiv];
            
            listOfMembers.add(new Individual(this, !child, household[curIndiv]));
        }        
     
        this.residence = null;
        this.justSuppressed = false;
    }

    /**
     * Constructor for the households of the Outside rural region village containing only household of single living there and working in the other regional village
     */
    public Household(Municipality myVill, int age) {
        this(myVill, new int[]{age});
    }

    /**
     * Constructor for the updating and moving households of the Outside rural region village containing only household of single living there and working in the other regional
     * village - if it is a migrant household, the new household is a duplication with a new household number
     */
    public Household(Household household, boolean migrant) {
        listOfMembers = new ArrayList<Individual>();
        if (migrant) {
            this.id = ++cpt;
            this.residence = Residence.createTransitResidence(this);
            updateOwnsResidence();
        } else {
            this.id = household.id;
            this.residence = household.residence;
            listOfMembers.addAll(household.listOfMembers);
            this.NextResidenceSize = household.getNextSizeOfResidence();
            this.nextResMunicipality = household.getNextResMunicipality();
            this.ownsResidence = household.ownsResidence;
            this.leader = household.leader;
            for (Individual member : listOfMembers) {
                member.setMyHousehold(this);
            }
            assert this.getAdults().size()<=2 : 
                    String.format("A household must have 2 or less adults. Found:%s at %s",
                    getAdults().size(), household.toString());
        }
        assert this.getAdults().size()<=2 : "A household must have 2 or less adults. Found:"+getAdults().size();
        myVillage = household.myVillage;
        type = household.type;
        meetingYear = household.meetingYear;
    }

    public Household(Municipality myVill, List<Individual> myInd, Residence residence) {
        this(myVill, myInd);
        this.type = calchHouseholdType();
        this.residence = residence;
        this.ownsResidence=false;
        if (residence != null) {
            residence.add(this);
        }
    }

    private Type calchHouseholdType(){
                
        // if it is one individual then it must be single
        if (this.listOfMembers.size() ==1 ){
            return Type.SINGLE;
        }
        
        // if more than 1 individual but only one adult then it is single with children
        if (this.getAdults().size()==1){
            return Type.SINGLE_W_CHILDREN;
        }
        
        // if 2 adults and exactly 2 members ten it is a couple without children
        if (this.getAdults().size()==2 && this.listOfMembers.size()==2){
            return Type.COUPLE;
        }
        
        // if 2 adults and more than 2 members ten it is a couple with children
        if (this.getAdults().size()==2 && this.listOfMembers.size()>2){
            return Type.COUPLE_W_CHILDREN;
        }
        // other cases are considered Complex
        return Type.COMPLEX;
        
    }
    private Household(Municipality myVill, List<Individual> myInd) {
        this.id = ++cpt;
        this.ownsResidence=false;
        this.myVillage = myVill;
        listOfMembers = myInd;
        assert this.getAdults().size()<=2 : "A household must have 2 or less adults. Found:"+getAdults().size();
        for (Individual individual : myInd) {
            individual.setMyHousehold(this);
        }
   
    }

    /**
     * Method searching a job if decided previously. If a job is found, changes are applied.
     */
    public void searchJobs() {
        for (Individual member : getCopyOfMembers()) {      

            if (!member.isLookForJob()) {
                continue;
            }
             Status oldStatus = member.getStatus();
            member.seekJob(false);

            if (oldStatus == Status.UNEMPLOYED && member.getStatus() == Status.WORKER) {
                getMyVillage().getMyRegion().anualUnemployedToEmployed++;
            }
        }
    }

    /**
     * Build the intersection between the neighborhood of the job of the leader
     * and the neighborhood of the residence of the household.
     * @return the list of municipalities in the intersection
     */
    public List<Municipality> getJobAndResidenceProximityIntersection() {
        if ((determineLeader().getCurrentJobLocation() == null) || (determineLeader().getCurrentJobLocation() == myVillage)) {
            return Arrays.asList(myVillage.getProximityJob());
        } else {
            List intersect = new ArrayList<Municipality>(Arrays.asList(determineLeader().getCurrentJobLocation().getProximityJob()));
            intersect.retainAll(Arrays.asList(myVillage.getProximityJob()));
            return intersect;
        }
    }

    /**
     * Attach to an existing residence. Method used at initialization when the household has not been able to find a residence.
     * The method gets an existing residence at random and attaches the household in this residence
     */
    public void attachToExistingResidence() {
        int resType ;
        if (size() >= myVillage.getResidencesTypeCount()) {
            resType = myVillage.getResidencesTypeCount() - 1;
        } else {
            do {
                resType = getRandom().nextInt(size()+1, myVillage.getResidencesTypeCount()) - 1;
                                
            } while (myVillage.getOccupiedResidencesCount(resType) < 1  || 
                    getSize()> getMyVillage().getMyRegion().getCapacityOfAvailRes(resType));
        }
        residence = myVillage.pickAnOccupiedResidence(resType);
        residence.add(this);
    }

    /**
     * Method to change of residence if necessary and possible S. Huet, 29.07.2009 - If the ideal
     * size is not available, the algorithm try one room more, then one room less, then two rooms more, then two rooms less, ...
     */
    public void initResidence() {
        final int myIdealSize = getMyVillage().appropriatedSizeOfRes(listOfMembers.size());
     
        if (!needToChangeRes(myIdealSize)){
            return; 
        }

        int size = -1;
        for (int i = 0; i < getMyVillage().getResidencesTypeCount() ; i++) {
            // Look for residences with size myIdealSize + i 
            if ((myIdealSize + i < getMyVillage().getResidencesTypeCount()) && getMyVillage().hasFreeResidences(myIdealSize + i)) {
                size = myIdealSize + i;
                break;
            }
            // Look for residences with size myIdealSize - i 
            if (myIdealSize - i > 0 && getMyVillage().hasFreeResidences(myIdealSize - i)) {
                size = myIdealSize - i;
                break;
            }
        }

        if (size >= 0) {
            if (getResidence() != null) {
                getMyVillage().releaseResidence(this);
            }
            setNextSizeOfResidence(size);
            getMyVillage().takeResidence(this);
        }


        if (getResidence() == null) {
            getMyVillage().getCounters().incNbOfDwellingLacking(1);
        }
    }

    /**
     * Method to init the status, the place of work and the pattern of individuals of the household The order of the pat table is the same than the one the individuals have been
     * stocked in the household list of members The format of data is described in a doc file "Instructions for the activity file.doc" S. Huet 08.10.2009
     */
    public void initIndividualActivities(String[][] pat) {

        List<Individual> temp = getCopyOfMembers();
        for (int memberIndex = 0; memberIndex < temp.size(); memberIndex++) {
            Individual member = temp.get(memberIndex);
            member.setStatus(Status.valueOf(Integer.parseInt(pat[memberIndex][0])));
            if (member.getStatus() != Status.STUDENT) {
                // this value is chosen because it will have no impact during the simulation time (the individual has already an activity)
                member.setAgeToEnterOnTheLabourMarket(member.getAge() - 1);
                
            }
            switch (member.getStatus()) {
                case WORKER: 
                    // by default it lives and works in the same municipality for all its activities
                    Municipality mun = getMyVillage();
                    if (!pat[memberIndex][1].equals(getMyVillage().getName())) {
                        // it works outside the village for all its activities
                        mun = getMyVillage().getMyRegion().getMunicipality(pat[memberIndex][1]);
                        if (mun == null) {
                            mun = getMyVillage().getMyRegion().getOutsides().get(0);
                            // FIXME non found municipalities are attributed to the first outside
                            LOGGER.warning("Municipality " + pat[memberIndex][1] + " has not been found. First outside region has been attributed as job location.");
                        }
                    }               
                    member.setCurrentJobLocation(mun);
                    member.setPreviousJobLocation(mun);          
                    
                    Activity act = new Activity(Integer.parseInt(pat[memberIndex][2]),Integer.parseInt(pat[memberIndex][3]));
                    member.setCurrentActivity(act);                                     
                    mun.addWorker(member);
                                                         
                    // ensure that this worker is an adult
                    if (!member.isAdult() ) {
                        member.becomeAdult();
                    }
                    break;
                case UNEMPLOYED:
                    member.setCurrentActivity(new Activity(Integer.parseInt(pat[memberIndex][1])));
                    // number 0 is the pattern of unemployed
                    member.setCurrentActivity(getMyVillage().getMyRegion().getAllActivities()[0]);                    
                    member.setCurrentJobLocation(getMyVillage());
                    //getMyVillage().setPatternOccupancy(0, getMyVillage().getPatternOccupancy()[0] + 1);
                    break;
                case INACTIVE:
                case STUDENT:
                    // Attribution of a random profession to work with at the beginning: done later in the last code version
                    // member.affectAFirstProfession();
                    break;
                default:
                    break;
            }
            //System.err.println("profession donnée à l'init "+member.getProfession()+" id hh "+getId()+" id indiv "+member.getId()+ "status "+member.getStatus()) ;
        }
    }

    /**
     * Look for each inactive, worker or unemployed of the household if they are going to change their labour situation (means change of status (unactive to active or the contrary)
     * or of activityPattern For each individual, the current situation is identified and the link is done between this current situation and the line of probability to use in the
     * transitionMatrix in order to send to the decideAboutLabourSituation() method the good probabilities to decide PAY ATTENTION IN CASE OF PLURIACTIVITY: It has to be notice
     * that for the Inactive and Unemployed status, only the first value of the attribute[] profession of the individual is considered to decide about the current state in the
     * transitionMatrix (if the past professional detail is considered by this matrix). It means that, if previously, the individual was pluriactive, it is only the profession
     * linked to the first activity encountered in the past ActivityPattern which is considered to decide about the probability of transition to another state.
     */
    public void willingJobs() {
        for (Individual member : getCopyOfMembers()) {
            member.pickTheNextJobSituationNew(getMyVillage());
        }
    }


    /* *************************************************
     * Functions for changing household structure
     */
    /**
     * Method spliByDivorce for couple and couple with children The splitting follows a constant probability
     */
    public boolean splitByDivorce() {
        return (getRandom().nextDouble() <= this.myVillage.getMyRegion().getParameters().getSplittingProba()) ;
    }

    /*
     * Method death
     */
    public List<Individual> death() {
        List<Individual> suppressed = new ArrayList<Individual>();
        for (Individual ind : listOfMembers) {
            if (ind.getAge() >= ind.getAgeToDie()) {             
                suppressed.add(ind);
                getMyVillage().getMyRegion().annualNbDeath++;                
            }
        }
        return suppressed;
    }

    /**
     * Method : for defining if a couple of parents (in case the household has a couple of parents) have a baby at this time
     *
     * @author sylvie.huet 17.03.2010
     * @return a boolean indicating if the couple of parents have a baby
     */
    public boolean birth() {
        boolean newborn = false;
        if (getHshType() == Type.COUPLE || getHshType() == Type.COUPLE_W_CHILDREN) {
            
            // if household does not correspond to number of adults, update household size
            // TODO-OMAR: There is a bug, there are households with 0 individuals!
            if (getAdults().size()<=1){
                if (getChildren().size()>0){
                    type=Type.SINGLE_W_CHILDREN;
                }
                else{
                    type=Type.SINGLE;
                }
                return false;                
            }
            
            final Individual indiv = getAdults().get(getRandom().nextInt(0, 1));
            if (indiv.getAge() >= myVillage.getMyRegion().getAgeMinToChild()
                    && indiv.getAge() <= myVillage.getMyRegion().getAgeMaxToChild()
                    && getRandom().nextDouble() <= myVillage.getMyRegion().getProbaBirth()) {                
                    newborn = true;
                    getMyVillage().getMyRegion().annualNbBirth++;                
            }
        }
        return newborn;
    }

    /**
     * Function for supressing one member in a household A child leaving his family or someone die
     */
    public void suppressMember(int member, MemberSuppressionReason event) {
        suppressMember(listOfMembers.get(member), event);
    }

    public enum MemberSuppressionReason {
        BECOME_ADULT, //0
        PARTNER_QUITTING, //2
        DEATH,  //3
        STUDY_ABROAD
    }
    
    public void suppressMember(Individual member, MemberSuppressionReason event) {
        switch (event) {
            case BECOME_ADULT: // a children becomes an adult and leaves the parental household
                listOfMembers.remove(member);
                break;
            case PARTNER_QUITTING: // in case a partner quits the other, the leaving one and its
                // children are suppressed from the remaining household
                break;
            case DEATH:
                // someone died, it leaves the activity pattern
                member.leaveActivity();
                break;
            case STUDY_ABROAD:
                break;
        }
        // Is the member to suppress is a child, in case, it has also to be suppressed of the list of child
        listOfMembers.remove(member);        
    }

    /**
     * Function for adding a new individual in a household
     */
    public void addMember(Individual newMember) {
        newMember.setMyHousehold(this);
        listOfMembers.add(newMember);
    }

    /**
     * Says if an individual is looking for a partner.
     * An individual looks for a partner if she is a single adult or if she is
     * not an adult but is older than 15 years old.
     * @param member The member who may or may not be looking for a partner
     * @return True if an individual is looking for a partner or false otherwise
     */
    public boolean isLookingForAPartner(Individual member) {
        boolean looks = false;
        if (member.isAdult() ) {           
            looks = (oldPartner == null)&&
                    ((type == Type.SINGLE) || (type == Type.SINGLE_W_CHILDREN));                                      
        } else {
            looks = (member.getAge() > 15);                
        }        
        return looks;
    }

    /**
     * true if household is single and possible old partner left.
     * @param member
     * @return
     */
    public boolean singleAdult() {
        if ((getAdults().size() == 1) && (oldPartner == null)) {
            return true;
        }
        return false;
    }

    /**
     * Method returning a boolean saying if a household wants to change of residence SH: 18.02.2010
     */
    public boolean needToChangeRes(final int idealTypeOfRes) {
        boolean change;
        if (getResidence() == null) {
            change = true;
        } else {
            // Assume household is satisfied if the size of the flat is more or
            // less 1 room from the ideal size
            if (getResidence().getType() <= idealTypeOfRes +1 
                    || getResidence().getType() >= idealTypeOfRes -1){
                change = false;
            }
            else {
                change = true;
            }            
        }
        return change;
    }

    /**
     *  Update of the household type when a child become adult after his first job
     */
    public void updateHshFeaturesWhenBecomingAdultForAFirstJob() {
        // decide if the concerned household is the one of the parent or the
        // new one of the old child
        if (listOfMembers.size() == 1) { // the new young household which call the method
            // or it was a single parent family
            assert (listOfMembers.get(0).isAdult());
            type = Type.SINGLE;
        }
        // change household type when last children leaves
        if (getHshType() != Type.COMPLEX && getChildren().isEmpty()) {
            switch (getHshType()){
                case SINGLE_W_CHILDREN:
                        type = Type.SINGLE;
                        break;
                case COUPLE_W_CHILDREN:
                        type = Type.COUPLE;
            }           
        }
    }

    /**
     * Update of the household type after leaving the other adult of the couple.
     * Reset the meeting year to -1.
     */
    public void updateHshFeaturesAfterDivorce(Household oldPartner) {
        assert getAdults().size() == 1;
        this.oldPartner = oldPartner;
        coupleAtParents = false;
        resetLeader();
        if (listOfMembers.size() > 1) { // that is a new household with
            // children, thus a monoparentaly family
            type = Type.SINGLE_W_CHILDREN;
        } else { // that is a new household without children, thus a single
            type = Type.SINGLE;
        }
        meetingYear = -1;
    }

    /**
     * Update of the household type after a death.     
     */
    public void updateHshFeaturesAfterDeath() {
        // has to decide regarding who is dead
        boolean parentDead = false;
        int nbParents = 0;
        if (listOfMembers.size() == 1) {
            type = Type.SINGLE;
            resetLeader();
        } else {
            // decide if the concerned household is the one of the parent or the new one of the old child
            int nbChild = getChildren().size();
            nbParents = listOfMembers.size() - nbChild;
            if (nbParents <= 1) {
                parentDead = true;
            }
            if (parentDead) {
                if (nbParents == 1) {
                    if (getHshType() == Type.COUPLE) {
                        type = Type.SINGLE;
                        resetLeader();
                    }
                    if (getHshType() == Type.COUPLE_W_CHILDREN) {
                        type = Type.SINGLE_W_CHILDREN;
                        resetLeader();
                    }
                } else {
                    if (listOfMembers.size() == 1) {
                        type = Type.SINGLE;
                        resetLeader();
                    } else {
                        type = Type.COMPLEX;
                    }
                }
            } else {
                if (nbChild == 0) {
                    if (getHshType() == Type.COUPLE_W_CHILDREN) {
                        type = Type.COUPLE;
                    }
                    if (getHshType() == Type.SINGLE_W_CHILDREN) {
                        type = Type.SINGLE;
                        resetLeader();
                    }
                }
            }
        }
    }

    /**
     * Update of the household type after a birth
     */
    public void updateHshFeaturesAfterBirth() {
        assert getHshType() == Type.COUPLE || getHshType()==Type.COUPLE_W_CHILDREN: 
                String.format("Household must be a couple to have birth. Found %s in %s",
                getHshType(), this.toString());
        if (getHshType() == Type.COUPLE) {
            type = Type.COUPLE_W_CHILDREN;
        }
    }

    /**
     * Update of the household type when the household join another adult
     */
    public void updateHshFeaturesAfterJoining(int iter) {
        meetingYear = iter;
        resetLeader();
        if (getHshType() == Type.SINGLE) {
            if (getChildren().isEmpty()) {
                type = Type.COUPLE;
            } else {
                type = Type.COUPLE_W_CHILDREN;
            }
        }
        if (getHshType() == Type.SINGLE_W_CHILDREN) {
            type = Type.COUPLE_W_CHILDREN;
        }
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder("Household ").append(id);
        if (justSuppressed) {
            buff.append(" justSuppressed ");
        }
        if (getResidence() != null && getResidence().isTransit()) {
            buff.append(" isTransit ");
        }
        buff.append(" in ").append(getMyVillage().getName()).append("\n");
        for (Individual member : listOfMembers) {
            buff.append("  #").append(member.getId()).append(" ").append(member.getAge()).append("(").append(member.isAdult() ? "adult" : "child").append(")");
            buff.append(",").append(member.getStatus()).append(",").append(member.getCurrentActivity());
            buff.append("\n");
        }
        return buff.toString();
    }

    /**
     * Method to create migrants by duplication of the potentially migrant individuals.
     * New migrants are created by spawning copies of current individuals in the
     * provided household. Created individuals are then added as members of the provided household.
     * 
     * The status of created individuals cloned from Worker individuals is set to UNEMPLOYED. 
     * 
     * @param sourceHousehold Household containing the individuals that will be cloned to
     * create new immigrants.
     * 
     */
    public void createMigrants(Household sourceHousehold) {  
        for (Individual indiv : sourceHousehold) {
            Individual newInd = new Individual(indiv, true);
            newInd.setMyHousehold(this);
            switch (newInd.getStatus()) {
                case WORKER:
                    newInd.setStatus(Status.UNEMPLOYED);
                    newInd.becomeUnemployed(getMyVillage());
                    newInd.setLookForJob(true);
                    break;
                case UNEMPLOYED:
                    newInd.setCurrentJobLocation(this.getMyVillage());
                    newInd.takeActivity(new Activity(indiv.getProfession()), indiv.getCurrentJobLocation());
                    newInd.setNextActivity(null);
                    newInd.setLookForJob(true);
                    break;
            }
            listOfMembers.add(newInd);
        }
    }
    
    
}
