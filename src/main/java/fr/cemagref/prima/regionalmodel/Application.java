
/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

/**
 *
 * @author sylvie.huet
 * 8.04.09
 */
import com.thoughtworks.xstream.XStream;
import fr.cemagref.observation.kernel.ObservableManager;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.prima.regionalmodel.gui.ChartsPanel;
import fr.cemagref.prima.regionalmodel.gui.MainWindow;
import fr.cemagref.prima.regionalmodel.parameters.*;
import fr.cemagref.prima.regionalmodel.scenarios.DistributedTableUpdater;
import fr.cemagref.prima.regionalmodel.scenarios.ImmigrationUpdater;
import fr.cemagref.prima.regionalmodel.scenarios.Scenario;
import fr.cemagref.prima.regionalmodel.scenarios.TableUpdater.UpdateMode;
import fr.cemagref.prima.regionalmodel.scenarios.grouping.ExcludeOutsideMunicipalities;
import fr.cemagref.prima.regionalmodel.scenarios.grouping.MunicipalitiesMinimumSurfaceFilter;
import fr.cemagref.prima.regionalmodel.scenarios.grouping.MunicipalityIdentifiedByName;
import fr.cemagref.prima.regionalmodel.scenarios.period.PartialTableUpdater;
import fr.cemagref.prima.regionalmodel.scenarios.period.ScenarioPeriod;
import fr.cemagref.prima.regionalmodel.scenarios.period.distmethod.GivenDistribution;
import fr.cemagref.prima.regionalmodel.scenarios.period.distmethod.OneTimeRandom;
import fr.cemagref.prima.regionalmodel.scenarios.period.distmethod.UniformDistributed;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.io.*;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.apache.commons.cli.*;

/**
 * Main class of the simulator. As it used the commons-cli library for parsing the
 * command line arguments, running the program without arguments will give you a
 * description of the required arguments.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Application {

    private static final Logger LOGGER = Logger.getLogger(Application.class.getSimpleName());
    private boolean gui;
    private Parameters parameters;
    private List<Scenario> scenarios;
    private Random random;
    public static String nameFile;
    public static String nameFile1;
    public static String nameFile2;
    public static String myInPath = "dataIn" + File.separator;
    private final static XStream xstream = new XStream();

    static {
        for (Class type : new Class[]{Parameters.class, ValueFromConstant.class, ValueFromGivenDistribution.class,
            AgeDeterminationFromAgeAdult.class, AgeDeterminationFromDecisionByAge.class,
            AgeDeterminationFromDecisionByProfessionAndAge.class, DecisionByAgeAndProbas.class,
            IntValueFromExponentialLaw.class, IntValueFromNormalLaw.class, ProfessionFromUniformLaw.class, ProfessionTransitionParameters.class,
            ValueFromConstant.class, ValueFromGivenDistribution.class, ValueFromNormalLaw.class}) {
            xstream.alias(type.getSimpleName(), type);
        }
        for (Class type : new Class[]{MunicipalitiesMinimumSurfaceFilter.class, MunicipalityIdentifiedByName.class,
            ExcludeOutsideMunicipalities.class}) {
            xstream.alias(type.getSimpleName(), type);
        }
        for (Class type : new Class[]{ScenarioPeriod.class, ImmigrationUpdater.class, PartialTableUpdater.class,
            DistributedTableUpdater.class, UpdateMode.class, GivenDistribution.class, OneTimeRandom.class, UniformDistributed.class}) {
            xstream.alias(type.getSimpleName(), type);
        }
    }

    public Application() {
        random = new Random();
    }

    public Application(final String inputFilename) throws FileNotFoundException {
        this();
        final File inputFile = new File(inputFilename);
        dataDirectory = inputFile.getParentFile();
        parameters = (Parameters) fromXML(new FileReader(inputFile));
    }

    public Application(final File dataDirectory, final Parameters parameters) {
        this();
        this.dataDirectory = dataDirectory;
        this.parameters = parameters;
    }

    public Application(final String[] args) throws IOException, BadDataException, ProcessingException {
        this();
        // command line options parsing
        final Option optionUseGUI = OptionBuilder.withLongOpt("use-gui").withDescription("Use a graphical user interface").isRequired(false).create("g");
        final Option optionInputFile = OptionBuilder.withLongOpt("input").withDescription("Input file").hasArgs(1).withArgName("filename").isRequired().create("i");
        optionInputFile.setLongOpt("input");
        optionInputFile.setDescription("Input file");
        optionInputFile.setArgs(1);
        optionInputFile.setArgName("filename");
        optionInputFile.setRequired(true);
        optionUseGUI.setLongOpt("use-gui");
        optionUseGUI.setDescription("Use a Graphical user interface");
        optionUseGUI.setRequired(false);
        final Options options = new Options();
        options.addOption(optionInputFile).addOption(optionUseGUI);
        final CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Error while parsing command line arguments");
            new HelpFormatter().printHelp(" ", options);
            throw new BadDataException("Error while parsing command line arguments");
        }
        // Load parameters
        gui = cmd.hasOption(optionUseGUI.getOpt());        
        final File inputFile = new File(cmd.getOptionValue(optionInputFile.getOpt()));
        dataDirectory = inputFile.getParentFile();
        parameters = (Parameters) fromXML(new FileReader(inputFile));
        scenarios = (List<Scenario>) fromXML(new FileReader(getFileFromRelativePath(parameters.getScenarioFile())));
    }

    public Random getRandom() {
        return random;
    }

    public static void toXML(final Object obj, final OutputStream out) {
        xstream.toXML(obj, out);
    }

    public static void toXML(final Object obj, final Writer out) {
        xstream.toXML(obj, out);
    }

    public static String toXML(final Object obj) {
        return xstream.toXML(obj);
    }

    public static Object fromXML(final InputStream input) {
        return xstream.fromXML(input);
    }

    public static Object fromXML(final Reader xml) {
        return xstream.fromXML(xml);
    }

    public static Object fromXML(final String xml) {
        return xstream.fromXML(xml);
    }
    private File dataDirectory;

    public File getDataDirectory() {
        return dataDirectory;
    }

    public void setDataDirectory(final File dataDirectory) {
        this.dataDirectory = dataDirectory;
    }

    public File getFileFromRelativePath(final String filename) {
        return new File(getDataDirectory(), filename);
    }

    public boolean isGui() {
        return gui;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(final Parameters parameters) {
        this.parameters = parameters;
    }

    public File getMunicipalityDirectory(final Municipality municipality) {
        return new File(getDataDirectory(), municipality.getName());
    }

    public void loadFiles() throws BadDataException, ProcessingException {
        parameters.initRNG(random);
        // load villages parameters
        final File[] dirs = dataDirectory.listFiles(new FileFilter() {
            @Override
            public boolean accept(final File pathname) {
                return pathname.isDirectory() && !".svn".equals(pathname.getName());
            }
        });
        // sort dirs to get the same order at each run
        Arrays.sort(dirs, new Comparator<File>() {
            @Override
            public int compare(final File filea, final File fileb) {
                return filea.getName().compareToIgnoreCase(fileb.getName());
            }
        });
        
        // counting the "outside"
        int outNb = 0;
        for (File municipalityDir : dirs) {
            if (municipalityDir.getName().toLowerCase().startsWith("outside")) {
                outNb++;
            }
        }
        // reading dirs
        parameters.loadVillages(dirs, outNb);
        // init parameter configured by a file
        for (Field field : Parameters.class.getDeclaredFields()) {
            try {
                if (Value.class.isAssignableFrom(field.getType())) {
                    final boolean accessible = field.isAccessible();
                    field.setAccessible(true);
                    final Object obj = field.get(parameters);
                    if (obj != null && Value.class.isAssignableFrom(obj.getClass())) {
                        ((Value) obj).init(this);
                    }
                    field.setAccessible(accessible);
                }
            } catch (IllegalArgumentException ex) {
                throw new BadDataException("Error while loading parameters", ex);
            } catch (IllegalAccessException ex) {
                throw new BadDataException("Error while loading parameters", ex);
            }
        }
        // load observers
        parameters.loadObservers(gui);
    }

    public MunicipalitySet run() throws IOException, BadDataException, ProcessingException {
        MunicipalitySet region=null;
        if (gui) {
            final MainWindow window = new MainWindow(this, parameters, scenarios);
            window.setVisible(true);
        } else {
            // remove charts observer
            for (final Iterator<ObserverListener> it = ObservableManager.getObservable(MunicipalitySet.class).getObservers().iterator(); it.hasNext();) {
                final ObserverListener observerListener = it.next();
                if (observerListener instanceof ChartsPanel) {
                    it.remove();
                }
            }
            region = new MunicipalitySet(this, parameters, scenarios);
            LOGGER.fine("MunicipalitySet loaded, ready to run!");
            region.run();
            return region;
        }
        return null;
    }
    public MunicipalitySet run(final int replic) throws IOException, BadDataException, ProcessingException {
        final MunicipalitySet region = new MunicipalitySet(this, parameters, scenarios);
        region.run(replic);
        return region;
    }

    /**
     * The option on the command line -g allows to benefit from an interface to run the model and visualise some indicators
     */
    public static void main(final String[] args) throws IOException, BadDataException, ProcessingException  {
        // logging init
        try {
            LogManager.getLogManager().readConfiguration(Application.class.getResourceAsStream("logging.properties"));
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING,
                    "Problems to load the logging configuration file", ex);
        }
        // load and run
        final Application application = new Application(args);
        application.loadFiles();
        application.run();
    }
}
