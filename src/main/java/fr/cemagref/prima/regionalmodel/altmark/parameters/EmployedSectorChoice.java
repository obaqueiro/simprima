/*
 * Copyright (C) 2011 Baqueiro
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.altmark.parameters;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.parameters.ConfiguredByFile;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Omar Baqueiro Espinosa
 */
@ServiceProvider(service = Value.class)
public class EmployedSectorChoice implements Value<Integer, Individual>, ConfiguredByFile {

    private String filename="";
    private Character separator=';';
    protected transient SortedMap<Integer, Double[]> dataByAge;
    
    @Override
    public void init(Application application) throws BadDataException {
          dataByAge = new TreeMap<Integer, Double[]>();
        String[] data;
        try {
            CSVReader reader = CSV.getReader(application.getFileFromRelativePath(filename), separator);
            while ((data = CSV.readLine(reader)) != null) {
                int age = data[0].length() == 0 ? 0 : Integer.parseInt(data[0]);
                Double[] probas = dataByAge.get(age);
                if (probas == null) {
                    probas = Utils.parseDoubleArray(data, 1);
                    dataByAge.put(age, probas);
                }               
                
            }
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("Error while loading " + filename, ex);
        }             
    }

    @Override
    public Integer getValue(Individual ind) {
          Double[] probas = Utils.getValueFromLowerBound(dataByAge, ind);
         return ind.getHousehold().getRandom().nextIndexWithDistribution(probas);
    }

    public EmployedSectorChoice(String probaFile){
        super();
        this.filename=probaFile;
    }
    public EmployedSectorChoice(){
        super();
    }
    @Override
    public String getFilename() {
        return filename;
    }
}
