/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.auvergne;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jxl.Sheet;
import jxl.Workbook;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class GenerateInput {

    public static void main(String[] args) throws Exception {
        Random random = new Random();
        random.setSeed(new long[]{42, 42, 42, 42, 42, 42});
        // lambert geograpical position
        CSVReader reader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/Lambert.csv"), ';');
        reader.readNext();
        Map<String, String[]> lambert = new HashMap<String, String[]>(1320);
        String[] line;
        while ((line = reader.readNext()) != null) {
            for (int i = 0; i < line.length; i++) {
                line[i] = line[i].replace(',', '.');
            }
            lambert.put(line[0], line);
        }
        reader.close();
        // intercept and slope
        reader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/CoefficentRegression.csv"), ';');
        reader.readNext();
        Double[][] coeffRegression = new Double[4][2];
        for (int i = 0; i < 4; i++) {
            line = reader.readNext();
            coeffRegression[i][0] = Double.parseDouble(line[2].replace(",", "."));
            coeffRegression[i][1] = Double.parseDouble(line[3].replace(",", "."));
        }
        reader.close();
        reader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/IDDistComFreq.csv"), ';');
        reader.readNext();
        Map<String, Double[]> interceptsSlopes = new HashMap<String, Double[]>(1320);
        List<String> munIDs = new ArrayList<String>(1320);
        while ((line = reader.readNext()) != null) {
            String id = new Formatter().format("%05d", Integer.parseInt(line[0])).toString();
            interceptsSlopes.put(id, coeffRegression[Integer.parseInt(line[1])]);
            // store the list of mun IDs
            munIDs.add(id);
        }
        reader.close();

        // housing
        Workbook w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/LogementParam.xls"));
        Sheet sheet = w.getSheet(0);
        Map<String, Integer[]> housings = new HashMap<String, Integer[]>(1320);
        for (int i = 1; i < sheet.getRows(); i++) {
            // read occupations for each mun
            String munID = sheet.getCell(0, i).getContents();
            for (int j = 1; j >= 0; j--) {
                Integer[] housing = new Integer[4];
                int sum = 0;
                int free = Integer.parseInt(sheet.getCell(2+5*j, i).getContents());
                for (int col = 0; col < 4; col++) {
                    housing[col] = Integer.parseInt(sheet.getCell(col + 3+5*j, i).getContents());
                    sum += housing[col];
                }
                // redistribute the free residence amount proportionnaly to the occupied amount
                int freeRedistributed = 0;
                double[] rests = new double[4];
                for (int col = 0; col < 4; col++) {
                    double delta = free * ((double) housing[col] / sum);
                    rests[col] = delta - (int) delta;
                    housing[col] += (int) delta;
                    freeRedistributed += delta;
                }
                // now we can have some rest that we will give to those who have the higher rest
                while (freeRedistributed < free) {
                    int maxRestIndex = 0;
                    for (int col = 1; col < rests.length; col++) {
                        if (rests[col] > rests[maxRestIndex]) {
                            maxRestIndex = col;
                        }
                    }
                    rests[maxRestIndex] = 0;
                    freeRedistributed++;
                    housing[maxRestIndex]++;
                }
                if (j == 0) {
                    // initial housing
                    housings.put(munID, housing);
                } else {
                    // scenario
                    File housingFile = new File("dataIn/Auvergne/" + munID + "/dwelling.csv");
                    BufferedWriter housingWriter = new BufferedWriter(new FileWriter(housingFile));
                    housingWriter.write("#empty line\n");
                    housingWriter.write("1999");
                    for (Integer h : housing) {
                        housingWriter.write(";");
                        housingWriter.write(h.toString());
                    }
                    housingWriter.write("\n");
                    housingWriter.close();
                }
            }
        }
        // workers
        Map<String, Integer[]> workers = new HashMap<String, Integer[]>(1320);
        w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/A_Prendre_Pour_Scenarios.xls"));
        sheet = w.getSheet(0);
        for (int i = 1; i < sheet.getRows(); i++) {
            if (sheet.getCell(0, i).getContents().equals("1990")) {
                String munID = new Formatter().format("%05d", Integer.parseInt(sheet.getCell(1, i).getContents())).toString();
                Integer[] counts = new Integer[24];
                for (int col = 0; col < counts.length; col++) {
                    counts[col] = Integer.parseInt(sheet.getCell(12 + col, i).getContents());
                }
                workers.put(munID, counts);
            }
        }

        // population
        w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/population1990.xls"));
        sheet = w.getSheet(2);
        CSVReader distancesReader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/MatriceDistanceEuclidienneAuv.txt"), ' ');
        distancesReader.readNext();
        File outsideProxJobFile = new File("dataIn/Auvergne/outside0/proxJob.csv");
        BufferedWriter outsideProxJobWriter = new BufferedWriter(new FileWriter(outsideProxJobFile));
        for (int munIndex = 1; munIndex < sheet.getRows(); munIndex++) {
            String munID = new Formatter().format("%05d", Integer.parseInt(sheet.getCell(0, munIndex).getContents())).toString();

            // village.txt
            String popSize = sheet.getCell(58, munIndex).getContents();
            File villageFile = new File("dataIn/Auvergne/" + munID + "/village.txt");
            BufferedWriter writer = new BufferedWriter(new FileWriter(villageFile));
            writer.write("#empty line\n");
            writer.write(popSize);
            writer.write("\t-1\t");
            Integer[] workersToWrite = workers.get(munID);
            for (int j = 0; j < workersToWrite.length; j++) {
                writer.write(workersToWrite[j].toString());
                writer.write("\t");
            }
            Integer[] housingToWrite = housings.get(munID);
            for (int col = 0; col < 4; col++) {
                writer.write(housingToWrite[col].toString());
                writer.write("\t");
            }
            String[] munXY = lambert.get(munID);
            writer.write(munXY[1]);
            writer.write("\t");
            writer.write(munXY[2]);
            writer.write("\t");
            Double[] interceptSlope = interceptsSlopes.get(munID);
            writer.write(interceptSlope[0].toString());
            writer.write("\t");
            writer.write(interceptSlope[1].toString());
            writer.write("\t");
            writer.write("\n");
            writer.flush();
            writer.close();

            // proxjob.csv
            File proxJobFile = new File("dataIn/Auvergne/" + munID + "/proxJob.csv");
            writer = new BufferedWriter(new FileWriter(proxJobFile));
            line = distancesReader.readNext();
            for (int col = 1; col < line.length; col++) {
                double distance = Double.parseDouble(line[col]);
                if (col != munIndex && (distance < 20000)) {
                    writer.write(munIDs.get(col - 1));
                    writer.write(";");
                }
            }
            if (random.nextInt(0, 9) > 8) {
                writer.write("outside0");
                outsideProxJobWriter.write(munID);
                outsideProxJobWriter.write(";");
            }
            writer.write("\n");
            writer.close();
        }
        outsideProxJobWriter.write("\n");
        outsideProxJobWriter.close();

        generateExoJobScenarios();
    }

    public static void generateExoJobScenarios() throws Exception {
        // read the population size for 1999 and 2006
        CSVReader reader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/popAuvergne90_99_06.csv"), ';');
        reader.readNext();
        Map<String, Integer[]> popSize = new HashMap<String, Integer[]>(1320);
        List<String> munIDs = new ArrayList<String>(1320);
        String[] line;
        while ((line = reader.readNext()) != null) {
            Integer[] sizeByYear = new Integer[2];
            String id = new Formatter().format("%05d", Integer.parseInt(line[0])).toString();
            //System.err.println("id "+id) ;
            sizeByYear[0] = new Integer(line[2]);
            sizeByYear[1] = new Integer(line[3]);
            popSize.put(id, sizeByYear);
            // store the list of mun IDs
            munIDs.add(id);
        }
        reader.close();
        // read the total offer of jobs for every villages for 1999 and 2006
        Workbook w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/A_Prendre_Pour_Scenarios.xls"));
        Sheet sheet = w.getSheet(0);
        Map<String, Integer[]> workers1999 = new HashMap<String, Integer[]>(1320);
        Map<String, Integer[]> workers2006 = new HashMap<String, Integer[]>(1320);
        for (int i = 1; i < sheet.getRows(); i++) {
            String munID;
            if (sheet.getCell(0, i).getContents().equals("1999")) {
                munID = new Formatter().format("%05d", Integer.parseInt(sheet.getCell(1, i).getContents())).toString();
                Integer[] counts = new Integer[24];
                for (int col = 0; col < counts.length; col++) {
                    counts[col] = Integer.parseInt(sheet.getCell(12 + col, i).getContents());
                }
                workers1999.put(munID, counts);
            }
            if (sheet.getCell(0, i).getContents().equals("2006")) {
                munID = new Formatter().format("%05d", Integer.parseInt(sheet.getCell(1, i).getContents())).toString();
                Integer[] counts = new Integer[24];
                for (int col = 0; col < counts.length; col++) {
                    counts[col] = Integer.parseInt(sheet.getCell(12 + col, i).getContents());
                }
                workers2006.put(munID, counts);
            }
        }
        for (Map.Entry<String, Integer[]> munID : workers1999.entrySet()) {
            // compute the endogeneous offer in service sector from the population size for 1999
            int nbActivities = 24;
            int[] endogeneousOfferedJob99 = new int[nbActivities];
            int[] exogeneousOfferedJob99 = new int[nbActivities];
            Arrays.fill(endogeneousOfferedJob99, 0);
            Arrays.fill(exogeneousOfferedJob99, 0);
            int[] endogeneousOfferedJob06 = new int[nbActivities];
            int[] exogeneousOfferedJob06 = new int[nbActivities];
            Arrays.fill(endogeneousOfferedJob06, 0);
            Arrays.fill(exogeneousOfferedJob06, 0);
            int nbProfessions = 6;
            int serviceThreshold = nbProfessions * 3;
            int totOfferService99 = 0;
            int currentTotService99 = 0;
            int totOfferService06 = 0;
            int currentTotService06 = 0;
            for (int i = 0; i < nbActivities; i++) {
                if (i >= serviceThreshold) {
                    currentTotService99 += munID.getValue()[i];
                    currentTotService06 += workers2006.get(munID.getKey())[i];
                }
            }
            FileReader villageFile = new FileReader("dataIn/Auvergne/" + munID.getKey() + "/village.txt");
            char sep = '\t';
            reader = new CSVReader(villageFile, sep);
            reader.readNext();
            line = reader.readNext();
            double slope = new Double(line[33]).doubleValue();
            double intercept = new Double(line[32]).doubleValue();
            reader.close();
            totOfferService99 = (int) Math.round(((Math.log(popSize.get(munID.getKey())[0]) * slope) + intercept) * popSize.get(munID.getKey())[0]);
            totOfferService06 = (int) Math.round(((Math.log(popSize.get(munID.getKey())[1]) * slope) + intercept) * popSize.get(munID.getKey())[1]);
            if (totOfferService99 < 0) {
                totOfferService99 = 0;
            }
            if (totOfferService06 < 0) {
                totOfferService06 = 0;
            }
            //System.err.println("munID " + munID + " total offre service " + totOfferService99 + " pop size " + popSize.get(munID)[0] + " slope " + slope + " intercept " + intercept);
            if (totOfferService99 > 0) {
                for (int i = serviceThreshold; i < nbActivities; i++) {
                    if (munID.getValue()[i] == 0) {
                        endogeneousOfferedJob99[i] = (int) Math.round(totOfferService99 * 0.001);
                    } else {
                        endogeneousOfferedJob99[i] = Math.round(totOfferService99 * ((float) munID.getValue()[i] / (float) currentTotService99));
                    }
                    //System.err.println("i " + i + " total offre service " + totOfferService99 + " workers99 " + workers1999.get(munID)[i] + " currentTotServ " + currentTotService99 + " endog " + endogeneousOfferedJob99[i]);
                }
            }
            if (totOfferService06 > 0) {
                for (int i = serviceThreshold; i < nbActivities; i++) {
                    if (workers2006.get(munID.getKey())[i] == 0) {
                        endogeneousOfferedJob06[i] = (int) Math.round(totOfferService06 * 0.001);
                    } else {
                        endogeneousOfferedJob06[i] = Math.round(totOfferService06 * ((float) workers2006.get(munID.getKey())[i] / (float) currentTotService06));
                    }
                }
            }
            // compute the exogeneous offer (the scenario for job offer) from the endogeneous and the total offer
            for (int i = 0; i < nbActivities; i++) {
                exogeneousOfferedJob99[i] = munID.getValue()[i] - endogeneousOfferedJob99[i];
                exogeneousOfferedJob06[i] = workers2006.get(munID.getKey())[i] - endogeneousOfferedJob06[i];
            }
            //System.err.println(munID+" \t"+exogeneousOfferedJob99[17]+" \tworkers99\t "+workers1999.get(munID)[17]+" \tendog 99\t "+endogeneousOfferedJob99[17] ) ;
            // write down the scenario file for one municipality and the two dates 1999 (write 1991 at the beginning of the period) and 2006 (written 2000 at the beginning of the period)
            File exogJob = new File("dataIn/Auvergne/" + munID.getKey() + "/exogJobOffers.csv");
            BufferedWriter writer = new BufferedWriter(new FileWriter(exogJob));
            writer.write("Date and Total job offers expressed in quantity of offers\n");
            writer.write("1991");
            for (int j = 0; j < nbActivities; j++) {
                writer.write(";" + exogeneousOfferedJob99[j]);
            }
            writer.write("\n");
            writer.write("2000");
            for (int j = 0; j < nbActivities; j++) {
                writer.write(";" + exogeneousOfferedJob06[j]);
            }
            writer.write("\n");
            writer.flush();
            writer.close();
        }
    }
}