/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.auvergne.parameters;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.parameters.ConfiguredByFile;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service = Value.class)
public class AgeEnterOnLabourMarket implements Value<Integer, Individual>, ConfiguredByFile {

    private String filename;
    private Character separator;
    private transient List<Integer> ages;
    private transient List<Double>[] probasByAgeByCSP;

    @Override
    public void init(Application application) throws BadDataException {
        try {
            ages = new ArrayList<Integer>();
            String[] data;
            CSVReader reader = CSV.getReader(application.getFileFromRelativePath(filename), separator);
            while ((data = CSV.readLine(reader)) != null) {
                ages.add(Integer.parseInt(data[0]));
                if (probasByAgeByCSP == null) {
                    probasByAgeByCSP = new List[data.length - 1];
                    for (int i = 0; i < probasByAgeByCSP.length; i++) {
                        probasByAgeByCSP[i] = new ArrayList<Double>();
                    }
                }
                if (data.length - 1 != probasByAgeByCSP.length) {
                    Logger.getLogger(AgeEnterOnLabourMarket.class.getName()).warning("Input data line doesn't have the same size");
                }
                for (int i = 1; i < data.length; i++) {
                    double proba = Double.parseDouble(data[i]);
                    probasByAgeByCSP[i - 1].add(proba);
                }
            }
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("Error while loading "+filename, ex);
        }
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public Integer getValue(Individual individual) {
        if (individual.getAge() >= ages.get(ages.size() - 1)) {
            return individual.getAge() + 1;
        } else {
            int prof = individual.getParent1().getProfession();
            if (prof == -1) { // if the leader doesn't have a SCP
                for (Individual ind : individual.getHousehold().getAdults()) {
                    if (ind.getProfession() != -1) {
                        prof = ind.getProfession();
                        break;
                    }
                }
                if (prof == -1) {
                    prof = individual.getHousehold().getRandom().nextInt(0, individual.getHousehold().getMyVillage().getMyRegion().getNbSPC() - 1);
                }
            }
            List<Double> probas = probasByAgeByCSP[prof];
            return ages.get(individual.getHousehold().getRandom().nextIndexWithDistribution(probas));
        }
    }
}
