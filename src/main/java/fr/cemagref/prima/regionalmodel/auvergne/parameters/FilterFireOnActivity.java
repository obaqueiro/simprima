/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.auvergne.parameters;

import com.thoughtworks.xstream.XStream;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import java.util.Arrays;
import org.openide.util.lookup.ServiceProvider;

/**
 * Activities indexes given as parameter will indicate that the method {@code Municipality.fire()}
 * should not process the individual. Example of XML parametrization:
 * <pre>
 * &lt;fr.cemagref.prima.regionalmodel.auvergne.parameters.FilterFireOnActivity>
 *  &lt;excludedValues>
 *    &lt;int>0&lt;/int>
 *    &lt;int>2&lt;/int>
 *  &lt;/excludedValues>
 * &lt;/fr.cemagref.prima.regionalmodel.auvergne.parameters.FilterFireOnActivity>
 * </pre>
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class FilterFireOnActivity implements Value<Boolean, Integer> {

    private Integer[] excludedValues=null;

    @Override
    public void init(Application application) throws BadDataException {
    }

    @Override
    public Boolean getValue(Integer activity) {
        if (excludedValues==null) { excludedValues = new Integer[0];}
        final int result = Arrays.binarySearch(excludedValues, activity);
        return result >= 0 && result < excludedValues.length ;
    }
}
