/*
 *  Copyright (C) 2010 sylvie.huet
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.dynamics;

import fr.cemagref.prima.regionalmodel.Household;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

public abstract class EmigrationUpdater extends Dynamics<MunicipalitySet> {

    protected final static transient Logger LOGGER = Logger.getLogger(EmigrationUpdater.class.getSimpleName());
    public transient int emigrantsHH;
    public transient int emigrants;

    @Override
    public void step(final MunicipalitySet municipalitySet, final int iter) throws ProcessingException {
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            for (Household hh : mun.getMyHouseholds()) {
                if (hh.isJustSuppressed()) {
                    continue;
                }
                emigration(hh);
                // if the household was not suppresed (no emigration) 
                if (!hh.isJustSuppressed()) {
                    continue;
                }
                emigrantsHH++;
                emigrants = emigrants + hh.size();                
            }
        }

    }

    protected abstract void emigration(Household house);

    @ServiceProvider(service = EmigrationUpdater.class)
    public static class DefaultEmigrationUpdater extends EmigrationUpdater {

        private float probToQuitRegion;

        @Override
        protected void emigration(final Household house) {
            boolean leave = false;
            // If the household is suppresed or is a transit household then do nothing
            if (house.isJustSuppressed() || house.getResidence().isTransit()) {
                return;
            }

            if (house.getRandom().nextDouble() <= probToQuitRegion) {
                leave = true;
            }
            if (leave) {
                house.getMyVillage().getCounters().incNbHouseholdMoveOut(1);
                house.getMyVillage().getCounters().incNbIndividualMoveOut(house.size());
                house.getMyVillage().suppressHousehold(house,  true);
            }
        }
    }
}
