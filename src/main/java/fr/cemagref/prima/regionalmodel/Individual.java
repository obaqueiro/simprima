/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.parameters.IntValueFromExponentialLaw;
import fr.cemagref.prima.regionalmodel.parameters.IntValueFromNormalLaw;
import fr.cemagref.prima.regionalmodel.parameters.Parameters;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.util.ArrayList;
import java.util.List;
import umontreal.iro.lecuyer.probdist.ExponentialDistFromMean;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 * @author Sylvie Huet <sylvie.huet@cemagref.fr>
 * @author Omar Baqueiro Espinosa <baqueiro@iamo.de>
 */
public class Individual {

    private static int cpt = 0;
    private int id;

    public static enum Status {

        STUDENT(0), WORKER(1), UNEMPLOYED(2), RETIRED(3), INACTIVE(4);
        private  final int index;

        static {
        
        }
        
        Status(int index) {
            this.index = index;
        }

        public int index() {
            return index;
        }
        public static Status valueOf(int index){
            for (Status s : Status.values()){
                if (index == s.index){
                    return s;
                }
            }
            assert false:"The provided Status index is invalid";
            return null;
        }
    }
    /**
     * age of the individual
     */
    private byte age;
    private boolean adult;
    /**
     * status of the individual
     */
    private Status status;
    /**
     * This individual status should be changed to inactive at the end of the step
     */
    private transient boolean willBeInactive = false;   
    /**
     * Age at which the individual is going to enter on the labour market
     * It is picked out when it was born
     */
    private transient int ageToEnterOnTheLabourMarket;
    
    /** Activity performed by the individual */
    private Activity currentActivity;
    /** Next activity that the individual is going to find a place to adopt*/
    private transient Activity  nextActivity;
    /**
     * Searched profession (corresponds to a profession number or to -1 if the individual does not search for a job)
     */
    private int searchedProf;

    public int getSearchedProf() {
        return searchedProf;
    }

    public void setSearchedProf(int searchedProf) {
        this.searchedProf = searchedProf;
    }
    /** Previous activity that the individual is going to find a place to work */
    private transient Activity previousActivity;
    /** Place of Work of the previous activity*/
    protected transient Municipality previousJobLocation;
    /** Place of Work of the individual for current Activity*/
    protected Municipality currentJobLocation;
    /** Indicating at a given time if each member looks for a job*/
    private transient boolean lookForJob;
    
    /**
     * Household from which the individual is a member
     */
    private transient Household myHousehold;
    private transient int migrationYear = -1;
    private transient int lastFiringYear = -1;
    private transient Municipality lastFiringMun = null;
    private transient int lastSplittingYear = -1;
    private transient int lastJoiningYear = -1;
    private transient int lastMovingYear = -1;
    private transient int becomeAdultYear = -1;
    private transient Individual parent1 = null;
    private transient Individual parent2 = null;

    public void setParent1(final Individual parent1) {
        this.parent1 = parent1;
    }

    public long getId() {
        return id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status val) {
        this.status = val;
    }

   
    public byte getAge() {
        return age;
    }

    public boolean isAdult() {
        return adult;
    }
    public void isAdult(boolean ad){
        this.adult = ad;
    }

    private void setAge(int val) {
        if (val >= 127) {
            //this.age = Byte.MAX_VALUE; // Attention Byte.MAX_VALUE = -128
            this.age = 127;
            //throw new IllegalArgumentException("age can't be greater than " + Byte.MAX_VALUE);
        } else {
            this.age = (byte) val;
        }
    }

    public Random getRandom() {
        return getHousehold().getMyVillage().getMyRegion().getMyApplication().getRandom();
    }

    /**
     * Returns the profession ID or -1 if undefined.     
     * @return id corresponding to the profession
     */
    public int getProfession() {
        
        return this.getCurrentActivity().getSPC();
    }

    public void setProfession(int profess) {        
        assert getCurrentActivity()!=null : "Current activity must NOT be null!";
        this.getCurrentActivity().setSPC(profess);
        
    }
       
    /**
     * Age at which the individual go on retirement
     */
    private transient int ageToGoOnRetirement;

    public int getAgeToGoOnRetirement() {
        return ageToGoOnRetirement;
    }

    public void setAgeToGoOnRetirement(int AgeToGoOnRetirement) {
        this.ageToGoOnRetirement = AgeToGoOnRetirement;
    }
    /**
     * Age of death
     */
    private transient int ageToDie;

    public int getAgeToDie() {
        return ageToDie;
    }

    public void setAgeToDie(int AgeDie) {
        if (this.age > AgeDie)
            this.ageToDie = AgeDie;

        this.ageToDie = AgeDie;
    }

    public int getAgeToEnterOnTheLabourMarket() {
        return ageToEnterOnTheLabourMarket;
    }

    public void setAgeToEnterOnTheLabourMarket(final int ageToEnterOnTheLabourMarket) {
        this.ageToEnterOnTheLabourMarket = ageToEnterOnTheLabourMarket;
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public int getSectorOfActivity() {        
        return currentActivity.getSoA();                
    }

    public void setCurrentActivity(final Activity  newActivity) {      
        this.currentActivity = newActivity;
    }

    public Activity getNextActivity() {
        return nextActivity;
    }

    public void setNextActivity(final Activity nextActivity) {
        this.nextActivity = nextActivity;
    }

    public Activity getPreviousActivity() {
        return previousActivity;
    }

    public void setPreviousActivity(final Activity previousActPat) {
        this.previousActivity = previousActPat;
    }

    public Municipality getPreviousJobLocation() {        
        return previousJobLocation;
    }

    public void setPreviousJobLocation(final Municipality newPlaceOfWork) {
        this.previousJobLocation = newPlaceOfWork;       
    }

    public Municipality getCurrentJobLocation() {
        return currentJobLocation;
    }

    public void setCurrentJobLocation(final Municipality PlacesOfWork) {
        currentJobLocation = PlacesOfWork;
    }

    public Household getHousehold() {
        return myHousehold;
    }

    public void setMyHousehold(Household myHousehold) {
        this.myHousehold = myHousehold;
    }

    public int getMigrationYear() {
        return migrationYear;
    }

    public void setMigrationYear(int migrationYear) {
        this.migrationYear = migrationYear;
    }

    public Municipality getLastFiringMun() {
        return lastFiringMun;
    }

    public void setLastFiringMun(Municipality lastFiringMun) {
        this.lastFiringMun = lastFiringMun;
    }

    public int getLastFiringYear() {
        return lastFiringYear;
    }

    public void setLastFiringYear(int lastFiringYear) {
        this.lastFiringYear = lastFiringYear;
    }

    public int getLastJoiningYear() {
        return lastJoiningYear;
    }

    public void setLastJoiningYear(int lastJoiningYear) {
        this.lastJoiningYear = lastJoiningYear;
    }

    public int getLastSplittingYear() {
        return lastSplittingYear;
    }

    public void setLastSplittingYear(int lastSplittingYear) {
        this.lastSplittingYear = lastSplittingYear;
    }

    public int getLastMovingYear() {
        return lastMovingYear;
    }

    public void setLastMovingYear(int lastMovingYear) {
        this.lastMovingYear = lastMovingYear;
    }

    public int getBecomeAdultYear() {
        return becomeAdultYear;
    }

    public Individual getParent1() {
        return parent1;
    }

    public Individual getParent2() {
        return parent2;
    }

    public boolean isLookForJob() {
        return lookForJob;
    }

    public void setLookForJob(boolean lookForJob) {
        this.lookForJob = lookForJob;
    }

    public Object readResolve() {       
        return this;
    }

    /**
     * The constructor only takes an complete already defined individual as parameter to create a new individual:
     * - which remain the same (means the individual change of household)
     * - which is duplicate to represente a migrant
     * S. Huet 12.04.109
     */
    public Individual(final Individual ind, final boolean migrant) {
        if (migrant) {
            this.id = ++cpt;
            this.lookForJob = false;
            this.searchedProf = ind.getProfession();
            this.nextActivity = null;
            this.previousActivity = null;
            this.previousJobLocation = null;
        } else {
            this.id = ind.id;
            this.lookForJob = ind.lookForJob;
            this.searchedProf = ind.searchedProf;
            this.nextActivity = ind.getNextActivity();
            this.previousActivity = ind.getPreviousActivity();
            this.previousJobLocation = ind.getPreviousJobLocation();
            this.myHousehold = ind.getHousehold();
            this.migrationYear = ind.migrationYear;
            this.lastFiringMun = ind.lastFiringMun;
            this.lastFiringYear = ind.lastFiringYear;
            this.lastJoiningYear = ind.lastJoiningYear;
            this.lastSplittingYear = ind.lastSplittingYear;
            this.lastMovingYear = ind.lastMovingYear;
            this.becomeAdultYear = ind.becomeAdultYear;
        }
        setAge(ind.getAge());
        this.parent1 = ind.parent1;
        this.parent2 = ind.parent2;
        this.isAdult(ind.isAdult());
        this.ageToEnterOnTheLabourMarket = ind.getAgeToEnterOnTheLabourMarket();
        this.ageToGoOnRetirement = ind.getAgeToGoOnRetirement();
        if (ind.ageToDie !=0 && this.age > ind.ageToDie )
            this.ageToDie = ind.ageToDie;
        this.ageToDie = ind.ageToDie;
        this.status = ind.getStatus();
        this.currentActivity = ind.getCurrentActivity();
        this.currentJobLocation = ind.currentJobLocation;     
    }

    /**
     * Duplicate an individual for non-migrant. This constructor is equivalent to new Individual(ind, false).
     * @param ind
     */
    public Individual(Individual ind) {
        this(ind, false);
    }

    /**
     * The constructor of this class needs the age of the individuals and its status. It is to create a new individual at the birth time
     * S. Huet 6.04.09
     */
    public Individual(Household myHouseH, int age, Status status) {
        this.id = ++cpt;
        this.myHousehold = myHouseH;
        this.setAge(age);
        this.isAdult( false);
        this.status = status;
        this.searchedProf = -1;
        this.ageToEnterOnTheLabourMarket = -1;
        this.ageToGoOnRetirement = -1;
        List<Individual> adults = myHouseH.getAdults();
        this.parent1 = adults.get(0);
        this.parent2 = adults.get(1);
        initAgeToEnterOnLabourMarket();
        affectAFirstProfession();
        initAgeToGoOnRetirement();
        initAgeToDie();    
        currentJobLocation = getHousehold().getMyVillage();
    }

    /**
     * Initializes an individual 
     * @param age 
     */
    public Individual(int age){
        this.id = ++cpt;
        this.setAge(age);
        this.isAdult(false);
        this.status = Status.STUDENT;
         this.searchedProf = -1;
        this.ageToEnterOnTheLabourMarket = -1;
        this.ageToGoOnRetirement = -1;
        initAgeToEnterOnLabourMarket();
        affectAFirstProfession();
        initAgeToGoOnRetirement();
        initAgeToDie();    
        
    }
    /**
     * The constructor of this class needs the age of the individuals and the parameter managing the age
     * to become a worker (from a student status) [young] and the age to become a retirer [old]
     * Used at the initialisation time
     * S. Huet 28.07.09
     * Baqueiro 2012.01.17
     */
    public Individual(Household myHouseH, boolean isAdult, int age) {
        this.id = ++cpt;
        this.myHousehold = myHouseH;
        this.setAge(age);
        this.isAdult(isAdult);
        // assign parents to children (only in households where parents are available
        if (!isAdult && !getHousehold().getAdults().isEmpty()) {
             parent1 = getHousehold().getAdults().get(0);
            if (getHousehold().getHshType() == Household.Type.COUPLE_W_CHILDREN) {
                parent2 = getHousehold().getAdults().get(1);
            }
        }
        this.searchedProf = -1;
        setAgeToEnterOnTheLabourMarket(-1);
        setAgeToGoOnRetirement(-1);  
        currentJobLocation = getHousehold().getMyVillage();
        initStatusDoToAge(); // then certain become children and retirer (worker will be initialized later)
    }

    /**
     * Constructor used for migrants creation.
     * 
     * @param age
     * @param status
     * @param professions
     */
    public Individual(final boolean isAdult, final byte age, final  Status status, final Activity activity) {
        this.id = ++cpt;
        this.isAdult(isAdult);
        this.setAge(age);   
        this.status = status;
        setCurrentActivity(activity);
        this.searchedProf = -1;
    }

    @Override
    public String toString() {
        return "#" + id+ ";Age:"+this.age+";AgeToDie:"+this.ageToDie;
    }
   
    private Parameters getParameters() {
        return myHousehold.getMyVillage().getMyRegion().getParameters();
    }

    /**
     * update the individuals (age plus step year [step is a parameter as yound and old
     * defining respectively age to become a student and age to become a retirer] and status)
     * S. Huet 6.04.09
     */
    public void update() {
        setAge(getAge() + getHousehold().getMyVillage().getMyRegion().getIterationValue());
        updateStatusDoToAge();
    }

    /**
     * Promote to the adult status, create a new household in the same residence that the parents' one
     */
    public void becomeAdult() {
        Household parent = getHousehold();
        this.isAdult(true);
        this.becomeAdultYear = myHousehold.getMyVillage().getMyRegion().getCurrentYear();
       
        // if individual does not have a working status, enter labour market as unemployed
        if (getStatus()==Status.STUDENT){
            setStatus(Status.UNEMPLOYED);
            takeActivity(new Activity(getParameters().getFirstProfession(this)), this.getHousehold().getMyVillage());
        }
        
        List<Individual> listofmembers = new ArrayList<Individual>();
        Household newHsh = new Household(myHousehold.getMyVillage(), listofmembers, null);
        myHousehold.suppressMember(this, Household.MemberSuppressionReason.BECOME_ADULT);
        newHsh.addMember(this);        
        myHousehold.getMyVillage().getCounters().incNewHouseholdNb(1);
        myHousehold.getMyVillage().getMyHouseholds().add(newHsh);
        if (parent.getResidence().isTransit()) {
            newHsh.setTransitResidence();
        } else {
            newHsh.setResidence(parent.getResidence());
        }
        this.myHousehold = newHsh;
        parent.updateHshFeaturesWhenBecomingAdultForAFirstJob();
    }

    /**
     * update the status of the individuals regarding its age ; it enters on the labour market if it is ageToEnterOnTheLabourMarket;
     * it quits the labour market if it is ageToGoOnRetirement
     * the age is assessed via the parameter
     * S. Huet 6.04.09
     * Modified SH 12.02.2010
     */
    private void updateStatusDoToAge() {
        final Municipality vil = getHousehold().getMyVillage();
        if (getAge() == getAgeToEnterOnTheLabourMarket() && getStatus() == Status.STUDENT) {            
                becomeAdult();
                         
                return;            
        }

        if (getAge() >= getAgeToGoOnRetirement() && (getStatus() != Status.RETIRED)) {
            switch (getStatus()) {
                case UNEMPLOYED:
                    getHousehold().getMyVillage().getMyRegion().anualUnemployedToRetired++;
                    break;
                case WORKER:
                    leaveActivity();
                    break;

            }
            setStatus(Status.RETIRED);
            setNextActivity(null);
        }
        
    }

    /**
     * init the status of the individuals regarding its age
     * S. Huet 31.07.09
     * Modified SH 12.02.2010
     */
    private void initStatusDoToAge() {
        if (getAge() < getAgeToEnterOnTheLabourMarket()) {
            setStatus(Status.STUDENT);
            setCurrentActivity(null);
        } else {
            if (getAge() >= getAgeToGoOnRetirement()) {
                setStatus(Status.RETIRED);
                setCurrentActivity(null);
            } else { // all adult are unemployed at the beginning
                setStatus(Status.UNEMPLOYED);
                if (getCurrentActivity()==null){
                    setCurrentActivity(new Activity(getParameters().getProfIfUnactive(this)));
                }
            }
        }
    }

    /**
     * Utility method for get an age from a Parameter after a coherence check during
     * the initialization, with an exception for the normal law.
     * @param valueParameter
     * @return
     */
    private int getAgeAfterInitializationCoherenceChecking(Value<Integer, Individual> valueParameter) {
        int value = 0;
        int b = 50;
        int c = 0;
        if (valueParameter instanceof IntValueFromNormalLaw) {
            // specific case for normal distribution to be sure to get a value
            IntValueFromNormalLaw valueFromNormalLaw = (IntValueFromNormalLaw) valueParameter;
            value = valueFromNormalLaw.getValue(this);
            // After this upper bound, we have no chance to find a suitable age
            double max = valueFromNormalLaw.getMean() + (2 * valueFromNormalLaw.getStdDev());
            if (getAge() <= max) {
                while (value <= getAge()) {
                    value = valueFromNormalLaw.getValue(this);
                }
            } else {
                // this individual is too old for fitting in the distribution. Let's retire now!
                value = getAge() + 1;
            }
        } else {
            value = valueParameter.getValue(this);
            while (value <= getAge()) {
                value = valueParameter.getValue(this);
                c++;
                if (c == b) {
                    value = getAge() + 1;
                    break;
                }
            }
        }
        return value;
    }

    /**
     * init the age the individual is going to enter on the labour market (looking for a job)
     */
    public void initAgeToEnterOnLabourMarket() {
        setAgeToEnterOnTheLabourMarket(getParameters().getAgeEnterLabourMarket(this));
    }

    /**
     * This method compute again the age of some events to take into account the "real" status at the
     * initialisation time of the individual
     */
    public void updateAgeLabourMarket() {
        setAgeToEnterOnTheLabourMarket(getAgeAfterInitializationCoherenceChecking(getParameters().getAgeEnterLabourMarket()));
    }

    /**
     * init the age to go on retirement
     */
    public void initAgeToGoOnRetirement() {
        setAgeToGoOnRetirement(getParameters().getAgeToRetire(this));
    }

    /**
     * This method compute again the age of some events to take into account the "real" status at the
     * initialisation time of the individual
     */
    public void updateAgeRetirement() {
        setAgeToGoOnRetirement(getAgeAfterInitializationCoherenceChecking(getParameters().getAgeToRetire()));
    }

    /**
     * init the age to die
     * S. Huet 15.11.2010
     */
    public void initAgeToDie() {
        if (getParameters().getAgeToDie() instanceof IntValueFromExponentialLaw
          && (ExponentialDistFromMean.density(((IntValueFromExponentialLaw) getParameters().getAgeToDie()).getMean(), getAge()) < 0.003)) 
         {
            setAgeToDie(getAge() + 1);
            return;
        }        
        do {
            setAgeToDie(getParameters().getAgeToDie(this));
        } while (getAgeToDie() < getAge());
    }

    
    /**
     * Attribute a first profession to the individual just entering on the labour market
     * S. Huet 20.10.2010
     */
    public void affectAFirstProfession() {
        if (this.getCurrentActivity()==null){
            this.setCurrentActivity(new Activity(getParameters().getFirstProfession(this)));
        }
        else {
            setProfession(getParameters().getFirstProfession(this));
        }
    }
  
     /**
     * Method to init the status, the place of work and the pattern of individuals of the household The order of the pat table is the same than the one the individuals have been
     * stocked in the household list of members The format of data is described in a doc file "Instructions for the activity file.doc" S. Huet 08.10.2009
     */
    public void initIndividualActivities(String[] pat) {

                      
            this.setStatus(Status.valueOf(Integer.parseInt(pat[0])));
                        
            if (this.getStatus() != Status.STUDENT) {
                // this value is chosen because it will have no impact during the simulation time (the individual has already an activity)
                this.setAgeToEnterOnTheLabourMarket(this.getAge() - 1);
                
            }
            // if the individual is an adult wiht Student status then force status to Unemployed
            if (this.status==Status.STUDENT && myHousehold.getAdults().contains(this)){
                this.status = Status.UNEMPLOYED;                  
                 pat[1] =String.valueOf(getParameters().getFirstProfession(this));
            }
            switch (this.getStatus()) {
                case WORKER: 
                    // ensure that this worker is an adult
                    if (!this.isAdult() ) {
                        this.becomeAdult();
                    }
                    // by default it lives and works in the same municipality for all its activities
                    Municipality mun = getHousehold().getMyVillage();
                    if (!pat[1].equals(getHousehold().getMyVillage().getName())) {
                        // it works outside the village for all its activities
                        mun = mun.getMyRegion().getMunicipality(pat[1]);
                        if (mun == null) {
                            mun = getHousehold().getMyVillage().getMyRegion().getOutsides().get(0);
                            // FIXME non found municipalities are attributed to the first outside
                            mun.getLogger().warning("Municipality " + pat[1] + " has not been found. First outside region has been attributed as job location.");
                        }
                    }               
                    this.setCurrentJobLocation(mun);
                    this.setPreviousJobLocation(mun);          
                    
                    Activity act = new Activity(Integer.parseInt(pat[2]),Integer.parseInt(pat[3]));
                    this.setCurrentActivity(act);                                     
                    mun.addWorker(this);
                   
                                      
                   
                    break;
                case UNEMPLOYED:
                    // Create new unemploye dactivity (providing only the SPC
                    this.setCurrentActivity(new Activity(Integer.parseInt(pat[1])));          
                    this.setCurrentJobLocation(getHousehold().getMyVillage());   
                    break;
                case INACTIVE:
                    break;
                case STUDENT:
                    this.isAdult(false);
                 
                    break;
                default:
                    break;
            }
    }
    /**
     * Method looking at the next job state of the individual depending on the only transition matrix
     * to decide about the individual labour situation depending on the current one
     * (see the method lookAtTheLabourSituation() in Household to have more detail
     * Changed by S. Huet 21.01.2010
     *  Changed S. Huet 25.02.2010
     */
    public void pickTheNextJobSituationNew(Municipality vil) {
        this.willBeInactive = false;
        setLookForJob(false);
        setSearchedProf(-1);
        // If the next state does not immediately require a place to practice it (as a work), the individual is immediately updated with it
        switch (getStatus()) {
            case INACTIVE:
                if (remainInactive())
                 {
                    // the individual becomes unactive
                    becomeInactive(vil);
                } else { 
                    // the individual becomes unemployed and then it is directly going to look for an available job
                     
                    if (this.getCurrentActivity() != null) {
                        setLookForJob(true);
                        setSearchedProf(getParameters().getProfIfUnactive(this));                                                
                        currentJobLocation = getHousehold().getMyVillage();
                        becomeUnemployed(vil);                        
                    }
                }
                break;
            case UNEMPLOYED:
                if (getParameters().isInactiveAfterUnemployment(this)) {
                    // the individual becomes unactive
                    leaveActivity();
                    becomeInactive(vil);
                    // Count the total of individuals who went from Unemployed to Inactive
                    getHousehold().getMyVillage().getMyRegion().anualUnemployedToInactive++;
                } else { // the individual remains unemployed and then it is directly going to look for an available job
                    if (getHousehold().getResidence().isTransit()) {
                        setSearchedProf(getProfession());
                    } else {
                        setSearchedProf(getParameters().getProfIfUnactive(this));
                    }
                }
                break;
            case WORKER:
                if (getParameters().isInactiveAfterEmployment(this)) {
                    // the individual becomes unactive
                    leaveActivity();
                    becomeInactive(vil);
                     getHousehold().getMyVillage().getMyRegion().anualWorkerToInactive++;
                } else {
                    setSearchedProf(getParameters().getProfIfWorking(this));
                }
                break;
        }
        if (getSearchedProf() > -1) {
            setLookForJob(true);
        }
    }

    /**
     * Method making the individual inactive
     * S. Huet, 21.01.2010
     */
    private void becomeInactive(Municipality vil) {
        Municipality[] Places = new Municipality[1]; // no or his own
        // update the statut and the place of work (that is his own municipality in this case)        
        setCurrentJobLocation(vil);
        setStatus(Status.INACTIVE);        
        setNextActivity(null);
    }

    /**
     * Method making the individual unemployed
     * S. Huet, 21.01.2010
     */
    public void becomeUnemployed(Municipality vil) {
        getHousehold().getMyVillage().getMyRegion().becomeUnemploy++;        
        // update the statut and the place of work (that is his own municipality in this case)        
        setCurrentJobLocation(vil);
        setStatus(Status.UNEMPLOYED);
        setNextActivity(null);
        takeActivity(new Activity(this.getSearchedProf()), vil);
    }

    /**
     * Leaves the current activity of the individual and reduces the corresponding occupation counters
     * in the region.     
     */
    public void leaveActivity() {
      
        setPreviousActivity(getCurrentActivity());
        setPreviousJobLocation(getCurrentJobLocation());
        if (getStatus() == Status.WORKER) {
              assert currentActivity!=null:
                String.format("Individual does not have an activity to leave (currentActivity ==null). At:%s in %s ",
                this.toString(), this.myHousehold.toString());
            getCurrentJobLocation().removeWorker(this);           
            getHousehold().getMyVillage().getMyRegion().getLabourOffice().updateAvailabilities(getCurrentActivity(), getCurrentJobLocation());
        }
        setCurrentActivity(null);
    }

    /**
     * An individual takes a pattern, makes the corresponding activities more occupied and update the job location.
     */
    public void takeActivity(Activity newActivity, Municipality place) {      
        assert getStatus() == Status.WORKER || 
                getStatus() == Status.UNEMPLOYED || getStatus() == Status.INACTIVE:
                String.format("Error, an individual with Status %s cannot take an activity. For: %s; in:%s", 
                getStatus(), this.toString(), this.myHousehold.toString());                
        
        setCurrentActivity(newActivity);
        currentJobLocation = place;
        // Skip job initialization for individuals taking an unemployed acitivty. 
        if (newActivity.isUnemployed()) {
            return;
        }
        if (getPreviousActivity() != null) {
            getHousehold().getMyVillage().getMyRegion().stopBeingUnemployed++;
        }
        setPreviousActivity(null);
        setPreviousJobLocation(null);
        setStatus(Status.WORKER);

        currentJobLocation = place;
        place.addWorker(this);       
        getHousehold().getMyVillage().getMyRegion().getLabourOffice().updateAvailabilities(newActivity, place);

        if (!isAdult()) {
            becomeAdult();
        }
        

    }


    /**
     * Chooses a job from the list of found jobs based on a probability distribution
     * @param jobs The list of job propositions to choose from
     * @return The index of the chosen job
     */
    private LabourOffice.JobProposition chooseJob(List<LabourOffice.JobProposition> jobs) {
        final  Value<Integer, Individual> dynamic =
                myHousehold.getMyVillage().getMyRegion().getSectorChoiceScenarioDynamic(this.myHousehold.getMyVillage());
        int selectedSector;
        LabourOffice.JobProposition selJob = null;

        // select section based on dynamic (either default dynamic or scenario dynamic)
        if (dynamic != null) {
            selectedSector = dynamic.getValue(this);
        } else {
            selectedSector = getParameters().getSectorIfWorking().getValue(this);
        }

        // select one of the jobs with the defined sector from the list
        List<LabourOffice.JobProposition> jobsInSector = new ArrayList<LabourOffice.JobProposition>();
        for (LabourOffice.JobProposition prop : jobs) {
            if (prop.getActivity().getSPC()== selectedSector) {
                jobsInSector.add(prop);
            }
        }
        // if there are jobs in the preffered Sector are found, select one randomly
        if (!jobsInSector.isEmpty()) {
            selJob = jobsInSector.get(getRandom().nextInt(0, jobsInSector.size() - 1));
        } else {
            
                selJob = jobs.get(getRandom().nextInt(0, jobs.size() - 1));                        
        }


        return selJob;
    }

    public void seekJob(boolean onlyLocal) {
        final MunicipalitySet region = getHousehold().getMyVillage().getMyRegion();        
        // local search
        List<LabourOffice.JobProposition> myJob = 
                region.getLabourOffice().getLocalMunicipalitiesForProfession(getHousehold().getMyVillage(), getSearchedProf());

        // Decide whether this is going to be a local or global search.        
        onlyLocal = (region.getRandom().nextDouble() > region.getParameters().getProbLookingRegionalJobs());                    

        // Perform gloal search only if no job was found in local search and global search is available
        if ((myJob.isEmpty()) && !onlyLocal) {
            // searching in the region
            myJob = region.getLabourOffice().getMunicipalitiesForProfession(getSearchedProf());
        }
   
        if (myJob.isEmpty()) {
            setNextActivity(null);
        } else {
            LabourOffice.JobProposition chosen = chooseJob(myJob);

            setNextActivity(chosen.getActivity());
            leaveActivity();
            Municipality jobLocation = chosen.getJobsLocations()[0];
            takeActivity(getNextActivity(), jobLocation);


            //Determine the need of residence as a function of: 1. Residence ownership, 2. New job location.
            // Individual will only move if he is not a residence owner and not within commuting distance
            if (getHousehold().isResidenceOwner()) {
                getHousehold().setNeedsResidence(false);
            } else {
                // if the distance to the new job is greater than the commuting threshold then try to move
                if (region.getCommutingDistance(getHousehold().getMyVillage(), jobLocation)
                        > region.getParameters().getCommutingTimeThreshold()) {
                    getHousehold().setNeedsResidence(true);
                }
            }
        }
        setLookForJob(false);
    }
    

    
    /**
     * Decides whether an individual will remain inactive or enter the labour market.
     * The decision is made using either the default dynamic (from parameters) or 
     * a municipality dynamic defined by a scenario.
     * @return True if the individual remains inactive, false if it will enter the labour market
     */
    private boolean remainInactive(){   
        Value<Boolean,Individual> dynamic =
                myHousehold.getMyVillage().getMyRegion().getInactiveInactiveScenarioDynamic(this.myHousehold.getMyVillage());
        if (dynamic!=null){
            return dynamic.getValue(this);
        }
        else{
         return(getParameters().remainInactive(this));                    
        }
            
        
    }
}
