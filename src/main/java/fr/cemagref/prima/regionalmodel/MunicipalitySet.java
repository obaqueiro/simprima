/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;
import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.observation.kernel.ObservableManager;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.prima.regionalmodel.Individual.Status;
import fr.cemagref.prima.regionalmodel.altmark.observations.AltmarkObservableManager;
import fr.cemagref.prima.regionalmodel.parameters.MunicipalityParameters;
import fr.cemagref.prima.regionalmodel.parameters.OutsideParameters;
import fr.cemagref.prima.regionalmodel.parameters.Parameters;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import fr.cemagref.prima.regionalmodel.scenarios.Scenario;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.ArrayUtils;
/**
 * Class corresponding to the regional application defining attributes from the region and managing the whole application by the main loop (iteration())
 *
 * @author Sylvie Huet <sylvie.huet@cemagref.fr>
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalitySet {
    int nbBirth = 0;
    int nbDeath = 0;
    int annualNbBirth = 0;
    int annualNbDeath = 0;
    public int[] emigrantStatus = new int[8]; // in order: farmers, craftmen, executives, interm.prof., employees, workers, retired, other inactives
    public int[] immigrantStatus = new int[8]; // in order: farmers, craftmen, executives, interm.prof., employees, workers, retired, other inactives
    public int[] emigrantAge = new int[8];
    public int[] immigrantAge = new int[8];
    private static final Logger LOGGER = Logger.getLogger(MunicipalitySet.class.getSimpleName());
    protected List<Scenario> scenarios;
    /** Map containing the commuting distances between each pair of municipalities in the municipalityset
     * The key is the concatenation of the name of the Origin and the Destination municpalities.
     */
    protected SortedMap<String, Float> commutingDistances;
    /**
     * Labour office of the region where each individuals can be informed about the availability of the activity
     */
    private LabourOffice labourOffice;
    private RealEstateAgency realEstateAgency;
    public int getNbSoA() {
        return parameters.getNbSoA();
    }
    public int getNbSPC() {
        return parameters.getNbSPC();
    }
    public Random getRandom() {
        return getMyApplication().getRandom();
    }
    private void setNbProfessions(int nbProfess) {
        this.parameters.setNbTypesBySectorActiv(nbProfess);
    }
    public int getNbActivities() {
        return parameters.getNbActivities();
    }
    public LabourOffice getLabourOffice() {
        return labourOffice;
    }
    public int getNbOfUnactiveSituations() {
        return parameters.getNbOfUnactiveSituations();
    }
    public int getNbOfUnemployedSituations() {
        return parameters.getNbOfUnemployedSituations();
    }
    public double getProbaBirth() {
        return parameters.getProbaBirth();
    }
    public double getSplittingProbability() {
        return parameters.getSplittingProba();
    }
    public double getResSatisfactionMargin() {
        return parameters.getResSatisfacMargin();
    }
    /**
     * capacity of AvailRes give the number of room corresponding to a position in the table availability of residence of the village
     */
    private int[] capacityOfAvailRes;
    public int[] getCapacityOfAvailRes() {
        return capacityOfAvailRes.clone();
    }
    public void setCapacityOfAvailRes(int[] capacityOfAvailRes) {
        this.capacityOfAvailRes = capacityOfAvailRes.clone();
    }
    public void setCapacityOfAvailRes(int i, int val) {
        this.capacityOfAvailRes[i] = val;
    }
    public int getCapacityOfAvailRes(int i) {
        return capacityOfAvailRes[i];
    }
    public int getNbSizesResidence() {
        return parameters.getNbSizeRes();
    }
    public double getAvDiffAgeCouple() {
        return parameters.getAvDifAgeCouple();
    }
    public double getStDiffAgeCouple() {
        return parameters.getStDifAgeCouple();
    }
    public int getAgeMinToChild() {
        return parameters.getAgeMinHavingChild();
    }
    public int getAgeMaxToChild() {
        return parameters.getAgeMaxHavingChild();
    }
    public double getNbChildren() {
        return parameters.getNbChild();
    }
    public int getStartStep() {
        return parameters.getStartStep();
    }
    public int getNbIteration() {
        return parameters.getNbStep();
    }
    public int getNbMunicipalities() {
        return parameters.getMunicipalitiesParameters().size();
    }
    /**
     * Collection of all activities available in the region F.Gargiulo - 15.04.2009
     */
    private Activity[] allActivities;
    public Activity[] getAllActivities() {
        return allActivities;
    }
    public void setAllActivities(Activity[] allPatterns) {
        this.allActivities = allPatterns.clone();                
        
        
    }
              
    public int getIterationValue() {
        return parameters.getStep();
    }
    Application myApplication;
    public Application getMyApplication() {
        return myApplication;
    }
    public void setMyApplication(Application myApplication) {
        this.myApplication = myApplication;
    }
    /**
     * Temporary variable allowing to count the number of individual in a village S. Huet - 13.08.2009
     */
    private int countIndiv;
    public int getCountIndiv() {
        return countIndiv;
    }
    public void setCountIndiv(int countIndiv) {
        this.countIndiv = countIndiv;
    }
    /**
     * Collection of villages representing my region S. Huet - 7.04.2009
     */
    protected List<Municipality> myMunicipalities;

    /**
     * Provides the municipalities of this region.
     *
     * @return the unmodifiable list of municipalities
     */
    public List<Municipality> getMyMunicipalities() {
        return myMunicipalities;
    }
    /**
     * Particular Municipality representing cities ou village outside the region its resident are those working as external in the village of the rural region and not living in its
     * external workers are those living in the rural region and not working in S. Huet - 7.10.2009
     */
    private List<Municipality> outsides;
    public List<Municipality> getOutsides() {
        return outsides;
    }
    /**
     * Return the municipality (from inside or outside) given by its name, or null
     * if not found.
     * @param name of the municipality
     * @return The first municipality with the specified name or null if none is found
     */
    public Municipality getMunicipality(String name) {
        for (Municipality mun : myMunicipalities) {
            if (mun.getName().equals(name)) {
                return mun;
            }
        }
        for (Municipality mun : outsides) {
            if (mun.getName().equals(name)) {
                return mun;
            }
        }
        return null;
    }
    public Household findHouseholdById(long id) {
        Household hh = null;
        for (Municipality mun : myMunicipalities) {
            hh = mun.findHouseholdById(id);
            if (hh != null) {
                break;
            }
        }
        return hh;
    }
    public void setMyMunicipalities(List<Municipality> myMunicipalities) {
        this.myMunicipalities = myMunicipalities;
    }
    private Parameters parameters;
    public Parameters getParameters() {
        return parameters;
    }

    private ObservablesHandler regionObservable;
    private AltmarkObservableManager altmarkObserver;
    
    /**
     * Constructor used for tests
     */
    MunicipalitySet(Activity[] regionalActivities, Activity[] allActivities, Municipality... municipalities) throws BadDataException {
        this.parameters = new Parameters();
        setAllActivities(regionalActivities);
        List<Integer> professions = new ArrayList<Integer>();
        // compute nb of professions
        for (Activity activity : regionalActivities) {
            if (!professions.contains(activity.getSPC())) {
                professions.add(activity.getSPC());
            }
        }
        outsides = new ArrayList<Municipality>();
        setNbProfessions(professions.size());
        setMyMunicipalities(Arrays.asList(municipalities));
        labourOffice = new LabourOffice(this);
        labourOffice.initBase();
        labourOffice.initNetwork();

    }

    /**
     * Constructor used for tests
     */
   public MunicipalitySet(Application myAppli, Parameters parameters, List<Scenario> scenarios) throws IOException, BadDataException, ProcessingException {
        this.myApplication = myAppli;
        this.parameters = parameters;
        this.scenarios = scenarios;
        this.currentYear = getStartStep();
        finishToInitTheRegion();
        if (scenarios != null) {
            for (Scenario scenario : scenarios) {
                // init the scenario
                scenario.init(this);
            }
        }
        
        initInactiveInactiveScenarioDynamic();
        initSectorChoiceDynamic();
      //  parameters.getEmigration().init(this);
        regionObservable = ObservableManager.addObservable(MunicipalitySet.class);
        ObservableManager.initObservers();
        regionObservable.fireChanges(this, currentYear);
        // initalize observer for the altmark
        altmarkObserver = new AltmarkObservableManager(this, this.getParameters());
        altmarkObserver.fireChanges();    
        // Loads table with commuting distances between municipalities
        initCommutingDistances();     
    }
    public void run(int replic) throws ProcessingException {
        for (int iter = 0; iter <= getNbIteration(); iter++) {
            iteration(iter+1);
        }
        ObservableManager.closeObservers();
    }
    public void run() throws ProcessingException {
        run(1);
    }
    private void finishToInitTheRegion() throws IOException, BadDataException {
        initRegionalActivities();
        setMyMunicipalities(new ArrayList<Municipality>(getNbMunicipalities()));
        for (MunicipalityParameters munParameters : parameters.getMunicipalitiesParameters()) {
            getMyMunicipalities().add(new Municipality(this, munParameters.getMunicipalityName()));
        }
        labourOffice = new LabourOffice(this);
        labourOffice.initBase();
        labourOffice.initNetwork();
        realEstateAgency = new RealEstateAgency();
        if (parameters.getOutsideParameters().size() != 1) {
            throw new RuntimeException("TODO Model is built for supporting only one outside");
        }
        outsides = new ArrayList<Municipality>();
        for (OutsideParameters outsideParameter : parameters.getOutsideParameters()) {
            Municipality outside = new Municipality(this, outsideParameter.getName());
            outsides.add(outside);
            
            
            List<Municipality>proximityJob = new ArrayList<Municipality>();           
            for (String neighbour : outsideParameter.getNeighbours()) {
                // TODO index municipalities by their names in a map for better performance                
                for (Municipality mun : myMunicipalities) {
                    if (mun.getName().equals(neighbour)) {
                        proximityJob.add(mun);                        
                        break;
                    }
                }
            }
            outside.setProximityJob(proximityJob);
            initOutside(outsideParameter);
        }
        initCapacityAvailableResidence();
        for (int i = 0; i < parameters.getMunicipalitiesParameters().size(); i++) {
            MunicipalityParameters munParameters = parameters.getMunicipalitiesParameters().get(i);
            LOGGER.log(Level.FINER, "Loading data for municipality {0}", munParameters.getMunicipalityName());
            int countHousehold = initOneVillage(munParameters, i);
            Municipality mun = getMyMunicipalities().get(i);          
            // Recalculate the availability of the village 0 (working people
            // living and working in + people living outside
            // working in)
            Logger munLogger = mun.getLogger();
            if (munLogger.isLoggable(Level.FINE)) {
                StringBuilder munPrint = new StringBuilder("Init Municipality ").append(mun.getName()).append("\n");
                munPrint.append("  nb individuals=").append(getCountIndiv());
                munPrint.append(" against parameterize=").append(munParameters.getPopsize());
                munPrint.append(" nb households=").append(countHousehold).append("\n");
                munPrint.append("  ").append(mun.editAvailableRes()).append("\n");
                munPrint.append("  ").append(mun.editDemography(0)).append("\n");
                munPrint.append("  ").append(mun.editHouseholdDetailSizes()).append("\n");
                munPrint.append("  ").append(mun.editHouseholdDetailTypes());
                munLogger.fine(munPrint.toString());
            }
        }
        
        updateMunicipalityWorkers();
        // Fired people who have an employment which does not correspond to an offer
        // in the same loop initialise the parent from people who was parent at the initialisation time and only student
        // and then we can finish to init the individual calling the finishToInitMunicipality method
        for (Municipality municipality : getMyMunicipalities()) {
            LOGGER.log(Level.FINER, " processing mun {0}", municipality.getName());
            for (Household hh : municipality.getMyHouseholds()) {
                for (Individual ind : hh) {
                    if (ind.getParent1() == null && ind.getStatus() == Status.STUDENT) {                        
                        Individual parent=null;
                        for (int r=0;r<100;r++){
                            Municipality munic = getMyMunicipalities().get(getRandom().nextInt(0, (getNbMunicipalities() - 1)));
                            Household hsh = munic.getMyHouseholds().get(getRandom().nextInt(0, (munic.getMyHouseholds().size() - 1)));
                            parent = hsh.getAdults().get(0);
                            if (parent.getAge() >= (ind.getAge() + getAgeMinToChild())) {
                                break;
                            }
                        }                                                                        
                        ind.setParent1(parent);
                    }
                }
            }
            municipality.finishToInitMunicipality();
            municipality.createJobsForRegionWorkers(); // create missing jobs for workers within the region
            municipality.clearOccupiedActivititesByOutside();
            
            municipality.fire(true);
            getLabourOffice().updateAvailabilities(municipality);
        }
        for (Municipality outside : outsides) {
            outside.createJobsForRegionWorkers(); // create missing jobs for workers within the region
            outside.fire(true);
            getLabourOffice().updateAvailabilities(outside);
        }
        // try to find again a job to the one who has been fired
        LOGGER.log(Level.FINE, "Fired people now look for a new job");
        for (Municipality mun : getMyMunicipalities()) {
            for (Household hh : mun.getMyHouseholds()) {
                for (Individual ind : hh) {
                    if (ind.isLookForJob()) {
                        ind.seekJob(true); // true is going to say that the job search is only local
                    }
                }
            }
        }
        LOGGER.log(Level.FINE, "Initialisation of job occupation by outsiders");
        // Initialisation of occupation by outsiders equal to "total offers of job - occupied by resident - occupied by ext)
        int serviceThreshold = getNbSPC() * 3;      
       
        for (Municipality mun : getMyMunicipalities()) {

            for (Activity act : mun.getOccupiedActivitiesByOutside().keySet()){
                mun.setOccupiedActivitiesByOutside(act, 
                        Math.max(mun.getOfferedActivities(act)
                        - (int) mun.getOccupiedActivitiesByRes(act)
                        - (int) mun.getOccupiedActivitiesByExt(act),0));
            }            
            
            // computation of the initial part of the job offer dedicated to service to people living there and the other part which is exogeneous
            int[] offerM = new int[getNbActivities()];
            Arrays.fill(offerM, 0);
            int currentTotService = 0;
                        
            for (int i=0; i < getNbActivities();i++){
                Activity act = allActivities[i];
                if (i >= serviceThreshold) {
                    currentTotService += mun.getOfferedActivities(act);
                }
                i++;
            }
            
             int totOfferService = 
                    (int) Math.round((mun.getSlope() * (Math.log(mun.getPopulationSize()) + mun.getIntercept())) * mun.getPopulationSize());
                  
            if (totOfferService > 0) {
                for (int i = serviceThreshold; i < getNbActivities(); i++) {
                    Activity act = allActivities[i];
                    
                    int newJobs;
                    if (mun.getOfferedActivities(act)==0) {
                       newJobs= (int)Math.round(totOfferService * 0.001);                    
                    }
                    else{
                        newJobs=(int)
                                Math.round(totOfferService * ((float) mun.getOfferedActivities(act)) / (float) currentTotService);
                    }                    
                    mun.setEndogeneousOfferedJob(act,newJobs );

                }
            }
            
            for (Activity act: getAllActivities()){
                mun.setExogeneousOfferedJob(act, mun.getOfferedActivities(act)-mun.getEndogenousOfferedJob(act));
                assert mun.getExogenousOfferedJob(act)>=0 : "Exogenous offered jobs must be >= 0. Found "+mun.getExogenousOfferedJob(act);
            }

            getLabourOffice().updateAvailabilities(mun);
        }
   
        for (Municipality outside : outsides) {                
            for (int i = 0; i < getNbActivities(); i++) {
                outside.setExogeneousOfferedJob(allActivities[i], outside.getOfferedActivities(allActivities[i]) 
                        - outside.getEndogenousOfferedJob(allActivities[i]));
            }
            getLabourOffice().updateAvailabilities(outside);
        }
    }
    /**
     * Method initActivities First version: S. Huet, 24.04.2009 Changed S. Huet 08.10.2009
     */
    private void initRegionalActivities() {
        allActivities = new Activity[getNbActivities()];
        int k = 0;
        for (int j = 0; j < getNbSoA(); j++) {
            for (int i = 0; i < getNbSPC(); i++) {
                allActivities[k] = new Activity(j, i);
                k++;
            }
        }
    }  
    /**
     * Method initOneVillage NB : the villageNb allows the identification of the village First version: S. Huet, 7.04.2009
     */    
    private int initOneVillage(MunicipalityParameters parameters, int villageNb) throws IOException {
        Municipality municipality = getMyMunicipalities().get(villageNb);
        municipality.setOfferedActivities(parameters.getOfferedActivities());   
        
        // Initialize household
        int[][] myHsh = readPopFile(parameters.getPopsize(), parameters.getHouseholdsFilename());
        // Initialize individual patterns
        String[][] myIndivActiv = readActivityFile(getCountIndiv(), myHsh, parameters.getResidentsActivityFilename());
        CSVReader reader = CSV.getReader(parameters.getProxJobFile(), null);
        String[] proxJob = reader.readNext();
        reader.close();
        // Initialization of the accessible villages from the village villageNb      
        municipality.initVillage(parameters, countIndiv, myHsh, myIndivActiv, proxJob);
        return myHsh.length;
    }
    /**
     * Method initializing the OutsideRuralRegion village First version: S. Huet, 7.04.2009
     */
    private void initOutside(OutsideParameters parameters) {
        // Read the parameter of the village
        try {
            for (Municipality outside : outsides) {
                String livingPop[][] = new String[0][0];
                int[] availableResidence = new int[getNbSizesResidence()];
                for (int i = 0; i < getNbSizesResidence(); i++) {
                    availableResidence[i] = 0;
                }
                outside.initOutside(livingPop, availableResidence, parameters.getOfferedActivities());
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error during outside init", e);
        }
    }
    @Observable(description = "Total job offer for insiders comprised the outside offer")
    public int getJobOfferForInsidersWithOutside() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Activity act : allActivities) {
                size += municipality.getOfferedActivities(act) - municipality.getOccupiedActivitiesByOutside(act);
            }
        }
        for (Municipality municipality : getOutsides()) {
            for (Activity act : allActivities) {
                size += municipality.getOfferedActivities(act) - municipality.getOccupiedActivitiesByOutside(act);
            }
        }
        return size;
    }
    @Observable(description = "Population size")
    public int getPopulationSize() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            size += municipality.getPopulationSize();
        }
        return size;
    }

       
    @Observable(description = "Annual natural corrected balance (birth - death - killed orphans)")
    public int getAnnualNaturalBalanceWithKilledOrphans() {
        return annualNbBirth - annualNbDeath - totalOrphans;
    }
    @Observable(description="Number of Killed Orphans") public int getNbOrphans () {
        return totalOrphans;
    }
    @Observable(description="Anual Number of Death")
    public int getAnualNbDeath() {
        return annualNbDeath;
    }
    @Observable(description = "Annual natural balance (birth - death)")
    public int getAnnualNaturalBalance() {
        return annualNbBirth - annualNbDeath;
    }
    @Observable(description = "Annual Number of Births")
    public int getAnnualNbBirth() {
        return annualNbBirth ;
    }
    @Observable(description = "Nb birth")
    public int getNbBirth() {
        return nbBirth;
    }
    @Observable(description = "Nb death")
    public int getNbDeath() {
        return nbDeath;
    }
    @Observable(description = "Total natural balance (birth - death)")
    public int getTotalNaturalBalance() {
        return nbBirth - nbDeath;
    }
    @Observable(description = "Migration balance (in - out migrations)")
    public int getMigratoryBalance() {
        return getNbImmigrants() - parameters.getEmigration().emigrants;
    }
    @Observable(description = "Nb of out migrants")
    public int getNbOutMigrants() {
        return parameters.getEmigration().emigrants;
    }


    @Observable(description = "Nb of in migrants")
    public int getNbImmigrants() {
        int tot = 0;
        for (Municipality mun : getMyMunicipalities()) {
            tot = tot + mun.nbImmigrants;
        }
        return tot;
    }


    @Observable(description = "Nb of in OutSide")
    public int getNbOutsideMigrants() {
        int tot = 0;
        for (Municipality mun : getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbIndivdiualMoveOutside();
        }
        return tot;
    }

    @Observable(description = "Unemployed (w/o immigrants)")
    public int getUnemployedWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED) {
                            size++;
                        }
                    }
                }
            }
        }
        return size;
    }
    int nbRetiredF = 0;
    @Observable(description = "Nb Retired farmers this year)")
    public int getNbRetiredFarmersThisYear() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge() && ind.getProfession() == 0) {
                            size++;
                        }
                    }
                }
            }
        }
        nbRetiredF = nbRetiredF + size;
        return size;
    }
    @Observable(description = "Nb Retired farmers cumulated")
    public int getNbRetiredFarmersCumulated() {
        getNbRetiredFarmersThisYear();
        return nbRetiredF;
    }
    int nbRetiredC = 0;
    @Observable(description = "Nb Retired craftmen this year)")
    public int getNbRetiredCraftmenThisYear() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge() && ind.getProfession() == 1) {
                                size++;                          
                        }
                    }
                }
            }
        }
        nbRetiredC = nbRetiredC + size;
        return size;
    }
    @Observable(description = "Nb Retired craftmen cumulated")
    public int getNbRetiredCraftmenCumulated() {
        getNbRetiredCraftmenThisYear();
        return nbRetiredC;
    }
    int nbRetiredW = 0;
    @Observable(description = "Nb Retired workers this year)")
    public int getNbRetiredWorkerThisYear() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge() && ind.getProfession() == 5) {
                                size++;                            
                        }
                    }
                }
            }
        }
        nbRetiredW = nbRetiredW + size;
        return size;
    }
    @Observable(description = "Nb Retired workers cumulated")
    public int getNbRetiredWorkerCumulated() {
        getNbRetiredWorkerThisYear();
        return nbRetiredW;
    }  

    @Observable(description="Total Number of Free Jobs")
    public float getSumOfAvailableJobs() {
        float sumjobs=0;        
        for (Municipality mun : myMunicipalities) {            
                sumjobs+= mun.getSumFreeActivities();
        }
        return sumjobs;


    }
    @Observable(description = "Offered jobs")
    public int getOfferedJobs() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Activity act: getAllActivities()){
                size += municipality.getOfferedActivities(act);
            }
        }
        return size;
    }
  
    @Observable(description = "Occupied jobs by outside")
    public int getOccupiedJobsByOutside() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Integer values : municipality.getOccupiedActivitiesByOutside().values()) {
                size = size + (int) values;
            }
        }
        return size;
         
    }
   
    @Observable(description = "Unemployed farmers (w/o immigrants)")
    public int getUnemployedFarmersWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED && ind.getProfession() == 0) {
                                size++;                           
                        }
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "Unemployed craftmen (w/o immigrants)")
    public int getUnemployedCraftmenWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED && ind.getProfession() == 1) {
                                size++;
                        }
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "Unemployed executives (w/o immigrants)")
    public int getUnemployedExecutivesWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED && ind.getProfession() == 2) {
                                size++;                           
                        }
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "Unemployed intermediairy prof (w/o immigrants)")
    public int getUnemployedIntermProfWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED && ind.getProfession() == 3) {
                                size++;                            
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployed employees (w/o immigrants)")
    public int getUnemployedEmployeesWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED && ind.getProfession() == 4) {
                            size++;
                        }
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "Unemployed workers (w/o immigrants)")
    public int getUnemployedWorkersWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED && ind.getProfession() == 5) {
                                size++;                            
                        }
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "Unemployed (with immigrants)")
    public int getUnemployedWithImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household) {
                    if (ind.getAge() >= 16 && ind.getAge() < 65 && ind.getStatus() == Status.UNEMPLOYED) {
                            size++;                       
                    }
                }
            }
        }
        return size;
    }



    @Observable(description = "Students >=16)")
    public int getNbStudentsInEmploymentAge() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household) {
                    if (ind.getAge() >= 16 &&ind.getStatus()==Status.STUDENT) {
                            size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Nb of inactives")
    public int getNbInactives() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            if (municipality.getMyHouseholds() != null) {
                for (Household household : municipality.getMyHouseholds()) {
                    if (!household.isJustSuppressed()) {
                        for (Individual ind : household) {
                            if (ind.getAge() >= 16 && ind.getAge() < 65 && ind.getStatus() == Status.INACTIVE) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "Retired")
    public int getRetiredCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.RETIRED) {
                        size++;
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "potential immigrants")
    public int getNbPotentialImmigrants() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (household.getResidence().isTransit()) {
                    size += household.size();
                }
            }
        }
        return size;
    }
    @Observable(description = "Couples")
    public int getNbCouples() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit() 
                        &&(household.getHshType() == Household.Type.COUPLE) || (household.getHshType() == Household.Type.COUPLE_W_CHILDREN)) {
                        size++;                    
                }
            }
        }
        return size;
    }
    @Observable(description = "Nb Adults")
    public int getNbAdults() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                size += household.getAdults().size();
            }
        }
        return size;
    }
    @Observable(description = "Average Nb. Households per residence ")
    public int getNbHshByRes() {
        float size = 0;
        int size2 = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getResidencesTypeCount(); i++) {
                size += municipality.getResidenceOffer(i) - municipality.getFreeResidencesCount(i);
            }
            size2 += municipality.getMyHouseholds().size();
        }
        size = (float) size2 / size;
        return (int) size;
    }
    @Observable(description = "Nb res occupied)")
    public int getNbOccupiedRes() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getResidencesTypeCount(); i++) {
                size += municipality.getResidenceOffer(i) - municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }
    public int[] getPopulationSizeDetail() {
        int[] size = new int[5];
        // in the 4th position, we have the number of individual in age of procreation
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() <= 25) {
                            size[0]++;
                        } else {
                            if (ind.getAge() <= 45) {
                                size[1]++;
                            } else {
                                if (ind.getAge() <= 65) {
                                    size[2]++;
                                } else {
                                    size[3]++;
                                }
                            }
                        }
                        if (ind.getAge() >= getParameters().getAgeMinHavingChild()
                                && ind.getAge() <= getParameters().getAgeMaxHavingChild()) {
                            size[4]++;
                        }
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "Young population (age <= 25)")
    public int getYoungPopulationSize() {
        return getPopulationSizeDetail()[0];
    }
    @Observable(description = "Young population (age <= 45)")
    public int getMid1PopulationSize() {
        return getPopulationSizeDetail()[1];
    }
    @Observable(description = "Young population (age <= 65)")
    public int getMid2PopulationSize() {
        return getPopulationSizeDetail()[2];
    }
    @Observable(description = "Young population (age > 65)")
    public int getOldPopulationSize() {
        return getPopulationSizeDetail()[3];
    }


    @Observable(description = "Status of Active People")
    public String getActiveStatusTable() {
       
        SortedMap<String, Integer> hist = new TreeMap<String, Integer>();
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household) {
                    if (ind.getAge() >= 16 && ind.getAge() < 65) {
                        if (hist.containsKey(ind.getStatus().toString())) {
                            hist.put(ind.getStatus().toString(), hist.get(ind.getStatus().toString()) + 1);
                        } else {
                            hist.put(ind.getStatus().toString(), 1);
                        }
                    }
                }
            }
        }

        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, Integer> i : hist.entrySet()){
            builder.append(i.getKey()).append(":").append(i.getValue()).append(" / ");
        }
        return builder.toString();
    }

    @Observable(description="Poulation 16 to 64 Year Old")
    public int getPopulation16to64() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {

                    for (Individual ind : household) {
                        if (ind.getAge()>=16 && ind.getAge()<65) {
                            size++;
                        }
                    }                
            }
        }
        return size;
    }
    @Observable(description = "Annual nb birth by 1/2 individual in procreation age * 100")
    public int getAnnualNatalityLevel() {
        return (int) ((float) annualNbBirth / (float) (getPopulationSizeDetail()[4] / 2.0f) * 100);
    }
    @Observable(description = "Actives")
    public int getActivesCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED) {
                            size++;
                        }
                        if (ind.getStatus() == Status.WORKER) {
                            size++;
                        }
                    }
                }
            }
        }
        return size;
    }
    @Observable(description = "Occupied Socio-Professional-Categories")
    public String getOccupiedSPCTable() {        
        List<Integer> occSPC = new ArrayList<Integer>();
        for (int i=0;i < getNbSPC();i++) {
            occSPC.add(0);
        }

        for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus()==Status.WORKER) {
                            int numProf=ind.getProfession();
                            occSPC.set(numProf, occSPC.get(numProf)+1);
                        }
                    }
                }
            }
        }
        StringBuilder builder = new StringBuilder();
        for (Integer i: occSPC) {
            builder.append(i.toString()).append(" / ");
        }
        return builder.toString();
    }

    @Observable(description = "Workers with SPC1 (Managers and Senior Officials / Professional Occ.")
    public int getOccupiedSPC1() {
        int result=0;
          for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus()==Status.WORKER) {
                            int numProf=ind.getProfession();
                            if (numProf==0) result++;
                        }
                    }
                }
            }
        }
          return result;
    }
    @Observable(description = "Workers with SPC2 (Associate Professional and Tecnical)")
    public int getOccupiedSPC2() {
           int result=0;
          for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus()==Status.WORKER) {
                            int numProf=ind.getProfession();
                            if (numProf==1) result++;
                        }
                    }
                }
            }
        }
          return result;
    }

    @Observable(description = "Workers with SPC3 (Admin. and Secretarial / Skilled Trades ")
    public int getOccupiedSPC3() {
           int result=0;
          for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus()==Status.WORKER) {
                            int numProf=ind.getProfession();
                            if (numProf==2) result++;
                        }
                    }
                }
            }
        }
          return result;
    }

        @Observable(description = "Workers with SPC4 (Personal Service / Sales and Customer Serv.)")
    public int getOccupiedSPC4() {
           int result=0;
          for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus()==Status.WORKER) {
                            int numProf=ind.getProfession();
                            if (numProf==3) result++;
                        }
                    }
                }
            }
        }
          return result;
    }

              @Observable(description = "Workers with SPC5 (Process. Plant and Machine Operators)")
    public int getOccupiedSPC5() {
           int result=0;
          for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus()==Status.WORKER) {
                            int numProf=ind.getProfession();
                            if (numProf==4) result++;
                        }
                    }
                }
            }
        }
        return result;
    }

           @Observable(description = "Workers with SPC6 (Elementary Occupations)")
    public int getOccupiedSPC6() {
           int result=0;
          for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus()==Status.WORKER) {
                            int numProf=ind.getProfession();
                            if (numProf==5) result++;
                        }
                    }
                }
            }
        }
        return result;
    }


      @Observable(description = "Occupied Sector-Of-Activity")
    public String getOccupiedSoATable() {        
        final List<Integer>  occSoA = new ArrayList<Integer>();
        for (int i=0;i < getParameters().getNbSoA();i++) {
            occSoA.add(0);
        }

        for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus()==Status.WORKER) {
                            final int numProf=ind.getSectorOfActivity();
                            occSoA.set(numProf, occSoA.get(numProf)+1);
                        }
                    }
                }
            }
        }      
        final StringBuilder builder = new StringBuilder();
        for (Integer i: occSoA) {
            builder.append(i.toString()).append(" / ");
        }
        return builder.toString();        
    }

   
   
            
    @Observable (description="Population Age Distribution")
    public String getPopulationAgeHistogramTable () {
        int ages[] =new int[18];
        int ranges[] = {0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85};
          
        for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {             
                    for (Individual ind : household) {
                        int age = (int) ind.getAge();
                        int i=0;
                        for (;i<ranges.length;i++) {
                            if (age<ranges[i]) {
                                break;                                
                            }
                        }
                        ages[i-1]++;
                    }                
            }
        }

        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < ages.length;i++) {
            builder.append(String.valueOf(ages[i])).append("/");
        }
        return builder.toString();
    }


    @Observable(description = "Distribution of Ages for Death People")
    public String getDeathAgeHistogramTable() {        
        final SortedMap<Integer, Integer> hist = new TreeMap<Integer, Integer>();
        for (Individual ind : allSuppressed) {
            final int age = (int) ind.getAge();
            if (hist.containsKey(Integer.valueOf(age))) {
                hist.put(age, hist.get(age) + 1);
            } else {
                hist.put(age, 1);
            }
        }
    
        final StringBuilder builder = new StringBuilder();
        for (Map.Entry<Integer,Integer> i : hist.entrySet()){
            builder.append(i.getKey()).append(':').append(i.getValue());                        
        }
        return builder.toString();
   }

    @Observable(description = "Household Sizes Distribution")
    public String getHouseholdSizesTable() {
        int sizes[]= new int[7]; // 1,2,3,4,5,6,7+ residences
        for (Municipality mun : getMyMunicipalities()) {
            for (Household household : mun.getMyHouseholds()) {
           
                    int size = household.getSize();
                    if (size>sizes.length) {size=sizes.length-1;}
                    sizes[size-1]++;
                
            }
        }
        final StringBuilder builder = new StringBuilder();
        for (int i:sizes) {
            builder.append(String.valueOf(i)).append(" / ");
        }
        return builder.toString();
    }
     @Observable(description = "Available Residences Distribution")
    public String getAvailableResidencesTable() {
        String result = "";
        SortedMap<Integer, Integer> hist = new TreeMap<Integer, Integer>();
        for (Municipality mun : getMyMunicipalities()) {
            for (int i=0;i <mun.getResidencesTypeCount();i++){
                    int size = mun.getFreeResidencesCount(i)+1;
                    if (hist.containsKey(Integer.valueOf(i))) {
                        hist.put(i, hist.get(i) + 1);
                    } else {
                        hist.put(i, 1);
                    }
            }

        }
        
        final StringBuilder builder = new StringBuilder();
        for (Map.Entry<Integer,Integer> i : hist.entrySet()) {
            builder.append(i.getKey()).append(':').append(i.getValue()).append('/');                        
        }
        return builder.toString();
    }
    @Observable(description = "Occupied Actives")
    public int getActivesOccupiedCount() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                    for (Individual ind : household) {
                        //if (ind.getStatus() == Status.UNEMPLOYED) {
                        //size++;
                        //}
                        if (ind.getStatus() == Status.WORKER) {
                            size++;
                        }
                    }                
            }
        }
        return size;
    }

     @Observable(description = "Employed wihtout Out-commuters")
    public int getEmployedWithoutCommuteOutside() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.WORKER && !getOutsides().contains(ind.getCurrentJobLocation())) {
                            size++;
                        }
                    }
            }
        }
        return size;
    }


     @Observable(description = "Distribution of Work Places")
    public String getJobLocationsFrequencyTable() {        
        final SortedMap<String, Integer> hist = new TreeMap<String, Integer>();
        for (Municipality mun : getMyMunicipalities()) {
            for (Household hh : mun.getMyHouseholds()) {
                for (Individual ind : hh.getCopyOfMembers()) {
                    if (ind.getStatus() == Individual.Status.WORKER) {
                        final String workPlace = ind.getCurrentJobLocation().getName();

                        if (hist.containsKey(workPlace)) {
                            hist.put(workPlace, hist.get(workPlace) + 1);
                        } else {
                            hist.put(workPlace, 1);
                        }
                    }
                }
            }

        }
        final StringBuilder builder = new StringBuilder();
        for ( Map.Entry<String, Integer> i : hist.entrySet()) {
               builder.append(i.getKey()).append(':').append(i.getValue());
        }
        return builder.toString();
    }


    @Observable(description = "Unemployment rate on total actives")
    public int getUnemployementRate() {
        int size = 0;
        int size2 = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.UNEMPLOYED) {
                            size++;
                            size2++;
                        }
                        if (ind.getStatus() == Status.WORKER) {
                            size++;
                        }
                    }
                }
            }
        }
        size = (int) (((float) size2 / ((float) (size + size2))) * 100);
        return size;
    }
    @Observable(description = "Average size of households")
    public int getAverageSizeHsh() {
        int size = 0;
        int size2 = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            size2 += municipality.getMyHouseholds().size();
            for (Household household : municipality.getMyHouseholds()) {
                size += household.size();
            }
        }
        return (int) (((float) size / (float) size2) * 100);
    }
    @Observable(description = "% of active individuals profession = farmer")
    public int cspFarmer() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Status.RETIRED && ind.getStatus() != Status.STUDENT && ind.getStatus() != Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 0) {
                            size++;
                        }
                    }
                }
            }
        }
        //System.err.println("nombre de fermiers "+size) ;
        return (int) ((float) size / (float) tot * 100);
    }
    @Observable(description = "% of active individuals profession = craftmen")
    public int cspCraftmen() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Status.RETIRED && ind.getStatus() != Status.STUDENT && ind.getStatus() != Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 1) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }
    @Observable(description = "% of active individuals profession = executives")
    public int cspExecutives() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Status.RETIRED && ind.getStatus() != Status.STUDENT && ind.getStatus() != Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 2) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }
    @Observable(description = "% of active individuals profession = interm prof")
    public int cspIntermProf() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Status.RETIRED && ind.getStatus() != Status.STUDENT && ind.getStatus() != Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 3) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }
    @Observable(description = "% of active individuals profession = employee")
    public int cspEmployee() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Status.RETIRED && ind.getStatus() != Status.STUDENT && ind.getStatus() != Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 4) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }
    @Observable(description = "% of active individuals profession = worker")
    public int cspWorker() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Status.RETIRED && ind.getStatus() != Status.STUDENT && ind.getStatus() != Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 5) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }
    @Observable(description = "Available residences")
    public int getAvailableResidences() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (int i = 0; i
                    < getNbSizesResidence(); i++) {
                size = size + municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }
    @Observable(description = "Occupied residences")
    public int getOccupiedResidences() {
        float size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household hsh : municipality.getMyHouseholds()) {
                if (!hsh.getResidence().isTransit()&& hsh.getResidence().getNbHhResidents() > 0) {
                        size = size + (1.0f / (float) (hsh.getResidence().getNbHhResidents()));                    
                }
            }
        }
        return (int) Math.round(size);
    }
    @Observable(description = "Total residence offer")
    public int getResidenceOffer() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (int i = 0; i
                    < getNbSizesResidence(); i++) {
                size = size + municipality.getResidenceOffer(i);
            }
        }
        return size;
    }
    @Observable(description = "plop")
    public int plop() {
        return 2;
    }

    @Observable(description = "PopulationBirthRate(AnuanNbBirth/PopulationSize")
    public String getPopulationBirthRate() {
        return String.valueOf(getAnnualNbBirth()/ getPopulationSize());
    }

    @Observable(description = "PopulationBirthRate(AnuanNbDeath/PopulationSize")
    public String getPopulationDeathRate() {
        return String.valueOf(getAnualNbDeath()/ getPopulationSize());
    }

    @Observable(description = "Nb households with immigrant one")
    public int getHouseholdNb() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            size = size + municipality.getMyHouseholds().size();
        }
        return size;
    }
    @Observable(description = "Stop being unemployed during the year")
    public int getStopBeingUnemployed() {
        return stopBeingUnemployed;
    }
    int stopBeingUnemployed;

    int anualWorkerToInactive=0;

    @Observable(description = "NbUnemployedToInactive")
    public int getAnualWorkerToInactive() {
        return anualWorkerToInactive;
    }
    int anualUnemployedToInactive=0;
    @Observable(description = "NbUnemployedToInactive")
    public int getAnualUnemployedToInactive() {
        return anualUnemployedToInactive;
    }
        int anualUnemployedToRetired=0;
    @Observable(description = "NbUnemployedToRetired")
    public int getAnualUnemployedToRetired() {
        return anualUnemployedToRetired;
    }

    int anualUnemployedToEmployed=0;
    @Observable(description = "NbUnemployedToEmployed")
    public int getAnualUnemployedToEmployed() {
        return anualUnemployedToEmployed;
    }

    int anualNewCouples=0;
    @Observable(description = "Anual Number of New Couples")
    public int getAnualNewCouples() {
        return anualNewCouples;
    }



    @Observable(description = "Become unemployed during the year")
    public int getBecomeUnemploy() {
        return becomeUnemploy;
    }
    int becomeUnemploy;
    private List<Household> householdsToUpdate;
    public List<Household> getHouseholdsToUpdate() {
        return householdsToUpdate;
    }
    private int currentYear;
    public int getCurrentYear() {
        return currentYear;
    }
    
    int totalOrphans;
    int totalExpulsed;
    //int cherche ;
    //int trouve ;

    /**
     * Method containig the evolution algorithm for each village First version: F.Gargiulo 16/04/09 Added parameter "nbMunicipalities": S. Ternes 20/04/09 Changed: february 2010
     * (S. Huet)
     */
    public static int nbsupressed=0;
    public void iteration(int iter) throws ProcessingException {
        currentYear = getStartStep() + iter;
        parameters.updateTemporalValues(currentYear);
        LOGGER.log(Level.INFO, "BEGIN OF STEP {0}", currentYear);
        anualUnemployedToInactive=0;
        anualWorkerToInactive=0;
        stopBeingUnemployed = 0;
        anualUnemployedToEmployed=0;
        anualNewCouples=0;
        anualUnemployedToRetired=0;
        becomeUnemploy = 0;
        totalExpulsed = 0;
        annualNbBirth = 0;
        annualNbDeath = 0;
        totalOrphans = 0;
        allSuppressed.clear(); // list of individuals suppressed  due to age     
        parameters.updateTemporalValues(currentYear);
        LOGGER.log(Level.INFO, "BEGIN OF 1STEP {0}", currentYear);
        LOGGER.finer("nb unemployed before to start " + getUnemployedWithImmigrantsCount());
        for (Municipality mun : getMyMunicipalities()) {
            mun.resetCounters();    

        }
        
        if (scenarios != null) {
            // For each Municipality, applied the scenario and compute the
            // consequences on land use, jobs, services, individuals and householdsFilename
            for (Scenario scenario : scenarios) {
                scenario.step(this, iter);
            }
        }
        // Making dynamic the employment offer
        if (parameters.getDynamicServiceEmployment() != null) {
            parameters.getDynamicServiceEmployment().step(this, iter);
        } // Make a list given the order in which the individuals of the region
        // are going to be updated
        householdsToUpdate = new ArrayList<Household>();
        for (Municipality mun : getMyMunicipalities()) {
            householdsToUpdate.addAll(mun.getMyHouseholds());
            getLabourOffice().updateAvailabilities(mun);
        }
        getRandom().shuffle(householdsToUpdate);
        for (Household hh : householdsToUpdate) {
            // Define need of households regarding work - directly
            // applying the transition to inactivity making available some jobs
            // Employed individiduals can be designed as inactive
            if (!hh.isJustSuppressed() && !hh.getResidence().isTransit()) {
                hh.willingJobs();
            }
        }
        // manage the consequence of the change in availability of jobs
        // Fired people become unemployed only if they had not been already designed as inactive by the willingJobs method
        boolean activeFire = true;

        if (activeFire) {
            updateMunicipalityWorkers();
            for (Municipality municipality : getMyMunicipalities()) {
                municipality.fire(false); // false means that it is not the initialisation time
                getLabourOffice().updateAvailabilities(municipality);
            }
            for (Municipality municipality : getOutsides()) {
                municipality.fire(false);
                getLabourOffice().updateAvailabilities(municipality);
            }
        }
        // Emigration (going to live out of the bunch) and Suppression of the suppress household
        //parameters.getEmigration().step(this, currentYear);

        // Remove Students that go outside due to studies
        for (Household hh : householdsToUpdate){
         
            // ignore household in transit
            if (hh.getResidence().isTransit() ||hh.getSize()==1){
                continue;
            }
                                   
            //TODO-OMAR: Change age constant to input file DecisionByAgeAndProbas table
            for (Individual ind : hh.getCopyOfMembers()){
                assert ind.getHousehold() == hh : 
                        String.format("Individual myHousehold [%s] and Household [%s] do not correspond in %s",
                        ind.getHousehold().getId(),hh.getId(), hh.toString());
                if (hh.size()==1 || 
                        ind.getStatus() != Status.STUDENT ||  // ensure only students can leave
                        ind.getAge()<17){ // ensure sonly older students can leave 
                    continue;
                }
                
                if (getRandom().nextDouble() < getParameters().getProbabilityStudyOutside()) {
                    hh.suppressMember(ind, Household.MemberSuppressionReason.STUDY_ABROAD);
                }                             
            }
        }
        // Update household regarding work and residence
        int nbCouples = 0;
        int coupleFailures = 0;
        int nbDivorces = 0;
        for (Household hh : householdsToUpdate) { 

            if (!hh.getResidence().isTransit()) { // if it is not a potential immigrant
                death(hh); //death process in household
                birth(hh); // birth process in household
            }
            if (!hh.isJustSuppressed()) {
                hh.searchJobs(); // it is possible the hh has been already suppressed by the createCouple dynamics                          
            }
           
        }
        householdsToUpdate = new ArrayList<Household>();
        for (Municipality mun : getMyMunicipalities()) {
            householdsToUpdate.addAll(mun.getMyHouseholds());
        }
        getRandom().shuffle(householdsToUpdate);
        for (Household hh : householdsToUpdate) {      
                        //make couples
            if (hh.isJustSuppressed()) {
                continue;
            }
            if (getRandom().nextDouble() < probabilityToMakeCouple(hh)) {
                if (createNewCouple(hh, currentYear)) {
                    nbCouples++;
                    anualNewCouples++;
                } else {
                    coupleFailures++;
                }
            }
            if (!hh.getResidence().isTransit() && splitHousehold(hh)) {
                nbDivorces++; //divorces                   
            }
            // if the household need a residence due to a precedent event
            // or if its residence seems unapproriate (including other occupants)
            if (hh.isJustSuppressed()) {
                continue;
            }
            boolean needOfResidence = hh.needsResidence();
            boolean zeroWantLeave = true;
            if (!needOfResidence) {
                int appropriateSizeOfRes = hh.getMyVillage().appropriatedSizeOfRes(hh.size());
                for (Household roomate : hh.getResidence()) {
                    if (roomate.needsResidence()) {
                        zeroWantLeave = false;
                    } else if (roomate != hh) {
                        appropriateSizeOfRes += hh.getMyVillage().appropriatedSizeOfRes(roomate.size());
                    }
                }
                if (zeroWantLeave) {
                    needOfResidence = !satisfiedByLodging(appropriateSizeOfRes, hh.getResidence().getType());
                }
            }
            if (needOfResidence) {
                hh.setNeedsResidence(true);

                updateResidence(hh);
            }


        }
        // Emigration (going to live out of the bunch) and Suppression of the suppress household
        //parameters.getEmigration().step(this, currentYear);
        // Suppression of household updated
        for (Municipality mun : myMunicipalities) {
            for (Iterator<Household> it = mun.getMyHouseholds().iterator(); it.hasNext();) {
                Household hh = it.next();
                if (hh.isJustSuppressed()) {
                    hh.getMyVillage().releaseResidence(hh);
                    it.remove();
                }
            }
        }
        // Logging
        for (Municipality mun : getMyMunicipalities()) {
            nbBirth = nbBirth + mun.getCounters().getNumBirth();
            nbDeath = nbDeath + mun.getCounters().getNumDeath();
            totalOrphans = totalOrphans + mun.getCounters().getNbKilledOrphans();
            totalExpulsed = totalExpulsed + mun.getCounters().getNbHouseholdExpulsed();
            mun.updatePopSize();
      
        }
 
        regionObservable.fireChanges(this, currentYear);
        
        altmarkObserver.fireChanges();
        // Make people becoming older
        for (Municipality municipality : getMyMunicipalities()) {
            municipality.updateHouseholdAges();
        }        
        LOGGER.log(Level.INFO, "END OF STEP {0}", currentYear);
        
        
    }

    private int getCurrentPopSize(){
        int pop=0;
        for (Municipality mun : getMyMunicipalities()) {
            pop += mun.getCurrentPopSize();
        }
        return pop;
    }
    /**
     * Method findPartner: select a household which is not a couple one or not a complex one because we don't know about the dynamics of complex one. Also select an adult which respect a
     * given difference of ages inside the couple (parameters) Observation: we must check the sex for the adults in the future
     *
     * @return the found partner. Can be null if none has been found.
     * First looks in own municipality, then if not found in all households
     * First version: S. Ternes,
     * F. Gargiulo, 22.04.2009 Changed by S. Huet, 1.02.2010, by G. Deffuant 4/11/2010
     */
    private Individual findPartner(Household hsh) {
        // We assumethat hsh is single
        assert hsh.getAdults().size()==1 : 
                String.format("Individual must be single to find partner. Found %d individuals at %s",
                hsh.getAdults().size(),hsh.toString() );
        Municipality munic = hsh.getMyVillage();
     
        Municipality[] mun = Arrays.copyOf(munic.getProximityJob(), munic.getProximityJob().length + 1);
        mun[munic.getProximityJob().length] = munic;
        return findPartnerInMunicipalities(hsh, mun, parameters.getNbJoinTrials());
       
    }
    /**
     * Satisfaction regarding the dwelling size
     */
    private boolean satisfiedByLodging(int idealSizeOfRes, int actualTypeOfRes) {
        return (Math.abs(actualTypeOfRes - idealSizeOfRes) <= getResSatisfactionMargin());
    }
    /**
     * Finds the partner in a list of households
     */
    private Individual findPartnerInHouseholdList(Household hsh, List<Household> HHList, int maxTire) {
        int i = 0;
        Individual adult = hsh.getAdults().get(0);
        Household otherHH;
        boolean isTransit = hsh.getResidence().isTransit();
            Individual partner;
        for (int d = 0; d < maxTire; d++) {
                i = getRandom().nextInt(0, HHList.size() - 1);
                otherHH = HHList.get(i);
            if ((!isTransit) || (!otherHH.getResidence().isTransit())
                    && ((otherHH != hsh) && (!otherHH.isJustSuppressed()))) {
                // check the conditions to become a couple                
                partner = findPartnerInHousehold(adult, otherHH);
                if (partner != null) {
                    return partner;
                }
            }
        }
        return null;
    } /*
     * Finds a partner in a table of municipalities
     */


    private Individual findPartnerInMunicipalities(Household hsh, Municipality[] municipalities, int maxTire) {
        int tot = 0;      
        Individual partner;
        Household otherHH;
        boolean isTransit = hsh.getResidence().isTransit();
        Individual adult = hsh.getAdults().get(0);
        for (Municipality munic : municipalities) {
            tot += munic.getMyHouseholds().size();
        }
        if (tot == 0) {
            return null;
        }
        for (int d = 0; d < maxTire; d++) {
            otherHH = householdsToUpdate.get(getRandom().nextInt(0, householdsToUpdate.size() - 1));
            // skip household if is just suppressed or is same as current household
            if (otherHH.isJustSuppressed() || (otherHH == hsh)){
                continue;
            }
            // skip household if is both HH are residence (at least 1 household must be non-residence)
            if (isTransit && otherHH.getResidence().isTransit()){
                continue;
            }
            
            partner = findPartnerInHousehold(adult, otherHH);
            if (partner != null) {
                return partner;
            }

        }

        return null;
    }
    /**
     * gets the household of a given number in a list of municipalities, taking them in order
     */
    private Household getHouseholdInMunicipalities(Municipality[] municipalities, int i) {
        int sum = 0, diff;
        for (Municipality munic : municipalities) {
            if ((diff = i - sum) < munic.getMyHouseholds().size()) {
                return munic.getMyHouseholds().get(diff);
            }
            sum += munic.getMyHouseholds().size();
        }
        return null;
    }
    /**
     * Individual adult looks for a partner in Household other HH
     * First consider adult if single
     * second, consider children
     */
    private Individual findPartnerInHousehold(Individual adult, Household otherHH) {
        if (otherHH.singleAdult()) {
            Individual partn = otherHH.getAdults().get(0);
            if (matchingPartners(adult, partn)) {
                return partn;
            }
        }
        for (Individual partner : otherHH) {
            if (!partner.isAdult() && (partner.getAge() > 15) && (matchingPartners(adult, partner))) {
                return partner;
            }
        }
        return null;
    }
    /**
     * Conditions for two adults from single households to become a couple. In this case, only age
     */
    private boolean matchingPartners(Individual adult, Individual partner) {
        if (Math.abs(adult.getAge() - partner.getAge()) > getAvDiffAgeCouple() - getStDiffAgeCouple()
                && Math.abs(adult.getAge() - partner.getAge()) < getAvDiffAgeCouple() + getStDiffAgeCouple()) {
            return true;
        }
        return false;
    }
    /**
     * creation of new couple for a single household
     * First try to find a single household matching with age
     * if not found, with equal probability, duplicate the single adumt with difference of age
     * or leave the simulation
     */
    private boolean createNewCouple(Household hsh, int iter) {
        assert hsh.getAdults().size() <= 2 :
                String.format("Error - Household with > 2 [%d] adults in %s ",
                hsh.getAdults().size(), hsh.toString());
        Individual partner = findPartner(hsh);
        if (partner==null)
            return false;

  
        if (partner.getHousehold().getResidence().isTransit()) {
            for (Individual ind : partner.getHousehold()) {
                ind.setMigrationYear( getCurrentYear());
                // compute again the age to die if it has already passed
                if (ind.getAgeToDie() <= ind.getAge()) {
                    ind.initAgeToDie();
                }
            }
        }

        if (!partner.isAdult()) {
            partner.becomeAdult();
            partner.affectAFirstProfession();
        }

        hsh.getAdults().get(0).setLastJoiningYear(getCurrentYear());
        partner.setLastJoiningYear( getCurrentYear());
        Household partnerHH = partner.getHousehold();
        Household leavingHH = hsh;
        Municipality departure = hsh.getMyVillage();
        Household growingHH = partnerHH;
        
        // if outside, the household simply disappears
        if (getOutsides().contains(partnerHH.getMyVillage())) {
            hsh.getMyVillage().getCounters().incNbIndividualMoveOutside(hsh.getSize());      
            // supress household killing individuals (as we do not keep track of them anymore
             departure.suppressHousehold(leavingHH,  true); 
              return true;
        }
        // when the municipality where the partner lives is in the municipality network take the largest residence
        float partnerHHres = 0.0f;
        float hshRes = 0.0f;
        for (Household roomate : partnerHH.getResidence()) {
            if (!roomate.needsResidence()) {
                partnerHHres = partnerHHres + roomate.listOfMembers.size();
            }
        }
        partnerHHres = (partnerHH.getResidence().getType() + 1) / (partnerHHres + hsh.listOfMembers.size());
        for (Household roomate : hsh.getResidence()) {
            if (!roomate.needsResidence()) {
                hshRes = hshRes + roomate.listOfMembers.size();
            }
        }
        hshRes = (hsh.getResidence().getType() + 1) / (hshRes + partnerHH.listOfMembers.size());
        
        if (partnerHHres < hshRes) {        
            leavingHH = partnerHH;
            growingHH = hsh;
            departure = leavingHH.getMyVillage();
        }
        if (growingHH.getResidence().getNbHhResidents() > 1) {
            growingHH.setCoupleAtParents(true);
        }
        growingHH.updateHshFeaturesAfterJoining(iter);
        // We copy the members of our household into the found one
        for (Individual individual : leavingHH.getCopyOfMembers()) {          
            growingHH.addMember(individual);
                  
            // Skip individuals moving to a household within the same Municipality 
            // or inactive individuals
            if (growingHH.getMyVillage() == leavingHH.getMyVillage()
                    || individual.getCurrentActivity() == null) {
                continue;
            }
            
            if (individual.getStatus() == Status.UNEMPLOYED) {
                individual.setCurrentJobLocation(growingHH.getMyVillage());
                continue;
            } 
            // if not a worker then skip updating of occupied activities
            if (individual.getStatus() != Status.WORKER) {
                continue;
            }           

            if (individual.getCurrentJobLocation() == leavingHH.getMyVillage()) {
                // if moving out of the working location, then increase the municipality
                // occupied activity by External residents.
                leavingHH.getMyVillage().increaseOccupiedActivityExt(individual.getCurrentActivity(), 1);                
                individual.getCurrentJobLocation().increaseOccupiedActivityRes(individual.getCurrentActivity(), -1);
                continue;
          }
            if (individual.getCurrentJobLocation() == growingHH.getMyVillage()) {                
                growingHH.getMyVillage().increaseOccupiedActivityRes(individual.getCurrentActivity(), 1);
                growingHH.getMyVillage().increaseOccupiedActivityExt(individual.getCurrentActivity(), -1);                
            }

            
        }
        // update the counter of moving people
        Municipality arrival = growingHH.getMyVillage();
        arrival.getCounters().incNbHouseholdMoveIn(1);
        arrival.getCounters().incNbIndividualMoveIn(leavingHH.size());
        if (growingHH.getMyVillage() == leavingHH.getMyVillage()) {
            departure.suppressHousehold(leavingHH,  false);
        } else {
            // the houshold leave its village
            departure.getCounters().incNbHouseholdMoveOut(1);
            departure.getCounters().incNbIndividualMoveOut(leavingHH.size());
            departure.suppressHousehold(leavingHH,  false);        
        }

            assert hsh.getAdults().size() <= 2 :
                String.format("Error - Household with > 2 [%d] adults in %s ",
                hsh.getAdults().size(), hsh.toString());
        return true;
    }        
    
    List<Individual> allSuppressed = new ArrayList<Individual>();
    /**
     * Death process applied to a household.
     * If no adults remains in an household, the children are suppressed
     * from the system.
     */
    private void death(Household hsh) {
        if (hsh.isJustSuppressed()) {
            return;
        }
        //int orphans = 0;
        Municipality mun = hsh.getMyVillage();
        List<Individual> suppressed = hsh.death();
        mun.getCounters().incNumDeath(suppressed.size());
        allSuppressed.addAll(suppressed);

        if (suppressed.size() == hsh.size()) {
            // if all the members are dead => remove the household
            mun.suppressHousehold(hsh,  true);
        } else {
            for (Individual ind : suppressed) {
                hsh.suppressMember(ind, Household.MemberSuppressionReason.DEATH);
            }
            
            if (suppressed.isEmpty()) {
                return;
            }
            hsh.updateHshFeaturesAfterDeath();
            if (hsh.getAdults().isEmpty()) {
                //orphans+=hsh.getSize();
                mun.getCounters().incNbKilledOrphans(hsh.getSize());
                mun.suppressHousehold(hsh,  false);
            }
            
        }
    }
    /*
     * Birth: an household is enlarged
     * GD 11/2010
     */
    private void birth(Household hsh) {
        if (!hsh.isJustSuppressed()) {
            Municipality mun = hsh.getMyVillage();
            if (hsh.birth()) {
                mun.getCounters().incNumBirth(1);
                Individual newIndy = new Individual(hsh, 0, Status.STUDENT);
                if (newIndy.getAge() == newIndy.getAgeToDie()) {
                    // the child is dead at birth :-/
                    mun.getCounters().incNumDeath(1);
                } else {
                    // The household type is update by addMember
                    hsh.addMember(newIndy);
                    hsh.updateHshFeaturesAfterBirth();
                }
            }
        }
    }
    /**
     * splitting households by divorce: couple members quit each other (create a new household) for couple type 2 (couple without children) and 3 (with children) Each
     * individual leaves , the children remains with one of the adult
     */
    private boolean splitHousehold(Household hsh) {
       
        Municipality mun = hsh.getMyVillage();
        if (hsh.isJustSuppressed() 
                || !(hsh.getHshType()==Household.Type.COUPLE || hsh.getHshType() == Household.Type.COUPLE_W_CHILDREN )) {
                return false;
        }
                         
        if (!hsh.splitByDivorce()){
            return false;
        }
             
        List<Individual> adults = hsh.getAdults();
        assert adults.size()==2:"Household must always have at most 2 adults";
        adults.get(0).setLastSplittingYear(getCurrentYear());
        adults.get(1).setLastSplittingYear(getCurrentYear());

        // Leaving adult is selected randomly
        int leavingAdultIndex = this.getRandom().nextInt(0, 1);
                
        List<Individual> listofmembers = new ArrayList<Individual>();
        Individual leavingAdult = adults.get(leavingAdultIndex);
        listofmembers.add(leavingAdult);
        Household newHsh = new Household(mun, listofmembers, hsh.getResidence());
        newHsh.setNeedsResidence(true);
        if (hsh.isCoupleAtParents()) {
            hsh.setNeedsResidence(true);
        }
        mun.getCounters().incNewHouseholdNb(1);
        mun.getMyHouseholds().add(newHsh);
        hsh.suppressMember(leavingAdult, Household.MemberSuppressionReason.PARTNER_QUITTING);
        // children dispatching
        List<Individual> children = hsh.getChildren();
        int nbChildrenForLeavingInd = getRandom().nextInt(0, children.size());
        int[] indexes = getRandom().randomList(children.size());
        for (int i = 0; i
                < nbChildrenForLeavingInd; i++) {
            Individual child = children.get(indexes[i]);
            newHsh.addMember(child);
            hsh.suppressMember(child, Household.MemberSuppressionReason.PARTNER_QUITTING);
        }
        newHsh.updateHshFeaturesAfterDivorce(hsh);
        hsh.updateHshFeaturesAfterDivorce(newHsh);
        return true;

      
    }
    /**
     * probability to make a new couple
     * In this case is constant but could vary with age and number of children
     */
    private double probabilityToMakeCouple(Household hsh) {
        if (hsh.singleAdult()) {
            return parameters.getProbabilityToMakeCouple();
        }
        return 0;
    }
    public void updateResidence(Household hh) {
         realEstateAgency.findNewResidence(hh);
        if (hh.getNextResMunicipality() != null) {
            hh.getMyVillage().move(hh);
        }
    }
   
    /**
     * Initialise the capacity i term of number of people for each type of residence As the ideal size for an household follows in France the rule
     * (nbIndividualsOfTheHouseholds*1)+1 room then for a flat size of x + 1 = rooms, the ideal size of household is x individuals This method gives a result to know the
     * appropriate size of a residence for a given size of household First version: S. Huet, 28.07.2009
     */
    private void initCapacityAvailableResidence() {
        capacityOfAvailRes = new int[getNbSizesResidence()];
        for (int i = 0; i
                < getNbSizesResidence(); i++) {
            if (i == 0) {
                setCapacityOfAvailRes(i, 1);
            } else {
                setCapacityOfAvailRes(i, i);
            }
        }
    }
    /**
     * To print err the activity pattern
     */
    public String printAllPatternsOfActivity() {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i
                < getAllActivities().length; i++) {
            buf.append("\tpattern #").append(i).append(": ");
            buf.append(allActivities[i]);
        }
        return buf.toString();
    }
    /**
     * Read the pop file generated by the class StartPopulation Create a table of household for which each line contains the type of household in position 0 and the other positions
     * contains the age of the individuals of the household
     */
    public int[][] readPopFile(int popSize, final String nameOfFile) throws IOException {
        int[][] myHouseH = new int[popSize][];
        int countPop = 0;
        int countHousehold = 0;
        setCountIndiv(0);
        int size, type;
        FileReader myFile = new FileReader(nameOfFile);
        StreamTokenizer mySt = new StreamTokenizer(myFile);
        mySt.nextToken();
        while (mySt.ttype != StreamTokenizer.TT_EOF) {
            type = (int) mySt.nval;
            mySt.nextToken();
            // the first value stocked is the one describing the household type
            size = (int) mySt.nval;
            countPop = countPop + size;
            mySt.nextToken();
            myHouseH[countHousehold] = new int[size + 1];
            myHouseH[countHousehold][0] = type;
            for (int i = 1; i
                    <= size; i++) {
                myHouseH[countHousehold][i] = (int) mySt.nval;
                mySt.nextToken();
                setCountIndiv(getCountIndiv() + 1);
            }
            countHousehold++;
        }
        myFile.close();
        // trim the array according to the number of hh read from the file
        int[][] myHsh = new int[countHousehold][];
        for (int i = 0; i < countHousehold; i++) {
            myHsh[i] = new int[myHouseH[i].length];
            for (int j = 0; j < myHouseH[i].length; j++) {
                myHsh[i][j] = myHouseH[i][j];
            }
        }
        return myHsh;
    }
    /**
     * Read the file activity of individuals The format of the file is described in a doc file (instructions for the activity file.doc)
     */
    public String[][] readActivityFile(int popSize, int[][] hsh, String nameOfFile) {
        int i = 0, j = 0, size = 0, ind = 0, pos = 0;
        String[][] myActives = new String[popSize][7];
        int truc = 0;
        String temp = "0";
        try {
            int status = 0;
            FileReader myFile = new FileReader(nameOfFile);
            StreamTokenizer mySt = new StreamTokenizer(myFile);
            mySt.resetSyntax();    // Configure the tokenizer
            mySt.wordChars(33, 255);             // accept all characters
            mySt.whitespaceChars(0, ' '); // set white space as separator
            mySt.eolIsSignificant(false);  // ignore end of line character
            for (i = 0; i < myActives.length; i++) {
                Arrays.fill(myActives[i], "-1000");
            }
                mySt.nextToken();
            while (mySt.ttype != StreamTokenizer.TT_EOF) {
                for (i = 0; i < hsh.length; i++) {
                    size = hsh[i].length - 1;
                    pos = 0;
                    for (j = 0; j < size; j++) {
                        // Corresponds to one line of the activity file
                        truc = Integer.parseInt((mySt.sval));
                        myActives[ind][pos] = Integer.toString(truc); // status
                        status = truc;
                        mySt.nextToken();
                        pos++;
                        switch (status) { // the following depends on the status
                            // of the individual (stocked in myActives[ind][pos])
                            case 0: // student
                                // nothing at this time
                                break;
                            case 1: // active
                                if (mySt.ttype == StreamTokenizer.TT_NUMBER) {
                                    truc = (int) mySt.nval;
                                    myActives[ind][pos] = Integer.toString(truc); // municipality where it works
                                } else {
                                    myActives[ind][pos] = mySt.sval; // municipality where it works
                                }
                                if (myActives[ind][pos].length() == 4) {
                                    myActives[ind][pos] = temp.concat(myActives[ind][pos]);
                                }
                                mySt.nextToken();                             
                                pos++;                                
                                myActives[ind][pos] = mySt.sval; // activity sector
                                mySt.nextToken();
                                pos++;
                                myActives[ind][pos] = mySt.sval;// socioprofessionnal category
                                mySt.nextToken();                                                                           
                                break;
                            case 2: // unemployed
                                if (mySt.ttype == StreamTokenizer.TT_NUMBER) {
                                    truc = (int) mySt.nval;
                                    myActives[ind][pos] = Integer.toString(truc); // municipality where it works
                                } else {
                                    myActives[ind][pos] = mySt.sval; // municipality where it works
                                }
                                // Socio professional category (for France)
                                mySt.nextToken();                          
                                break;
                            case 3: // retired
                                 // nothing at this time
                                break;
                            case 4: // others
                                // nothing at this time
                                break;
                        }
                        pos = 0; 
                        ind++;
                    }
                }
                LOGGER.log(Level.FINER, "readActivityFile: ind {0} pos {1}popSize {2}", new Object[]{ind, pos, popSize});
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Problem while reading the activity file", e);
        }
        return myActives;
    }

    /**
     * Updates list of workers for each municipality. Must be used before using the Municipality.fire() function
     */
    public void updateMunicipalityWorkers() {
        HashMap<Municipality, List<Individual>> map = new HashMap<Municipality, List<Individual>>();        
        for (Municipality mun : getMyMunicipalities()) {            
            for (Household household : mun.getMyHouseholds()) {
                for (Individual individual : household) {
                    if (individual.getStatus() == Status.WORKER && individual.getCurrentActivity()!=null) {
                        Municipality jobLoc = individual.getCurrentJobLocation();
                        if (map.containsKey(jobLoc)){
                            map.get(jobLoc).add(individual);
                        }
                        else {
                            List<Individual> l = new ArrayList<Individual>();
                            l.add(individual);
                            map.put(jobLoc, l);
                        }                            
                    }
                }
            }
        }

        for (Map.Entry<Municipality, List<Individual>> m : map.entrySet()){
            List<Individual> workers = m.getValue();
            if (workers==null)
                workers = new ArrayList<Individual>();
            m.getKey().setWorkers(workers);
        }
    }

    @Observable(description = "Nb of SPC1/SOA1")
    public int getNbSPC1SoA1() {
        int size = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Status.WORKER
                                && ind.getProfession() == 0
                                && ind.getSectorOfActivity() == 0) {
                            size++;
                        }
                    }
                }
            }
        }
        return size;
    }

    /**
     * Generates a table with the count of households with 1, 2, 4, and 5+ 
     * persons grouped by the specified municipality sizes.
     * @param MUNICIPALITY_SIZES array containing the sizes of the municipalities
     * to use for grouping the household sizes
     * @return  The generated table with the household sizes
     */
    public SortedMap<Integer, Double[]> getRegionHouseholdSizesByMunicipalitySize(Integer[] sizes) {
        SortedMap<Integer, Double[]> map=new TreeMap<Integer, Double[]>();
        SortedMap<Integer, Integer> totals = new TreeMap<Integer, Integer> ();
        // add municipality sizes
        for (Integer i : sizes){
            // 1, 2, 3, 4, 5+ persons household, Avg Hh size
            map.put(i, new Double[]{0d,0d,0d,0d,0d});            
            totals.put(i, 0);            
        }
                
        
        for (Municipality mun : myMunicipalities){
            // select the appropiate municipality size
            Integer key = null;
            
            for (Map.Entry<Integer, Double[]> entry: map.entrySet()){
                if (mun.getPopulationSize()<entry.getKey()){
                    break;
                }
                key = entry.getKey();
            }                                
            // if the municipality population is not within the table values, skip it.
            if (key==null) {continue;}
            
            Double[] d = map.get(key);
        
            // increase the household size counters depending on the municipalityi households
            for (Household h: mun.getMyHouseholds()) {
                // ignore transit households and households without individuals
                if (h.getResidence().isTransit() || h.getSize()==0){continue;}

                // increase the total amount of residences in the corresponding row
                totals.put(key, totals.get(key)+1) ;
                // if household size within the household sizes array then
                // increase the corresponding bin
                if (h.getSize()<d.length){
                    d[h.getSize()-1]++;                    
                }
                else {
                    // if household size is bigger then count it in the last
                    // array bin.
                    d[d.length-1]++;
                }                                            
            }
           
        }
        // calculate percentages of each row in the table
        for (Map.Entry<Integer,Double[]> entry : map.entrySet()){
            Double[] d = entry.getValue();
            for (int i=0;i<d.length;i++){
                d[i]/= totals.get(entry.getKey());
            }
        }                
        return map;
    }
    /**
     * Calculates the commuting distance (in minutes) between two given
     * municipalities. The distance is obtained form the commuting distance
     * table.
     * @param m1 The Origin municipality 
     * @param m2 The destination municipality 
     * @return The commuting distance between the two input municipalities
     */
    public float getCommutingDistance(Municipality m1, Municipality m2){
        float distance =0;
        if (commutingDistances!=null){
            distance = commutingDistances.get(m1.getName()+m2.getName());
        }
        return distance;
    }
    /**
     * Initialze the commuting distance table reading from the distances input file
     */
    private void initCommutingDistances() throws BadDataException {
        String fileName=this.getParameters().getMunicipalitiesDistances();
        commutingDistances = new TreeMap<String, Float>();
         
         
            try{
            CSVReader distancesReader = CSV.getReader(getMyApplication().getFileFromRelativePath(fileName), null);
            String[] readLine = CSV.readLine(distancesReader);
            // ignore the first element containing the coumn/row header
            String[] munLabels =(String[])ArrayUtils.removeElement(readLine, readLine[0]);
            String[] line;
            // for each line of distances parse the distance form the source to the destination municipalities (columns)
            while ((line = CSV.readLine(distancesReader)) != null) {
                for (int i=1; i<line.length;i++){
                    commutingDistances.put(line[0]+munLabels[i-1], Float.parseFloat(line[i]));
                }
            }            
            } catch (Exception ex) {
            throw new BadDataException("error while loading municipalities distances", ex);
        }
    }
 
    /**
     * Table with dynamics defined by scenario to be used for the transition
     * from Inactive to inactive by an individual.
     */
    private SortedMap<String,Value<Boolean, Individual>> scenarioInactiveToInactiveDynamics;
       /**
     * Gets the dynamic function defined for the given municipality.
     * Returns @code(null) if there is no dynamic defined for the given municipality
     * @param mun The municipality for which the dynamic will be returned
     */
    public Value<Boolean, Individual> getInactiveInactiveScenarioDynamic(Municipality mun){        
        if (scenarioInactiveToInactiveDynamics.isEmpty() ||  !scenarioInactiveToInactiveDynamics.containsKey(mun.toString())){
            return null;    
        }
        return scenarioInactiveToInactiveDynamics.get(mun.toString());                
        
    }
    
    /**
     * Table with dynamics defined by scenario to be used for the choice of
     * sector of activity
     */
    private SortedMap<String,Value<Integer, Individual>> scenarioSectorChoiceDynamics;
   
    /**
     * Gets the dynamic function defined for the given municipality.
     * Returns @code(null) if there is no dynamic defined for the given municipality
     * @param mun The municipality for which the dynamic will be returned
     */
    public Value<Integer, Individual> getSectorChoiceScenarioDynamic(Municipality mun){
        if (scenarioSectorChoiceDynamics==null || scenarioSectorChoiceDynamics.isEmpty() || !scenarioSectorChoiceDynamics.containsKey(mun.toString())){
            return null;
        }
        return scenarioSectorChoiceDynamics.get(mun.toString());
    }
    
    /**
     * Adds a Inactive to Inactive transition dynamic for a defined municipality.
     * @param mun The municipality for which the dynamic will be used.
     * @param dyn The dynamic to use for that municipality
     */
    public void addInactiveInactiveScenarioDynamic(Municipality mun, Value<Boolean, Individual> dyn){
        scenarioInactiveToInactiveDynamics.put(mun.toString(), dyn);
    }
    
    
    /**
     * Adds a Inactive to Inactive transition dynamic for a defined municipality.
     * @param mun The municipality for which the dynamic will be used.
     * @param dyn The dynamic to use for that municipality
     */
    public void addSectorChoiceScenarioDynamic(Municipality mun, Value<Integer, Individual> dyn){
        scenarioSectorChoiceDynamics.put(mun.toString(), dyn);
    }
    
    
    
    /**
     * Clears the table of Inactive to Inactive scenario  dynamics
     */
    public void initInactiveInactiveScenarioDynamic(){
        if (scenarioInactiveToInactiveDynamics==null){
            scenarioInactiveToInactiveDynamics= new TreeMap<String, Value<Boolean, Individual>>();
        }
        scenarioInactiveToInactiveDynamics.clear();
    }
    
    public void initSectorChoiceDynamic(){
        if (scenarioSectorChoiceDynamics==null){
            scenarioSectorChoiceDynamics= new TreeMap<String, Value<Integer, Individual>>();
        }
        scenarioSectorChoiceDynamics.clear();
    }
    
}
