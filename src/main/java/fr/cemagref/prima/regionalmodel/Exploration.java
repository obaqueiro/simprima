/*
 *  Copyright (C) 2011 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import com.thoughtworks.xstream.XStream;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.observation.observers.CSVObserver;
import fr.cemagref.prima.regionalmodel.parameters.Parameters;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.openide.util.Exceptions;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Exploration {

    /**
     * Represent a list of possible values for a parameter
     */
    protected static class ParameterValues {

        private String name;
        private Object[] values;
        private transient int index = 0;
        protected ParameterValues() {
        }

        public ParameterValues(final String name, final Object[] values) {
            this.name = name;
            this.values = values.clone();
        }

        public void incIndex() {
            index++;
        }

        public int getIndex() {
            return index;
        }

        public void resetIndex() {
            index = 0;
        }

        public List<ParameterValue> getNextValue() {
            return Arrays.asList(new ParameterValue(name, values[index]));
        }

        public int size() {
            return values.length;
        }
    }

    protected static class ComposedParametersValues extends ParameterValues {

        final private List<ParameterValues> parametersValues;

        public ComposedParametersValues(final List<ParameterValues> parametersValues) throws BadDataException {
            super();
            this.parametersValues = parametersValues;
            int size = -1;
            for (ParameterValues parameterValues : parametersValues) {
                if (size != -1 && size != parameterValues.size()) {
                    throw new BadDataException("The parameters doesn't have the same amount of values!");
                }
                size = parameterValues.size();
            }
        }

        @Override
        public List<ParameterValue> getNextValue() {
            final List<ParameterValue> result = new ArrayList<ParameterValue>(parametersValues.size());
            for (ParameterValues parameterValues : parametersValues) {
                result.addAll(parameterValues.getNextValue());
            }
            return result;
        }

        @Override
        public void incIndex() {
            for (ParameterValues parameterValues : parametersValues) {
                parameterValues.incIndex();
            }
        }

        @Override
        public int getIndex() {
            return parametersValues.get(0).getIndex();
        }

        @Override
        public int size() {
            return parametersValues.get(0).size();
        }

        @Override
        public void resetIndex() {
            for (ParameterValues parameterValues : parametersValues) {
                parameterValues.resetIndex();
            }

        }
    }

    /**
     * Represent the value to use for a simulation for a parameter
     */
    public static class ParameterValue {

        final private String name;
        final private Object value;

        public ParameterValue(final String name, final Object value) {
            this.name = name;
            this.value = value;
        }

        private ParameterValue(final ParameterValues pv) {
            this.name = pv.name;
            this.value = pv.values[pv.index];
        }

        @Override
        public String toString() {
            return name + ":" + value.toString();
        }
    }
    private transient String inputFile;
    private int nbReplicates;
    private String baseInputFile;
    private Boolean copyInputDir;
    private String baseOutputDir;
    private List<ParameterValues> parametersValues;
    private transient List<List<ParameterValue>> parametersValueBySimulation;
    private List<String> regionObservables;
    private transient int currentSimulation, totalSimulations;

    public Exploration(final int nbReplicates, final String baseInputFile, final String baseOutputDir) {
        this.nbReplicates = nbReplicates;
        this.baseInputFile = baseInputFile;
        this.baseOutputDir = baseOutputDir;
        this.totalSimulations = nbReplicates;
        
    }

    public Exploration(final int nbReplicates, final String baseInputFile, final String baseOutputDir, final Boolean copyInputDir) {
        this(nbReplicates, baseInputFile, baseOutputDir);
        this.copyInputDir = copyInputDir;
    }

    public static List<List<ParameterValue>> completePlan(final List<ParameterValues> parametersValues) {
        final List<List<ParameterValue>> parametersValueBySimulation = new ArrayList<List<ParameterValue>>();
        boolean end = false;
        while (!end) {
            final List<ParameterValue> simParameters = new ArrayList<ParameterValue>();
            for (ParameterValues pv : parametersValues) {
                simParameters.addAll(pv.getNextValue());
            }
            parametersValueBySimulation.add(simParameters);
            // prepare the next value
            for (ParameterValues pv : parametersValues) {
                pv.incIndex();
                if (pv.getIndex() == pv.size()) {
                    if (pv == parametersValues.get(parametersValues.size() - 1)) {
                        end = true;
                        break;
                    }
                    pv.resetIndex();
                } else {
                    break;
                }
            }
        }
        
        return parametersValueBySimulation;
    }

    public void run() throws IOException, BadDataException, ProcessingException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        final File outputDir = createOutputDir(baseOutputDir);
        final long begin = System.currentTimeMillis();
        FileUtils.copyFileToDirectory(new File(inputFile), outputDir);
        if (this.copyInputDir == null || this.copyInputDir) {
           new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        FileUtils.copyDirectory(new File(baseInputFile).getParentFile(), new File(outputDir, "input"), new NotFileFilter(new WildcardFileFilter(".svn**")));
                    } catch (IOException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }).start();
           
        }
        if (parametersValues == null || parametersValues.isEmpty()) {
            // no factors given, we'll run only replication of default value
            runReplicates(null, 0);
        } else {
            // complete plan
            parametersValueBySimulation = completePlan(parametersValues);
            // ready for running simulations
            final PrintStream valuesIndexStream = new PrintStream(baseOutputDir + "parameters_values.csv");
            final String separator = ";";
            // print simulation parameters headers
            StringBuilder buff = new StringBuilder();
            buff.append("ID");
            for (ParameterValues parameterValues : parametersValues) {
                buff.append(separator).append(parameterValues.name);
            }
            valuesIndexStream.println(buff);
            // run simulations
            int index = 0;
            totalSimulations = parametersValueBySimulation.size() * nbReplicates;
            currentSimulation = 0;
            buff = new StringBuilder();
            for (List<ParameterValue> runParameters : parametersValueBySimulation) {
                buff.setLength(0); // reset StringBuilder               
                index++;
                // print headers                
                buff.append(index);
                for (ParameterValue pv : runParameters) {
                    buff.append(separator).append(pv.value);
                }
                valuesIndexStream.println(buff);
                runReplicates(runParameters, index);
            }
            
            valuesIndexStream.close();
        }
        final long elapsed = System.currentTimeMillis() - begin;
        System.out.println("Time elapsed: " + String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(elapsed),
                TimeUnit.MILLISECONDS.toSeconds(elapsed)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(elapsed))));

    }

    public static File createOutputDir(final String dirname) throws IOException, HeadlessException {
        final File outputDir = new File(dirname);        
        if (outputDir.exists()) {            
            FileUtils.deleteDirectory(outputDir);
        }
        if (!outputDir.mkdirs()){
            throw new IOException("Could not create directory");
        }
        return outputDir;
    }

    public void runReplicates(final List<ParameterValue> runParameters, final int index)  {
        int maxThreads=5;
        // run replicates        
        Thread[] threads = new Thread[nbReplicates]; // Limit the number of threads!
        for (int i = 0; i < nbReplicates; i++) {         
            currentSimulation++;
            System.out.println("Simulation " + currentSimulation + " / " + totalSimulations);
            final String runOutputDir = baseOutputDir ;// + "_r_" + i;

            LogManager.getLogManager().reset();
        
            final List<Logger> loggers = new ArrayList<Logger>();
            for (String loggerName : new String[]{"fr.cemagref.prima", "MunicipalitySet", "Municipality", "Scenario", "Parameters"}) {
                final Logger logger = Logger.getLogger(loggerName);
                loggers.add(logger);
                logger.setLevel(Level.OFF);
                for (Handler handler : logger.getHandlers()) {
                    logger.removeHandler(handler);
                }
                //logger.addHandler(fileHandler);
            }
            
            final ExplorationThread myThread = new ExplorationThread(runParameters, runOutputDir, baseInputFile,i, index);
            threads[i]=new Thread(myThread);
            threads[i].setPriority(Thread.MIN_PRIORITY);            
            threads[i].start();

            if ((i + 1) % maxThreads == 0) {
                try {
                    threads[i].join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Thread was Interrupted");
                }
            }
        }
        
      for (Thread t : threads){
            if (t.isAlive()) {
                try {
                    t.join();
                }
                catch (InterruptedException ex){
                    Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Thread was Interrupted");
                }

            }
        }            
    }

    class ExplorationThread implements Runnable {

        List<ParameterValue> runParameters;
        int threadIndex;
        int expIndex;
        String runOutputDir;
        String baseInputFile;
        

        /**
         * Creates a thread to run one exploration
         * @param runPars   Parameters used for this exploration
         * @param outputDir Name of folder where exploration output data will be saved
         * @param baseInput Base input folder for the data
         * @param index     Simulation run index
         * @param expIndex  Exploration index (differentiating the parameters)
         */
        public ExplorationThread(final List<ParameterValue> runPars, final  String outputDir, final  String baseInput, final  int index, int expIndex) {
            super();
            this.runParameters = runPars;
            threadIndex = index;
            this.expIndex = expIndex;
            runOutputDir = outputDir;
            this.baseInputFile = baseInput;



        }

        @Override
        public void run() {
            try {
                // load default parameters
                final String[] params = new String[]{"-i", baseInputFile};

                final Application application = new Application(params);
                // set the seed and other exploration parameters
                setParam(application, "seedIndex", threadIndex);
                setParam(application, "expIndex", expIndex);
                if (runParameters != null) {
                    for (Exploration.ParameterValue pv : runParameters) {
                        setParam(application, pv.name, pv.value);
                    }
                }
                // init outputs
                setParam(application, "outputDir", runOutputDir);
                final List<ObserverListener> regionObservers = new ArrayList<ObserverListener>();
                if (regionObservables != null) {
                    regionObservers.add(new CSVObserver(false, runOutputDir + "-" + threadIndex + "_region.csv", regionObservables.toArray(new String[]{})));
                    setParam(application, "regionObservers", regionObservers);
                }
                setParam(application, "runPrefix", threadIndex);
                // achieve init and run
                application.getParameters().readResolve();                               
                application.loadFiles();
                /*if (i == 0) {
                Application.toXML(application.getParameters(), new FileWriter(runOutputDir + "_ficEnt.xml"));
                }*/
                application.run(threadIndex);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getSimpleName()).log(Level.SEVERE, ex.getLocalizedMessage());      
            }

        }
    }


    public void setParam(final Application application, final String name, final Object value) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        final Field field = Parameters.class.getDeclaredField(name);
        final boolean access = field.isAccessible();
        field.setAccessible(true);
        field.set(application.getParameters(), value);
        field.setAccessible(access);
        }

    public static void main(final String[] args) 
            throws FileNotFoundException, IOException, BadDataException, ProcessingException, 
            NoSuchFieldException, IllegalAccessException {
        if (args.length != 1) {
            System.err.println("This program have to receive the exploration input file as unique argument.");
            System.exit(1);
        }
        final XStream xstream = new XStream();
        for (Class type : new Class[]{Exploration.class, ParameterValues.class, ComposedParametersValues.class}) {
            xstream.alias(type.getSimpleName(), type);
        }
        xstream.addImplicitCollection(Exploration.class, "parametersValues", "factors", ParameterValues.class);
        xstream.addImplicitCollection(ComposedParametersValues.class, "parametersValues", "factor", ParameterValues.class);
        final FileReader exploFileReader = new FileReader(args[0]);
        final Exploration exploration = (Exploration) xstream.fromXML(exploFileReader);
        exploFileReader.close();
        exploration.inputFile = args[0];
        exploration.run();
    }
}
