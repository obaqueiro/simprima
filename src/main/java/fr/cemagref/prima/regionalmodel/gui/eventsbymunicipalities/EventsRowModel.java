/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.gui.eventsbymunicipalities;

import fr.cemagref.prima.regionalmodel.scenarios.TableUpdaterEvent;
import org.netbeans.swing.outline.RowModel;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class EventsRowModel implements RowModel {

    private int maxCategoryNumber;

    public EventsRowModel(int maxCategoryNumber) {
        this.maxCategoryNumber = maxCategoryNumber;
    }

    @Override
    public int getColumnCount() {
        return maxCategoryNumber;
    }

    @Override
    public Object getValueFor(Object node, int column) {
        if (node instanceof EventTreeNode) {
            TableUpdaterEvent event = ((EventTreeNode) node).getEvent();
            if (column >= event.getDeltasSize()) {
                return null;
            } else {
                return event.getDelta(column);
            }
        } else {
            return null;
        }
    }

    @Override
    public Class getColumnClass(int column) {
        return Integer.class;
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        return false;
    }

    @Override
    public void setValueFor(Object node, int column, Object value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getColumnName(int column) {
        return String.valueOf(column);
    }
}
