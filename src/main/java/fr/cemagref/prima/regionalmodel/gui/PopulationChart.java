package fr.cemagref.prima.regionalmodel.gui;

import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.observers.jfreechart.TemporalSerieChart;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.axis.NumberAxis;

/**
 * Specialize the TemporalSerieChart plotter to disable the label that displays
 *  the useless hashcode of the object.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class PopulationChart extends TemporalSerieChart {

    public PopulationChart() {
        super();
    }

    public PopulationChart(ObservablesHandler.ObservableFetcher variable) {
        super(variable);
    }

    @Override
    protected void graphTypeUpdated() {
        super.graphTypeUpdated();
        setTitlePrefix("");
        // disable the label that displays the hashcode of the object
        getJfchart().removeLegend();
        // reduce the minimum display size
        ((ChartPanel) getDisplay()).setMinimumDrawHeight(50);
        ((ChartPanel) getDisplay()).setMinimumDrawWidth(100);
        // set the title fond
        getJfchart().getTitle().setFont(new java.awt.Font("SansSerif", java.awt.Font.BOLD, 12));
        // display axis tick as integer and don't display title
        NumberAxis rangeAxis = (NumberAxis) getJfchart().getXYPlot().getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        rangeAxis.setLabel("");
        NumberAxis domainAxis = (NumberAxis) getJfchart().getXYPlot().getDomainAxis();
        domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        domainAxis.setLabel("");
        // convert tick to year
        domainAxis.setNumberFormatOverride(new DecimalFormat() {

            @Override
            public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
                return toAppendTo.append((int)(number));
            }
        });
    }

}
