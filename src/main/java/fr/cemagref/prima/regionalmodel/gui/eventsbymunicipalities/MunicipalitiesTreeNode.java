/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.gui.eventsbymunicipalities;

import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.scenarios.Event;
import fr.cemagref.prima.regionalmodel.scenarios.EventsWrapper;
import fr.cemagref.prima.regionalmodel.scenarios.TableUpdaterEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
class MunicipalitiesTreeNode implements TreeNode, TreeNodeWithChildren<MunicipalityTreeNode> {

    private List<MunicipalityTreeNode> children;
    private Map<Municipality, MunicipalityTreeNode> map;
    private String tableName;

    MunicipalitiesTreeNode(List<EventsWrapper> eventsWrappers, String tableName) {
        this.tableName = tableName;
        map = new HashMap<Municipality, MunicipalityTreeNode>();
        // extract event and put in a structure date/municipality/event
        MunicipalityTreeNode currentChild;
        int date = 0;
        for (EventsWrapper eventsWrapper : eventsWrappers) {
            for (Entry<Municipality, List<Event<Municipality>>> events : eventsWrapper.eventsIterator()) {
                currentChild = map.get(events.getKey());
                if (currentChild == null) {
                    currentChild = new MunicipalityTreeNode(events.getKey());
                    map.put(events.getKey(), currentChild);
                }
                for (Event<Municipality> event : events.getValue()) {
                    currentChild.add(new EventTreeNode(date, (TableUpdaterEvent) event));
                }
            }
            date++;
        }
        children = new ArrayList<MunicipalityTreeNode>(map.values());
    }

    @Override
    public String getDisplayName() {
        return "Events for " + tableName;
    }

    @Override
    public List<MunicipalityTreeNode> getChildren() {
        return children;
    }
}
