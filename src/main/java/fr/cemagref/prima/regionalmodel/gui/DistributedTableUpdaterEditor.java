/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.prima.regionalmodel.gui;

import fr.cemagref.prima.regionalmodel.scenarios.DistributedTableUpdater;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class DistributedTableUpdaterEditor extends javax.swing.JPanel {

    /** Creates new form DistributedTableUpdaterEditor */
    public DistributedTableUpdaterEditor(DistributedTableUpdater tableUpdater) {
        super();
        fileTextField.setText(tableUpdater.getInputFilename());
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        eventsPanel = new javax.swing.JPanel();
        plotPanel = new javax.swing.JPanel();
        editionToggleButton = new javax.swing.JToggleButton();
        fileTextField = new javax.swing.JTextField();
        loadButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(1.0);
        jSplitPane1.setOneTouchExpandable(true);

        eventsPanel.setLayout(new java.awt.BorderLayout());
        jSplitPane1.setTopComponent(eventsPanel);

        plotPanel.setLayout(new java.awt.BorderLayout());
        jSplitPane1.setRightComponent(plotPanel);

        editionToggleButton.setText(org.openide.util.NbBundle.getMessage(DistributedTableUpdaterEditor.class, "DistributedTableUpdaterEditor.editionToggleButton.text")); // NOI18N
        editionToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editionToggleButtonActionPerformed(evt);
            }
        });

        fileTextField.setText(org.openide.util.NbBundle.getMessage(DistributedTableUpdaterEditor.class, "DistributedTableUpdaterEditor.fileTextField.text")); // NOI18N
        fileTextField.setEnabled(false);

        loadButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Open24.gif"))); // NOI18N
        loadButton.setText(org.openide.util.NbBundle.getMessage(DistributedTableUpdaterEditor.class, "DistributedTableUpdaterEditor.loadButton.text")); // NOI18N
        loadButton.setEnabled(false);
        loadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadButtonActionPerformed(evt);
            }
        });

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/toolbarButtonGraphics/general/Save24.gif"))); // NOI18N
        saveButton.setText(org.openide.util.NbBundle.getMessage(DistributedTableUpdaterEditor.class, "DistributedTableUpdaterEditor.saveButton.text")); // NOI18N
        saveButton.setEnabled(false);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 677, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editionToggleButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 158, Short.MAX_VALUE)
                .addComponent(fileTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(loadButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saveButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editionToggleButton)
                    .addComponent(saveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(loadButton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fileTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void loadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_loadButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_saveButtonActionPerformed

    private void editionToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editionToggleButtonActionPerformed
        // TODO add your handling code here:
        if (editionToggleButton.isSelected()) {
            fileTextField.setEnabled(true);
            loadButton.setEnabled(true);
        } else {
            // TODO ask for saving if modified
            fileTextField.setEnabled(false);
            loadButton.setEnabled(false);
            saveButton.setEnabled(false);
        }
    }//GEN-LAST:event_editionToggleButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton editionToggleButton;
    private javax.swing.JPanel eventsPanel;
    private javax.swing.JTextField fileTextField;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JButton loadButton;
    private javax.swing.JPanel plotPanel;
    private javax.swing.JButton saveButton;
    // End of variables declaration//GEN-END:variables

}
