/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.gui.eventsbymunicipalities;

import fr.cemagref.prima.regionalmodel.scenarios.TableUpdaterEvent;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class EventTreeNode implements TreeNode {

    private int date;
    private TableUpdaterEvent event;

    public EventTreeNode(int date, TableUpdaterEvent event) {
        this.date = date;
        this.event = event;
    }

    @Override
    public String getDisplayName() {
        return  String.valueOf(date);
    }

    public TableUpdaterEvent getEvent() {
        return event;
    }

    public int getDate() {
        return date;
    }
}
