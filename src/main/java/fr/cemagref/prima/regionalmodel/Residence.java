/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author dumoulin
 */
public class Residence implements Iterable<Household> {

    private int type;
    protected List<Household> inhabitants;

    public Residence(int type) {
        this.type = type;
        inhabitants = new ArrayList<Household>();
    }

    /**
     * Current status of the residence where the household is living. For example,
     *  let's consider that were defined three diferent status of residences:
     * <ul><li>-1 means the household is in transit (for immigrants)
     * <li>type 1: residences with 1 or 2 bedrooms
     * <li>type 2: residences with 3 or 4 bedrooms
     * <li>type 3: residences with more than 4 bedrooms.
     * </ul>So, the status is related to the size of the residence.
     * S. Huet - 16.04.09: more generally, depending on the number of class retained,
     * the method appropriatedSizeOfRes of the class Municipality give the good
     * size of residence for a given size of family
     */
    public int getType() {
        return type;
    }

    public boolean add(Household e) {
        return inhabitants.add(e);
    }

    public int getNbHhResidents() {
        return inhabitants.size();
    }

    public boolean remove(Household o) {
        return inhabitants.remove(o);
    }

    /**
     * Create a particular residence with a type to -1 than can contain only one
     * household (immutable).
     * @param household
     * @return
     */
    public static Residence createTransitResidence(Household household) {
        return new TransitResidence(household);
    }

    public boolean isTransit() {
        return this instanceof TransitResidence;
    }

    @Override
    public Iterator<Household> iterator() {
        return inhabitants.iterator();
    }

    private static class TransitResidence extends Residence {

        public TransitResidence(Household resident) {
            super(-1);
            this.inhabitants.add(resident);
        }

        @Override
        public boolean add(Household e) {
            throw new UnsupportedOperationException("This object is immutable and cannot be modified!");
        }
    }
    
        public int getNbIndividualResidents(){
        int i=0;
        for (Household h: inhabitants){
            i+=h.getSize();
        }
        
        return i;
    }
}
