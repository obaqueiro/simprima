/*
 *  Copyright (C) 2010 sylvie.huet
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author sylvie.huet
 */
@ServiceProvider(service = Scenario.class)
public class ImmigrationUpdater extends Scenario {

    private final static Logger LOGGER = Logger.getLogger(ImmigrationEvent.class.getSimpleName());
    private transient List<EventsWrapper> eventsWrappers;
    private String globalInputFilename;
    private Integer firstEmigrantsAmount;

    @Override
    public final void init(final MunicipalitySet region) throws BadDataException {
        super.init(region);
        final int beginYear = region.getParameters().getStartStep();
        final int endYear = region.getParameters().getStartStep() + region.getParameters().getNbStep();
        eventsWrappers = new ArrayList<EventsWrapper>(endYear - beginYear + 1);
        for (int i = 0; i < endYear - beginYear + 1; i++) {
            eventsWrappers.add(new EventsWrapper());
        }
        if (inputFilename != null) {
            // read immmigrants file for each municipalities
            for (final Municipality municipality : municipalities) {
                try {
                    List<Event<Municipality>>[] eventsDistributed = new List[endYear - beginYear + 1];
                    for (int i = 0; i < eventsDistributed.length; i++) {
                        eventsDistributed[i] = new ArrayList<Event<Municipality>>();
                    }
                    // open file
                    CSVReader reader = CSV.getReader(true, municipality, inputFilename, separator);
                    CSV.readYearlyData(reader, new CSV.ArrayParser() {

                        @Override
                        public void parse(int date, String[] copyOfRange) throws BadDataException {
                            // check date
                            if (date > endYear) {
                                LOGGER.log(Level.WARNING, "This scenario input file gives event"
                                        + " that should occurs after the time period defined by"
                                        + " the scenario\n  input file:{0}  municipality concerned:{1}",
                                        new Object[]{inputFilename, municipality.getName()});
                            } else {
                                // add immigration event
                                eventsWrappers.get(date - beginYear).put(municipality, buildEvent(copyOfRange, false));
                            }
                        }
                    });
                    reader.close();
                } catch (Exception ex) {
                    throw new BadDataException("Error while reading input file:" + inputFilename
                            + "\n  municipality concerned:" + municipality.getName(), ex);
                }
            }
        }
        // read global immigrants file
        if (globalInputFilename != null) {
            try {
                CSVReader reader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(globalInputFilename), separator);
                CSV.readYearlyData(reader, new CSV.ArrayParser() {

                    @Override
                    public void parse(int date, String[] copyOfRange) throws BadDataException {
                        // check date
                        if (date > endYear) {
                            LOGGER.log(Level.WARNING, "This scenario input file gives event"
                                    + " that should occurs after the time period defined by"
                                    + " the scenario\n  input file:{0}",
                                    new Object[]{globalInputFilename});
                        } else {
                            // add immigration events
                            for (Municipality mun : municipalities) {
                                for (int i = date; i <= region.getStartStep() + region.getParameters().getNbStep(); i++) {
                                    // if we are in the first step, we compute nbHH with firstEmigrantsAmount and set useBalance to false
                                    Event<Municipality> events = null;
                                    if (i == region.getStartStep()) {
                                        if (firstEmigrantsAmount == null) {
                                            throw new BadDataException("You have given a migratory balance for the time step, "
                                                    + "so you need to set the parameter firstEmigrantsAmount in your "
                                                    + this.getClass().getSimpleName() + " scenario");
                                        }
                                        copyOfRange[0] = String.valueOf((Integer.parseInt(copyOfRange[0]) + firstEmigrantsAmount));
                                    }
                                    events = buildEvent(copyOfRange, true);
                                    eventsWrappers.get(i - beginYear).put(mun, events);
                                }
                            }
                        }
                    }
                });
                reader.close();
            } catch (Exception ex) {
                throw new BadDataException("Error while reading input file:" + globalInputFilename, ex);
            }
        }
    }

    /**
     * build an immigration event for a line in input
     * @param line array of input data from file
     * @param useMigratoryBalance if the input define a migratory balance (elsewise, it is an amount of HH)
     * @return
     */
    protected Event<Municipality> buildEvent(String[] line, boolean useMigratoryBalance) {
        return new ImmigrationEvent(line[0], useMigratoryBalance);
    }

    @Override
    public final List<? extends Event<MunicipalitySet>> getEvents() {
        return eventsWrappers;
    }
}
