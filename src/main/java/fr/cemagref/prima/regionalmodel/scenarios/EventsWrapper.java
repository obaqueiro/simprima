/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This class aims to wrap several lists of events for a set of municipalities.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class EventsWrapper implements Event<MunicipalitySet> {

    private Map<Municipality, List<Event<Municipality>>> events;

    public EventsWrapper() {
        events = new LinkedHashMap<Municipality, List<Event<Municipality>>>();
    }

    public List<Event<Municipality>> put(Municipality key, List<Event<Municipality>> value) {
        return events.put(key, value);
    }

    public void put(Municipality key, Event<Municipality> value) {
        List<Event<Municipality>> list = events.get(key);
        if (list == null) {
            list = new ArrayList<Event<Municipality>>();
            events.put(key, list);
        }
        list.add(value);
    }

    public Iterable<Entry<Municipality, List<Event<Municipality>>>> eventsIterator() {
        return events.entrySet();
    }

    @Override
    public void process(final MunicipalitySet region) throws ProcessingException {
        for (Map.Entry<Municipality, List<Event<Municipality>>> item : events.entrySet()) {
            for (Event<Municipality> event : item.getValue()) {
                try {
                    event.process(item.getKey());              
                } catch (Exception ex) {
                    throw new ProcessingException("Error while processing this event in municipality " + item.getKey().getName() + ": " + event, ex);
                }
            }
        }
    }
}
