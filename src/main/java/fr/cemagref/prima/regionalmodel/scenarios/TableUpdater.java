/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * This object is designed to alter an array of municipality objects. It will
 * apply a delta on each value of an array from:<ul>
 * <li>the name of the method to use for updating the array.
 * <li>a value that will be determined by class derivation
 * <li>the mode to use for applying the modification, see {@link UpdateMode}.
 * </ul>
 * <p>The updating method is supposed to invoke the method {@link UpdateMode#computeDelta(int, int,UpdateMode)}
 * and should have such a signature :
 * <code>(Integer category, Integer value, UpdateMode mode)</code>.
 * <p>You can also provide a method with the same name but without arguments that
 * will be called after having updated the entire table.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public abstract class TableUpdater extends Scenario {

    /**
     * Represents the mode to use for updating an array value.
     * Default mode is Delta.
     */
    public static enum UpdateMode {

        ABSOLUTE, DELTA, PERCENTAGE;

        /**
         * Computes the delta from the current value, the given value and the update mode.
         * @param base current value in the model
         * @param value new value given by the scenario
         * @param mode updating mode to use
         * @return returns the delta that should be applied on the current value
         */
        public static int computeDelta(int base, int value, UpdateMode mode) {
            switch (mode == null ? UpdateMode.DELTA : mode) {
                case ABSOLUTE:
                    return value - base;
                case DELTA:
                    return value;
                case PERCENTAGE:
                    return (int) (base * ((double) value / 100));
            }
            throw new java.lang.IllegalArgumentException("Update mode " + mode + " is not supported.");
        }
    }
    /**
     * Gives the name of the attribute in Municipality class that handles the
     * table to update.
     */
    protected String updaterMethodName;
    protected UpdateMode mode;
    protected transient List<EventsWrapper> eventsWrappers;
    protected transient int tableSize = -1;
    protected transient Method updaterMethod;
    protected transient Method finalUpdaterMethod;

    public TableUpdater() {
        super();
    }

    public TableUpdater(String inputFilename, String tableName) {
        super();
        this.inputFilename = inputFilename;
        this.updaterMethodName = tableName;
    }

    public String getUpdaterMethodName() {
        return updaterMethodName;
    }

    public int getTableSize() {
        return tableSize;
    }

    @Override
    public List<? extends Event<MunicipalitySet>> getEvents() {
        return eventsWrappers;
    }

    public final void initEventsDistributions(int beginYear, int endYear) throws BadDataException {
        eventsWrappers = new ArrayList<EventsWrapper>(endYear - beginYear + 1);
        for (int i = 0; i < endYear - beginYear + 1; i++) {
            eventsWrappers.add(new EventsWrapper());
        }
        try {
            updaterMethod = Municipality.class.getDeclaredMethod(updaterMethodName, Integer.class, Integer.class, UpdateMode.class);
        } catch (Exception ex) {
            throw new BadDataException("Error while trying to acces to method \"" + updaterMethodName
                    + "\n in class Municipality", ex);
        }
        try {
            finalUpdaterMethod = Municipality.class.getDeclaredMethod(updaterMethodName);
        } catch (NoSuchMethodException ex) {
            finalUpdaterMethod = null;
        } catch (Exception ex) {
            throw new BadDataException("Error while trying to acces to method \"" + updaterMethodName
                    + "\n in class Municipality", ex);
        }
        // each municipality will have its events
        for (Municipality municipality : municipalities) {
            try {
                List<Event<Municipality>>[] eventsDistributed = new List[endYear - beginYear + 1];
                for (int i = 0; i < eventsDistributed.length; i++) {
                    eventsDistributed[i] = new ArrayList<Event<Municipality>>();
                }
                initEventsDistributionsForMunicipality(beginYear, endYear, municipality, eventsDistributed);
            } catch (Exception ex) {
                throw new BadDataException("Error while initializing scenario for method " + updaterMethodName + " with input file " + inputFilename
                        + " in municipality " + municipality.getName(), ex);
            }
        }
    }

    protected abstract void initEventsDistributionsForMunicipality(int beginYear, int endYear, Municipality municipality, List<Event<Municipality>>[] eventsDistributed) throws BadDataException;
}
