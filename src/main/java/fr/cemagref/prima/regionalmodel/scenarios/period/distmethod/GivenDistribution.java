/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios.period.distmethod;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.scenarios.Event;
import fr.cemagref.prima.regionalmodel.scenarios.period.PeriodDistributionMethod;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import java.io.IOException;
import java.util.List;
import org.openide.util.lookup.ServiceProvider;

/**
 * Distribute the events according to a normalized cumulative distribution.
 * The distribution gives for each year the cumulative probability that an event
 * occurs. The probability for the last year is equals to 1.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = PeriodDistributionMethod.class)
public class GivenDistribution implements PeriodDistributionMethod {

    private String filename;
    private Character delimiter;
    private transient Double[] distributions;

    public GivenDistribution() {
    }

    public GivenDistribution(String filename) {
        this.filename = filename;
    }

    public void loadDataForMunicipality(Municipality municipality) throws BadDataException {
        CSVReader reader = CSV.getReader(true, municipality, filename, delimiter);
        distributions = CSV.nextDoublesLine(reader);
        try {
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("", ex);
        }
    }

    @Override
    public <T> void fillEventsDistribution(Municipality municipality, List<Event<T>> events, int periodLength, List<Event<T>>[] eventsDistributed) {
        if (distributions.length != periodLength) {
            throw new RuntimeException("Given distribution should contain as many"
                    + " data than the number of years in the time period (currently "
                    + periodLength + ")");
        }
        for (Event event : events) {
            double d = municipality.getMyRegion().getRandom().nextDouble();
            int idx = 0;
            double lowBound = 0;
            while (d > lowBound) {
                lowBound = distributions[idx];
                idx++;
            }
            eventsDistributed[idx-1].add(event);
        }
    }
}
