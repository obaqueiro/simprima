/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios.period.distmethod;

import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.scenarios.Event;
import fr.cemagref.prima.regionalmodel.scenarios.period.PeriodDistributionMethod;
import java.util.List;
import org.openide.util.lookup.ServiceProvider;

/**
 * Distribute each events randomly in the period (uniform distribution).
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = PeriodDistributionMethod.class)
public class UniformDistributed implements PeriodDistributionMethod {

    @Override
    public <T> void fillEventsDistribution(Municipality municipality, List<Event<T>> events, int periodLength, List<Event<T>>[] eventsDistributed) {
        for (Event event : events) {
            eventsDistributed[municipality.getMyRegion().getRandom().nextInt(0, periodLength-1)].add(event);
        }
    }
}
