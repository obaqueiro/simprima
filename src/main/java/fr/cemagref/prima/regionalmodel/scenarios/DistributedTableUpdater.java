/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

/**
 * TableUpdater built from an already distributed set of events loaded from files.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Scenario.class)
public class DistributedTableUpdater extends TableUpdater {

    private static final Event.DummyEvent<Municipality> dummyEvent = new Event.DummyEvent<Municipality>();

    public DistributedTableUpdater(String inputFilename, String tableName) {
        super(inputFilename, tableName);
    }

    public DistributedTableUpdater() {
        super();
    }

    @Override
    public void init(MunicipalitySet region) throws BadDataException {
        super.init(region);
        super.initEventsDistributions(region.getParameters().getStartStep(), region.getParameters().getStartStep() + region.getParameters().getNbStep());
    }

    // TODO add saveToCSV method
    @Override
    protected void initEventsDistributionsForMunicipality(int beginYear, int endYear, Municipality municipality, List<Event<Municipality>>[] eventsDistributed) throws BadDataException {
        List<Event<Municipality>> events = getEvents(beginYear, endYear, municipality);
        List<Event<Municipality>> eventsForATimestep;
        for (int i = 0; i < Math.min(eventsDistributed.length, events.size()); i++) {
            if (events.get(i) != dummyEvent) {
                eventsForATimestep = new ArrayList<Event<Municipality>>();
                eventsForATimestep.add(events.get(i));
                eventsWrappers.get(i).put(municipality, eventsForATimestep);
            }
        }
    }

    private List<Event<Municipality>> getEvents(int beginYear, int endYear, Municipality municipality) throws BadDataException {
        List<Event<Municipality>> events = new ArrayList<Event<Municipality>>();
        CSVReader reader = CSV.getReader(true, municipality, inputFilename, separator);
        // read all events in the file
        Integer[] nextIntegersLine;
        while((nextIntegersLine = CSV.nextIntegersLine(reader))!=null) {
            int date = nextIntegersLine[0];
            if (date > endYear) {
                Logger.getLogger(DistributedTableUpdater.class.getSimpleName()).log(Level.WARNING, "This scenario input file gives event"
                        + " that should occurs after the time period defined by the scenario\n  table name:"
                        + "{0}\n  input file:{1}  municipality concerned:{2}",
                        new Object[]{updaterMethodName, inputFilename, municipality.getName()});
            } else {
                // ensure the list size
                while (events.size() <= date - beginYear) {
                    events.add(dummyEvent);
                }
                events.set(date - beginYear, new TableUpdaterEvent(updaterMethod, finalUpdaterMethod, mode, municipality, Arrays.copyOfRange(nextIntegersLine, 1, nextIntegersLine.length)));
                if (nextIntegersLine.length - 1 > tableSize) {
                    tableSize = nextIntegersLine.length - 1;
                }
            }
        }
        try {
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("", ex);
        }
        return events;
    }
}
