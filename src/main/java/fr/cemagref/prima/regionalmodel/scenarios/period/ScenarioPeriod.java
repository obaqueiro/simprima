/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios.period;

import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.dynamics.Dynamics;
import fr.cemagref.prima.regionalmodel.scenarios.Event;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Wraps a set of scenarios for a time period (bounds included).
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class ScenarioPeriod extends Dynamics<MunicipalitySet> {

    private int beginYear;
    private int endYear;
    private List<PartialTableUpdater> scenarios;
    private PeriodDistributionMethod defaultDistributionMethod;
    private transient List<List<Event<MunicipalitySet>>> events;

    /**
     * Constructor
     *
     * @param beginYear
     * @param endYear
     * @param scenarios
     * @param defaultDistributionMethod This method will be used for scenarios that don't provide one.
     */
    public ScenarioPeriod(int beginYear, int endYear, List<PartialTableUpdater> scenarios, PeriodDistributionMethod defaultDistributionMethod) {
        super();
        this.beginYear = beginYear;
        this.endYear = endYear;
        this.scenarios = scenarios;
        this.defaultDistributionMethod = defaultDistributionMethod;
    }

    @Override
    public void init(MunicipalitySet region) throws BadDataException {
        super.init(region);
        // init events distribution in the time period
        events = new ArrayList<List<Event<MunicipalitySet>>>();
        for (int i = 0; i < endYear - beginYear + 1; i++) {
            events.add(new ArrayList<Event<MunicipalitySet>>());
        }
        for (PartialTableUpdater scenario : scenarios) {
            // init the scenario
            scenario.init(region);
            scenario.setDefaultPeriodDistributionMethod(defaultDistributionMethod);
            scenario.initEventsDistributions(beginYear, endYear);
            // queueing the events
            assert scenario.getEvents().size() == events.size() : "Bad number of events";
            for (int i = 0; i < scenario.getEvents().size(); i++) {
                events.get(i).add(scenario.getEvents().get(i));
            }
        }
    }

    @Override
    public void step(MunicipalitySet region, int iter) throws ProcessingException {
        if (iter - region.getStartStep() < events.size()) {
            for (Event<MunicipalitySet> event : events.get(iter - region.getStartStep())) {
                event.process(region);
            }
        }
    }

    public List<PartialTableUpdater> getScenarios() {
        return scenarios;
    }
}
