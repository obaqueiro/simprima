/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios.period;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.scenarios.Event;
import fr.cemagref.prima.regionalmodel.scenarios.TableUpdater;
import fr.cemagref.prima.regionalmodel.scenarios.TableUpdaterEvent;
import fr.cemagref.prima.regionalmodel.scenarios.period.distmethod.GivenDistribution;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A generic scenario that will update a table in concerned municipalities by
 * applying delta on category given by the input file for a given period.
 *
 * This object is suited to be part of a {@link ScenarioPeriod}.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class PartialTableUpdater extends TableUpdater {

    private PeriodDistributionMethod periodDistributionMethod;

    public PartialTableUpdater(String inputFilename, String methodName) {
        super(inputFilename, methodName);
    }

    public PartialTableUpdater() {
        super();
    }

    @Override
    protected void initEventsDistributionsForMunicipality(int beginYear, int endYear, Municipality municipality, List<Event<Municipality>>[] eventsDistributed) throws BadDataException {
        if (periodDistributionMethod instanceof GivenDistribution) {
            ((GivenDistribution) periodDistributionMethod).loadDataForMunicipality(municipality);
        }
        List<Event<Municipality>> events = getEvents(municipality);
        // the distribution is done for each municipality
        periodDistributionMethod.fillEventsDistribution(municipality, events, endYear - beginYear + 1, eventsDistributed);
        // putting events in the maps
        for (int i = 0; i < eventsDistributed.length; i++) {
            eventsWrappers.get(i).put(municipality, aggregatePerMunicipalityAndTimestepEvents(municipality, eventsDistributed[i]));
        }
    }

    private List<Event<Municipality>> getEvents(Municipality municipality) throws BadDataException {
        List<Event<Municipality>> events = new ArrayList<Event<Municipality>>();
        CSVReader reader = CSV.getReader(true, municipality, inputFilename, separator);
        Integer[] nextIntegersLine = CSV.nextIntegersLine(reader);
        try {
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("", ex);
        }
        for (int category = 0; category < nextIntegersLine.length; category++) {
            Integer delta = nextIntegersLine[category];
            // split the delta to have atomic event
            if (delta != 0) {
                int inc = delta / Math.abs(delta);
                for (int i = 0; i < Math.abs(delta); i++) {
                    events.add(new TableUpdaterEvent(updaterMethod, finalUpdaterMethod, mode, municipality, category, inc));
                }
            }
            if (category >= tableSize) {
                tableSize = category + 1;
            }
        }
        return events;
    }

    private List<Event<Municipality>> aggregatePerMunicipalityAndTimestepEvents(Municipality municipality, List<Event<Municipality>> eventsToAggregate) throws BadDataException {
        List<Event<Municipality>> result = new ArrayList<Event<Municipality>>();
        Integer[] deltas = new Integer[tableSize];
        Arrays.fill(deltas, 0);
        TableUpdaterEvent aggregatedEvent = new TableUpdaterEvent(updaterMethod, finalUpdaterMethod, mode, municipality, deltas);
        result.add(aggregatedEvent);
        for (Event event : eventsToAggregate) {
            TableUpdaterEvent tableUpdaterEvent = (TableUpdaterEvent) event;
            int category = 0;
            for (int i = 0; i < tableUpdaterEvent.getDeltasSize(); i++) {
                if (tableUpdaterEvent.getDelta(i) != 0) {
                    category = i;
                    break;
                }
            }
            aggregatedEvent.incDelta(category, tableUpdaterEvent.getDelta(category));
        }
        return result;
    }

    public PeriodDistributionMethod getPeriodDistributionMethod() {
        return periodDistributionMethod;
    }

    public void setDefaultPeriodDistributionMethod(PeriodDistributionMethod periodDistributionMethod) {
        if (this.periodDistributionMethod == null) {
            this.periodDistributionMethod = periodDistributionMethod;
        }
    }
}
