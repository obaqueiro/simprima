/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import fr.cemagref.prima.regionalmodel.Household;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class ImmigrationEvent implements Event<Municipality> {

    protected final static Logger LOGGER = Logger.getLogger(ImmigrationEvent.class.getSimpleName());
    protected int nbHHImmigrants;
    protected int migratoryBalance;

    public ImmigrationEvent(String param, boolean useMigratoryBalance) {
        if (useMigratoryBalance) {
            this.nbHHImmigrants = -1;
            this.migratoryBalance = Integer.parseInt(param);
        } else {
            this.nbHHImmigrants = Integer.parseInt(param);
        }
    }

    @Override
    public final void process(Municipality municipality) throws ProcessingException {
        LOGGER.log(Level.FINER, "In municipality {0}: {1} HH immigrants incoming", new Object[]{municipality.getName(), nbHHImmigrants});
        Household[] immigrants;
        // Action for the past period (when we exactly know the number of in-migrant households
        if (nbHHImmigrants >= 0) {
            immigrants = listPossibleMigrants(municipality.getMyRegion(), nbHHImmigrants);
        } else {
            int nbImmigrants = this.migratoryBalance;
            // for the first step, the nbImmigrants is precomputed with a given nbOutMigrants
            if (municipality.getMyRegion().getCurrentYear() > municipality.getMyRegion().getStartStep()) {
                nbImmigrants += municipality.getMyRegion().getNbOutMigrants();
            }
            int nbMunMig = (int) Math.round(nbImmigrants * (double) municipality.getPopulationSize() / municipality.getMyRegion().getPopulationSize());
            immigrants = listPossibleMigrants(municipality.getMyRegion(), nbMunMig);
        }
        municipality.addTheMigrants(immigrants);
        int immigIndiv = 0;
        int immigrantLess31 = 0;
        for (Household hh : municipality.getMyHouseholds()) {
            if (hh.getResidence().isTransit()) {
                postprocessImmigrants(hh);
                immigIndiv = immigIndiv + hh.size();
                for (Individual ind : hh) {
                    if (ind.getAge() < 31) {
                        immigrantLess31++;
                    }
                }
            }
        }
        LOGGER.log(Level.FINER, "In municipality {0}: {1} immigrants incoming", new Object[]{municipality.getName(), immigIndiv});
        LOGGER.log(Level.FINER, "In municipality {0}: {1} immigrants incoming being less than 31", new Object[]{municipality.getName(), immigrantLess31});
    }

    /**
     * Method to generate potential migrants S. Huet, 3.11.2010 by adding some randomly chosen household having the convenient properties
     */
    protected final Household[] listPossibleMigrants(MunicipalitySet municipalitySet, int nb) {
        int g = 0;
        int h = 0;
        Household[] hshMigrantsOfMun;
        // for the past period
        if (nbHHImmigrants >= 0) {
            hshMigrantsOfMun = new Household[nb];
            for (int i = 0; i < nb; i++) {
                g = municipalitySet.getRandom().nextInt(0, municipalitySet.getMyMunicipalities().size() - 1);
                Municipality mun = municipalitySet.getMyMunicipalities().get(g);
                if (mun.getMyHouseholds().size() > 0) {
                    h = municipalitySet.getRandom().nextInt(0, mun.getMyHouseholds().size() - 1);
                    Household hh = mun.getMyHouseholds().get(h);
                    if (evaluate(hh)) {
                        hshMigrantsOfMun[i] = hh;
                    } else {
                        i--;
                    }
                }
            }
        } else {
            // for the future period, since 2006 for Condat
            List<Household> hshM = new ArrayList();
            for (int i = 0; i < nb;) {
                g = municipalitySet.getRandom().nextInt(0, municipalitySet.getMyMunicipalities().size() - 1);
                Municipality mun = municipalitySet.getMyMunicipalities().get(g);
                if (mun.getMyHouseholds().size() > 0) {
                    h = municipalitySet.getRandom().nextInt(0, mun.getMyHouseholds().size() - 1);
                    Household hh = mun.getMyHouseholds().get(h);
                    if (evaluate(hh)) {
                        hshM.add(hh);
                        i = i + hh.size();
                    }
                }
            }
            hshMigrantsOfMun = new Household[hshM.size()];
            for (int j = 0; j < hshM.size(); j++) {
                hshMigrantsOfMun[j] = hshM.get(j);
            }
        }
        //System.err.println("nb immigrants trouvés " + nb+" nb migrant out "+municipalitySet.getNbOutMigrants());
        return hshMigrantsOfMun;
    }

    /**
     * Method to decide if an household can represent an immigrant household S. Huet, 3.11.2010
     */
    protected boolean evaluate(Household hh) {
        return (!hh.getResidence().isTransit());
    }

    /**
     * This method is called after the insertion of immigrants. It is a hook to perform additionnal
     * processing of these new households.
     * @param hh
     */
    protected void postprocessImmigrants(Household hh) {
    }
}
