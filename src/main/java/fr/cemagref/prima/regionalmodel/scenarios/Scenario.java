/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.dynamics.Dynamics;
import fr.cemagref.prima.regionalmodel.scenarios.grouping.MunicipalitiesFilter;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A scenario is considered as a predeterminated list of events that can occur
 * on a subset of municipalities of a region.
 * Filtering of municipalities is only processed at the begin of the simulation.
 *
 * These events can then be distributed in the time period according
 * to a specified method.
 *
 * Thus, if you provide data than describe a variation of
 * a quantity in the period, but you don't know at which date occurs the event,
 * the used method will determine at which date will occur each of these events.
 * In this case, you can consider to discretize the given variation if it makes
 * sense.
 *
 * For example, you want to add a scenario that will increase the property
 * <tt>p</tt> in your model by a value of 5. You may consider that this is 5 times
 * an augmentation of 1, and consequently indicate that you will give 5 events,
 * and each event will increase the property <tt>p</tt> by 1.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
// TODO maybe the Dynamics inheritance isn't needed as the process method is useless
public abstract class Scenario extends Dynamics<MunicipalitySet> {

    private static final Logger LOGGER = Logger.getLogger(Scenario.class.getSimpleName());
    protected String inputFilename;
    protected Character separator;
    /**
     * Filter that will be used to determine on which municipalities this scenario
     * should be applied. All municipalities will be accepted if this filter isn't
     * defined.
     */
    protected MunicipalitiesFilter municipalitiesFilter;
    /**
     * Filtered municipalities on which should be applied the scenario.
     */
    protected transient List<Municipality> municipalities;

    public final String getInputFilename() {
        return inputFilename;
    }

    public final Character getSeparator() {
        return separator;
    }

    public final void setInputFilename(String inputFilename) {
        this.inputFilename = inputFilename;
    }

    public final void setSeparator(Character separator) {
        this.separator = separator;
    }

    @Override
    public void init(MunicipalitySet region) throws BadDataException {
        super.init(region);
        // Init the concerned municipalities
        municipalities = new ArrayList<Municipality>();
        if (municipalitiesFilter == null) {
            municipalities.addAll(region.getMyMunicipalities());
            municipalities.addAll(region.getOutsides());
        } else {
            for (Municipality municipality : region.getMyMunicipalities()) {
                if (municipalitiesFilter.accept(municipality)) {
                    municipalities.add(municipality);
                }
            }
            for (Municipality municipality : region.getOutsides()) {
                if (municipalitiesFilter.accept(municipality)) {
                    municipalities.add(municipality);
                }
            }
        }
        LOGGER.log(Level.FINER, "{0} municipalities included in scenario with input file {1}", new Object[]{municipalities.size(), this.inputFilename});
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.log(Level.FINEST, "Municipalities included: {0}", municipalities);
        }
    }

    /**
     * Gives the list of events for each timestep.
     * @return
     * @throws BadDataException
     */
    public abstract List<? extends Event<MunicipalitySet>> getEvents();

    @Override
    public final void step(MunicipalitySet handler, int iter) throws ProcessingException {
        // ensure that there are events for this iteration
        if (iter<getEvents().size()){
            preStep();
            getEvents().get(iter).process(handler);
        }
    }
    
    /**
     * Function executed before executing the events of this step.
     * Should be overriden if something needs to be done before applying the scenario
     */
    protected void preStep(){};
        
    
}
