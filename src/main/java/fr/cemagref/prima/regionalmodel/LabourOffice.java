/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import java.util.*;
import org.apache.commons.lang.ArrayUtils;

/**
 * Provides the availability of jobs in the region.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class LabourOffice {

    private SortedMap<Activity, List<Municipality>> availabilitiesByJob;
    private MunicipalitySet region;
    private MunicipalitiesNetwork network;

    public LabourOffice(MunicipalitySet region) {
        this.region = region;
    }

    public void initBase() throws BadDataException {
        // init the base of municipalities by job
        availabilitiesByJob = new TreeMap<Activity, List<Municipality>>();
        for (Activity act : region.getAllActivities()) {
            List<Municipality> list = new ArrayList<Municipality>();
            availabilitiesByJob.put(act, list);
            for (Municipality municipality : region.myMunicipalities) {
                if (municipality.getFreeActivities(act) > 0) {
                    list.add(municipality);
                }
            }
        }
    }

    public void initNetwork() throws BadDataException {
        network = new MunicipalitiesNetwork(region.getParameters().getJobResearchMunNetworkClasses(), region.getParameters().getJobResearchMunNetworkProbas());
        network.load(region);
    }

    /**
     * Find the municipalities in which a job is available.
     *
     * @param pattern The type of job
     * @return The list of matching municipalities
     */
    public JobProposition getMunicipalitiesForJob(Activity job) {
        assert availabilitiesByJob!=null:
                String.format("availabilitiesByJob must NOT be null at this point");        
        if (!availabilitiesByJob.containsKey(job)){
            availabilitiesByJob.put(job, new ArrayList<Municipality>());
        }
        return new JobProposition(job, availabilitiesByJob.get(job).toArray(new Municipality[]{}));
    }

    /**
     * Find the municipalities in which an activity pattern is available. A
     * municipality have to match all the jobs of the pattern to be selected. It
     * is assumed that looking at the regional level, in case of pluri-active
     * activity pattern, the two jobs have to be located in the same
     * municipality
     *
     * @param job The activity pattern looked for
     * @return The list of matching municipalities
     */
    public List<JobProposition> getMunicipalitiesForActivity(Activity job) {
        List<Municipality> municipalities = new ArrayList<Municipality>();
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        assert job != null : "Job must NOT be null at this point";

        // get all municipalities for the first job of the pattern
        municipalities.addAll(availabilitiesByJob.get(job));

        for (Municipality mun : municipalities) {
            jobPropositions.add(new JobProposition(job, new Municipality[]{mun}));
        }
        return jobPropositions;
    }

    /**
     * Find the municipalities providing activity corresponding to a given
     * profession. Only monoactivity patterns are taken into consideration.
     *
     * May return two jobProposition for same municipalities, but for two
     * different patterns that match the profession.
     *
     * @param profession The profession looked for
     * @return The list of matching job propositions
     */
    public List<JobProposition> getMunicipalitiesForProfession(int profession) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        for (Activity act : this.availabilitiesByJob.keySet()) {
            if (act.getSPC() == profession) {
                jobPropositions.addAll(getMunicipalitiesForActivity(act));
            }
        }
        return jobPropositions;
    }

    /**
     * Find the municipalities in a neighbourhood in which an activity is
     * available.
     *
     * @param act The activity looked for
     * @param municipality where the individual lives
     * @return The list of matching municipalities
     */
    public List<JobProposition> getLocalMunicipalitiesForActivity(Activity act, Municipality municipality) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();           
        if (network.isLoaded()) {
            SortedMap<Double, List<Municipality>> neighbourhoodComplete = network.get(municipality);
            List<Municipality> pickedNeighbourhood = region.getRandom().nextMapObjectWithDistributionInKeys(neighbourhoodComplete);
            // for each municipality matching this job of the pattern
            for (Municipality foundMun : availabilitiesByJob.get(act)) {
                // if municipality in neighbourhood
                if (pickedNeighbourhood.contains(foundMun)) {
                    // OK, let's add it to the list
                    jobPropositions.add(new JobProposition(act, foundMun));
                }
            }
        } else {
            // for each municipality matching this job of the pattern
            for (Municipality foundMun : availabilitiesByJob.get(act)) {
                // add job proposition to the list if it is in the current municipality
                // or in the list of connected municipalities
                if ((foundMun == municipality) ||                         
                        ArrayUtils.contains(municipality.getProximityJob(), foundMun)){               
                       // OK, let's add it to the list
                    jobPropositions.add(new JobProposition(act, foundMun));
                }
            }
        }
                 
        assert jobPropositions != null : "jobPropositions must NOT be null at this point";
        return jobPropositions;
    }

    /**
     * Returns a list of job propositions available in the Outside region and
     * with the specified SPC 
     * @param spc The SPC that is looked for in a job
     * @return List of job propositions available (or empty list if none found)
     */
    public List<JobProposition> getOfferInOutside(int spc) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        for (Activity act : this.availabilitiesByJob.keySet()) {
            // skip activities with other SPC
            if (act.getSPC()!=spc){
                continue;
            }
            for (Municipality outside : region.getOutsides()) {
                if (outside.getFreeActivities(act) > 0) {
                    jobPropositions.add(new JobProposition(act, outside));
                }
            }
        }
        return jobPropositions;
    }

    /**
     * Find the municipalities in the municipality and in its reachable outside
     * where there are jobs available with the specified SPC
     *
     * @param vil The municipalities determining the neighbourhood where to search
     * @param spc The socio-professional category looked for
     * @return The list of matching municipalities
     */
    public List<JobProposition> getLocalMunicipalitiesForProfession(Municipality vil, int spc) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        for (Activity act : this.availabilitiesByJob.keySet()) {
            if (act.getSPC() != spc){
                continue;
            }
            List<JobProposition> localMunicipalitiesForActivityPattern = getLocalMunicipalitiesForActivity(act, vil);
            jobPropositions.addAll(localMunicipalitiesForActivityPattern);
        }
        return jobPropositions;
    }

    /**
     * Update the job availability of the municipality, if a job is taken off.
     *
     * @param job
     * @param municipality
     */
    public void takeJob(Activity job, Municipality inMunicipality) {
        if (inMunicipality.getFreeActivities(job) == 0) {
            availabilitiesByJob.get(job).remove(inMunicipality);
        }
    }

    /**
     * Update the job availability of the municipality, if a job is released.
     *
     * @param job
     * @param municipality
     */
    public void releaseJob(Activity job, Municipality inMunicipality) {
        if (!availabilitiesByJob.containsKey(job)){
            availabilitiesByJob.put(job, Arrays.asList(inMunicipality));
            return;
        }
        if ((inMunicipality.getFreeActivities(job) > 0) && !(availabilitiesByJob.get(job).contains(inMunicipality))) {
            availabilitiesByJob.get(job).add(inMunicipality);
        }
    }

    /**
     * Recompute the data of the labor office for a job against the availability
     * of jobs in this municipality.
     *
     * @param job
     * @param municipality
     */
    public void updateAvailabilities(Activity job, Municipality municipality) {
        if (availabilitiesByJob.get(job).contains(municipality)) {
            if (municipality.getFreeActivities(job) <= 0) {
                availabilitiesByJob.get(job).remove(municipality);
            }
        } else {
            if (municipality.getFreeActivities(job) > 0) {
                availabilitiesByJob.get(job).add(municipality);
            }
        }
        //int availCompteur = (int) (municipality.getOfferedActivities()[job] - (municipality.getOccupiedActivitiesByRes()[job]
        //+ municipality.getOccupiedActivitiesByExt()[job] + municipality.getOccupiedActivitiesByOutside()[job]));
        //if (job == 5) System.err.println("job "+job+" available in "+availabilitiesByJob[job].toString()+" avail with counters "+availCompteur+" for mun "+municipality) ;
    }

    public void updateAvailabilities(Municipality municipality) {
        for (Activity act : availabilitiesByJob.keySet())
         {
            updateAvailabilities(act, municipality);
        }
    }

    public static class JobProposition {

        private Activity job;
        private Municipality[] jobsLocations;

        JobProposition(Activity job, Municipality... jobsLocations) {
            this.job = job;
            this.jobsLocations = jobsLocations;
        }

        /**
         * Provides the municipalities that match for the jobs of the wanted
         * pattern. The order of the municipalities correspond to the order of
         * jobs in the pattern.
         *
         * @return an array of municipalities
         */
        public Municipality[] getJobsLocations() {
            return jobsLocations.clone();
        }

        public Activity getActivity() {
            return job;
        }

        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder(job.toString());
            for (Municipality municipality : jobsLocations) {
                buf.append(", ").append(municipality);
            }
            return buf.toString();
        }
    }
}
