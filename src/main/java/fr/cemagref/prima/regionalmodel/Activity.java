package fr.cemagref.prima.regionalmodel;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * The set of possible activities (different jobs) that the population of 
 * the village could have. The activities that people can choose can be 
 * described in terms of sector, level of revenue, surface and place of 
 * work (inside or outside the village).
 */
public class Activity implements Comparable<Activity> {

    /**
     * Sector of activity. Can be something as Agriculture, Industry, Building and Services
     * Note: A sector of activity of -1 can be used to denote an UNEMPLOYED individual
     * as unemployed individuals must not have any SoA defined.
     */
    private int sectorOfActivity;

    public int getSoA() {
        return sectorOfActivity;
    }

    public final void setSoA(final int newSector) {
        this.sectorOfActivity = newSector;
    }
    /**
     * The profession required in the sector of activity. For example, in Auvergne 
     * the profession is represented by the socio-professional category coded as
     * 0 agriculteur exploitant, 1 artisan et al, 2 cadres,
     * 3 professions intermediaires, 4 employés, 5 ouvriers
     */
    private int socioProfessionalCategory;

    public int getSPC() {
        return socioProfessionalCategory;
    }

    public final void setSPC(final int prof) {
        this.socioProfessionalCategory = prof;
    }

    /**
     * Constructor for activities related to unemployed individuals.
     * When an individual is unemployed, only the SPC is considered
     * @param spc The SPC of the activity
     */
    public Activity(final int spc){
        this(-1,spc);
    }
    /*
     *Constructor Activity
     * @param soa Sector of Activity (SoA) for the created activity (-1 for unemployed individuals)
     * @param spc Socio Professional Category of this activity
     */
    public Activity(final int soa, final int spc) {
        setSoA(soa);
        setSPC(spc);
    }

    
    /**
     * Two activities are equal if both their SocioProfessionalCategory (SPC) and Sector 
     * of Activity (SoA) are equal.
     * @param Object to compare to.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
             return true;
         }
         
        if (!(obj instanceof Activity)){
            return false;
        }
        
        Activity otherAct = (Activity) obj;
        
         if (this.getSPC() == otherAct.getSPC() 
                 && this.getSoA() == otherAct.getSoA()){
             return true;
         }
         
         return false;
   }
    
    @Override
    public int hashCode(){                
        return (new HashCodeBuilder().append(this.getSoA()).append(this.getSPC())).toHashCode();
    }
    
    @Override 
    public String toString(){
        return new StringBuilder().append(String.valueOf(getSoA())).append("/").append(String.valueOf(getSPC())).toString();
    }

    /**
     * Compares two activities by the values of their Sector of Activity and Socio Professional Category.
     * An activity A is considered greater than another activity B if:
     * - A.SoA > B.SoA
     * Or if:
     * - A.SoA == B.SoA and A.SPC > B.SPC
     * 
     * So if the SoA in activity A is higher than the one in B, the A is considered greater ignoring the value of SPC.
     * @param o Activity to compare to.
     * @return  A negative value if this Activity is less than parameter, zero if both activities are equal and positive if this
     * activity is greater than the parameter.
     */
    @Override
    public int compareTo(Activity o) {
        if (this.equals(o)){
            return 0;
        }
        int cmp = this.sectorOfActivity - o.sectorOfActivity;
        
        if (cmp==0){
            cmp = this.socioProfessionalCategory - o.socioProfessionalCategory;
        }
        
        return cmp;
        
    }
    
    public boolean isUnemployed(){
        return sectorOfActivity == -1;
    }
}

