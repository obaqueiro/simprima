/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author dumoulin
 */
public class RealEstateAgency {
    public boolean findNewResidence(Household hh) {
        Individual leader = hh.determineLeader();
        boolean fireOtherWorkers = false;
        // determine where to search
        List<Municipality> whereToSearch = hh.getJobAndResidenceProximityIntersection();
        if (whereToSearch.isEmpty()) {
            whereToSearch = Arrays.asList(leader.getCurrentJobLocation().getProximityJob()); 
        }
        // search a residence
        hh.setNextResMunicipality(null);
        hh.setNextSizeOfResidence(-1);
        int size = hh.getMyVillage().appropriatedSizeOfRes(hh.size());
        for (int index : hh.getRandom().randomList(whereToSearch.size())) {
            Municipality mun = whereToSearch.get(index);             
            if (mun.getFreeResidences(size).isEmpty()) 
            {
                // we consider that it is possible to accept the dwelling of the just upper size (+1 room)
                if (size <= (hh.getMyVillage().getMyRegion().getNbSizesResidence()-2)) { // max size - 1 (0, 1 , 2, 3)
                    size = size + 1;
                    for (int indx : hh.getRandom().randomList(whereToSearch.size())) {
                        mun = whereToSearch.get(indx);
                        if (!mun.getFreeResidences(size).isEmpty()) {                            
                                hh.setNextResMunicipality(mun);
                                hh.setNextSizeOfResidence(size);                            
                            break;
                        }
                    }
                }
            }
            else 
            {               
                hh.setNextResMunicipality(mun);
                hh.setNextSizeOfResidence(size);
                break;
            }
        }
        return fireOtherWorkers;
    }
}
