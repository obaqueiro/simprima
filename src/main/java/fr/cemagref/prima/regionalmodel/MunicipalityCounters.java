/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalityCounters {

    /**
     * Number of new households during a given iteration
     */
    private int newHouseholdNb;
    /**
     * Number of households moving out during an iteration (household which leave the village)
     */
    private int nbHouseholdMoveOut;
    /**
     * Number of households moving inside during an iteration (household which probabilisticMove without changing of village)
     */
    private int nbHouseholdMoveInside;
    /**
     * Number of households moving in during an iteration (household who enter into the village)
     */
    private int nbHouseholdMoveIn;
    /**
     * Number of households suppressed because they have joined or because one people died
     */
    private int nbHouseholdSuppressed;
    /**
     * Number of children suppressed due to the fact that their parents are died, whatever their age.
     */
    private int nbKilledOrphans;
    /**
     * Number of households suppressed because of the housing scenario
     */
    private int nbHouseholdExpulsed;
    /**
     * Number of households moving out during an iteration (household which leave the village)
     */
    private int nbIndividualMoveOut;
    /**
     * Number of households moving in during an iteration (household who enter into the village)
     */
    private int nbIndividualMoveIn;
    /**
     * Number of individuals moving inside during an iteration (household who enter into the village)
     */
    private int nbIndividualMoveInside;
    /**
     * Number of individuals moving form any one of the villages in the network to the "outside".
     * <b>Not</b> to be confused with nbIndividualMoveOut which only counts individuals who moved
     * from this village to another village within the network.
     */
    private int nbIndivdiualMoveOutside;


    private int numBirth;
    private int numDeath;
    /**
     * Number of households without a house during the current iteration
     */
    private int nbOfDwellingLacking;

    public int getNewHouseholdNb() {
        return newHouseholdNb;
    }

    public int getNbHouseholdMoveIn() {
        return nbHouseholdMoveIn;
    }

    public int getNbHouseholdMoveInside() {
        return nbHouseholdMoveInside;
    }

    public int getNbHouseholdMoveOut() {
        return nbHouseholdMoveOut;
    }

    public int getNbHouseholdSuppressed() {
        return nbHouseholdSuppressed;
    }

    public int getNbIndivdiualMoveOutside () {
        return this.nbIndivdiualMoveOutside;
    }
    public int getNbKilledOrphans() {
        return nbKilledOrphans;
    }

    public int getNbHouseholdExpulsed() {
        return nbHouseholdExpulsed;
    }

    public int getNbIndividualMoveIn() {
        return nbIndividualMoveIn;
    }

    public int getNbIndividualMoveInside() {
        return nbIndividualMoveInside;
    }

    public int getNbIndividualMoveOut() {
        return nbIndividualMoveOut;
    }

    public int getNumBirth() {
        return numBirth;
    }

    public int getNumDeath() {
        return numDeath;
    }

    public int getNbOfDwellingLacking() {
        return nbOfDwellingLacking;
    }

    public void incNewHouseholdNb(int newHouseholdNb) {
        this.newHouseholdNb += newHouseholdNb;
    }

    public void incNbHouseholdMoveIn(int nbHouseholdMoveIn) {
        this.nbHouseholdMoveIn += nbHouseholdMoveIn;
    }

    public void incNbHouseholdMoveInside(int nbHouseholdMoveInside) {
        this.nbHouseholdMoveInside += nbHouseholdMoveInside;
    }

    public void incNbHouseholdMoveOut(int nbHouseholdMoveOut) {
        this.nbHouseholdMoveOut += nbHouseholdMoveOut;
    }

    public void incNbHouseholdSuppressed(int nbHouseholdSuppressed) {
        this.nbHouseholdSuppressed += nbHouseholdSuppressed;
    }

    public void incNbKilledOrphans(int nbKilledOrphans) {
        this.nbKilledOrphans += nbKilledOrphans;
    }

    public void incNbHouseholdExpulsed(int nb) {
        this.nbHouseholdExpulsed += nb;
    }

    public void incNbIndividualMoveIn(int nbIndividualMoveIn) {
        this.nbIndividualMoveIn += nbIndividualMoveIn;
    }

    public void incNbIndividualMoveInside(int nbIndividualMoveInside) {
        this.nbIndividualMoveInside += nbIndividualMoveInside;
    }

    public void incNbIndividualMoveOut(int nbIndividualMoveOut) {
        this.nbIndividualMoveOut += nbIndividualMoveOut;
    }

    public void incNbIndividualMoveOutside(int increment) {
        this.nbIndivdiualMoveOutside += increment;
    }

    public void incNbOfDwellingLacking(int nbOfDwellingLacking) {
        this.nbOfDwellingLacking += nbOfDwellingLacking;
    }

    public void incNumBirth(int numBirth) {
        this.numBirth += numBirth;
    }

    public void incNumDeath(int numDeath) {
        this.numDeath += numDeath;
    }

    private int initialPopulation;
    public void setInitialPopulation(int newInitial) {
        initialPopulation = newInitial;
    }
    
    public int getInitialPopulation() {
        return this.initialPopulation;
    }
    public void resetCounters() {
        newHouseholdNb = 0;
        nbHouseholdMoveOut = 0;
        nbHouseholdMoveInside = 0;
        nbHouseholdMoveIn = 0;
        nbHouseholdSuppressed = 0;
        nbKilledOrphans = 0;
        nbHouseholdExpulsed = 0;
        nbIndividualMoveOut = 0;
        nbIndividualMoveIn = 0;
        nbIndividualMoveInside = 0;
        nbIndivdiualMoveOutside=0;
        numBirth = 0;
        numDeath = 0;
        nbOfDwellingLacking = 0;
        initialPopulation=0;
        
    }
}
