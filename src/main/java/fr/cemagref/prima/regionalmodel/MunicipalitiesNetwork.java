/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.util.*;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalitiesNetwork {

    private SortedMap<Municipality, SortedMap<Double, List<Municipality>>> probasByMunInNetwork;
    private String munNetworkClassesFilename;
    private String munNetworkProbasFilename;

    public MunicipalitiesNetwork(String munNetworkClassesFilename, String munNetworkProbasFilename) {
        this.munNetworkClassesFilename = munNetworkClassesFilename;
        this.munNetworkProbasFilename = munNetworkProbasFilename;
    }

    
    public void load(MunicipalitySet region) throws BadDataException {
        String municipalitiesDistancesFilename = region.getParameters().getMunicipalitiesDistances();
        if (munNetworkClassesFilename == null || munNetworkProbasFilename == null || municipalitiesDistancesFilename == null) {
            Logger.getLogger(MunicipalitiesNetwork.class.getSimpleName()).warning("No input file found for municipalities network for job research, fallback to old mode");
            return;
        }
        // init the network
        probasByMunInNetwork = new TreeMap<Municipality, SortedMap<Double, List<Municipality>>>();
        // loading municipalities classes
        SortedMap<Integer, Integer> munClasses = new TreeMap<Integer, Integer>();
        try {
            CSVReader classesReader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(munNetworkClassesFilename), null);
            Integer[] line;
            while ((line = CSV.nextIntegersLine(classesReader)) != null) {
                munClasses.put(line[0], line[1]);
            }
        } catch (Exception ex) {
            throw new BadDataException("error while loading municipalities classes", ex);
        }
        // loading distances classes probabilities
        SortedMap<Double, Double[]> probas = new TreeMap<Double, Double[]>();
        try {
            CSVReader probasReader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(munNetworkProbasFilename), null);
            String[] line;
            while ((line = CSV.readLine(probasReader)) != null) {
                probas.put(Double.parseDouble(line[0]), Utils.parseDoubleArray(line, 1));
            }
        } catch (Exception ex) {
            throw new BadDataException("error while loading municipalities probabilities", ex);
        }
        // loading the distances and building the network
        try {
            CSVReader distancesReader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(municipalitiesDistancesFilename), null);
            String[] readLine = CSV.readLine(distancesReader);
            Integer[] munLabels = Utils.parseIntegerArray(readLine, 1);
            String[] line;
            // for each line of distances
            while ((line = CSV.readLine(distancesReader)) != null) {
                // storing the class of the current municipality
                int munClass = munClasses.get(Integer.parseInt(line[0]));
                SortedMap<Double, List<Municipality>> currentProbas = new TreeMap<Double, List<Municipality>>();
                // for each distance with each other municipality
                for (int i = 0; i < munLabels.length; i++) {
                    double distance = Double.parseDouble(line[i + 1]);
                    // searching the distance class
                    Double distanceUpperBound = probas.firstKey();
                    for (Double d : probas.keySet()) {
                        if (d >= distance) {
                            break;
                        }
                        distanceUpperBound = d;
                    }
                    // getting the associated proba
                    Double proba = probas.get(distanceUpperBound)[munClass - 1];
                    // and storing the municipality in the appropriated list of the network
                    List<Municipality> muns = currentProbas.get(proba);
                    if (muns == null) {
                        muns = new ArrayList<Municipality>();
                        currentProbas.put(proba, muns);
                    }
                    muns.add(region.getMunicipality(munLabels[i].toString()));
                }
                probasByMunInNetwork.put(region.getMunicipality(line[0]), currentProbas);
            }
        } catch (Exception ex) {
            throw new BadDataException("error while loading municipalities distances", ex);
        }
    }

    public boolean isLoaded() {
        return probasByMunInNetwork != null;
    }

    public SortedMap<Double, List<Municipality>> get(Municipality key) {
        return probasByMunInNetwork.get(key);
    }
}
