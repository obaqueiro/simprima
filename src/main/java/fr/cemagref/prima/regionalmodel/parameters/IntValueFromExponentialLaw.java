/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import org.openide.util.lookup.ServiceProvider;
import umontreal.iro.lecuyer.randvar.ExponentialGen;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class IntValueFromExponentialLaw<O extends Object> implements Value<Integer, O> {

    private float mean;
    private transient ExponentialGen exponentialGen;

    @Override
    public void init(Application application) throws BadDataException {
        exponentialGen = application.getRandom().createExponentialGenFromMean(mean);
    }

    public float getMean() {
        return mean;
    }

    static IntValueFromExponentialLaw createInstance(Application application, float mu) throws BadDataException {
        IntValueFromExponentialLaw instance = new IntValueFromExponentialLaw();
        instance.mean = mu;
        instance.init(application);
        return instance;
    }

    @Override
    public Integer getValue(Object o) {
        return (int) Math.round(exponentialGen.nextDouble());
    }
}
