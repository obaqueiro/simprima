/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import com.thoughtworks.xstream.XStream;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import java.security.InvalidParameterException;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class AgeDeterminationFromDecisionByProfessionAndAge implements Value<Integer, Individual>, ConfiguredByFile {

    private DecisionByAgeAndProbas[] decisionsByProfession;

    @Override
    public Integer getValue(Individual individual) {
        if (individual.getProfession() < 0) {
            System.err.println();
            throw new InvalidParameterException("This individual has no profession initialized: " + individual
                    + " age " + individual.getAge()
                    + " transit " + individual.getHousehold().getResidence().isTransit()
                    + " statut " + individual.getStatus()
                    + " prof parent 1" + individual.getParent1().getProfession()
                    + " prof parent 2 " + individual.getParent2().getProfession()
                    + " age enter labour marker " + individual.getAgeToEnterOnTheLabourMarket());
        }
        DecisionByAgeAndProbas decision = decisionsByProfession[individual.getProfession()];
        if (individual.getAge() >= decision.getMaximumAge()) {
            return Integer.valueOf(individual.getAge() + 1);
        } else {
            for (int age = Math.max(decision.getMinimumAge(), individual.getAge()); age <= decision.getMaximumAge(); age++) {
                if (decision.getValue(individual, age)) {
                    return age;
                }
            }
            return decision.getMaximumAge();
        }
    }

    @Override
    public void init(Application application) throws BadDataException {
        for (DecisionByAgeAndProbas decision : decisionsByProfession) {
            decision.init(application);
        }
    }

    @Override
    public String getFilename() {
        StringBuilder buff = new StringBuilder();
        for (DecisionByAgeAndProbas decision : decisionsByProfession) {
            buff.append(decision.getFilename());
        }
        return buff.deleteCharAt(buff.length() - 1).toString();
    }
}
