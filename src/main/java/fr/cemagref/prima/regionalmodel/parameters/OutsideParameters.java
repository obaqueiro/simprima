/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Activity;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class OutsideParameters {

    private String name;
    private List<String> neighbours;
    private SortedMap<Activity, Integer> offeredActivities;

    public SortedMap<Activity, Integer> getOfferedActivities() {
        return offeredActivities;
    }
    private String inputFileName;
 
    public OutsideParameters(Parameters parentParameters, File directory) throws FileNotFoundException {
        //public OutsideParameters(Parameters parentParameters, File directory) {
        //public OutsideParameters(Parameters parentParameters, File municipalityDir) throws IOException {
        this.name = directory.getName();
        Scanner scanner = new Scanner(new File(directory, "proxJob.csv"));
        this.neighbours = Arrays.asList(scanner.nextLine().split(";"));
        scanner.close();
        // reading main input file found in municipality dir
        String outs = directory.getAbsolutePath() + File.separator;
        inputFileName = outs + "village.txt";
        scanner = new Scanner(new File(inputFileName));
        scanner.useDelimiter("\\s+");
        scanner.nextLine();        
        offeredActivities = new TreeMap<Activity, Integer> ();        
        for (int i=0;i<parentParameters.getNbSoA();i++){
            for (int j=0;j < parentParameters.getNbSPC();j++){                
                offeredActivities.put(new Activity(i,j), scanner.nextInt());
            }
        }
        
        scanner.close();
    }

    public String getName() {
        return name;


    }

    public List<String> getNeighbours() {
        return neighbours;

    }
}
