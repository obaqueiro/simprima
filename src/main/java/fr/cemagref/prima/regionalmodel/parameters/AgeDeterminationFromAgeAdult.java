/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import org.openide.util.lookup.ServiceProvider;

/**
 * Determine an age from the age to become adult (other parameter) until a given margin
 * using an uniform distribution in this range. The margin is computed with the inverse of the given argument.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class AgeDeterminationFromAgeAdult implements Value<Integer, Individual> {

    private int ageToBecomeAdult;
    private double parameter;

    @Override
    public Integer getValue(final Individual ind) {
        if (ind.getAge() >= (int) (ageToBecomeAdult + (1.0f / parameter))) {
            return Integer.valueOf(ind.getAge()+1);
        } else {
            return ind.getHousehold().getRandom().nextInt(ageToBecomeAdult, (int) (ageToBecomeAdult + (1.0f / parameter)));
        }
    }

    @Override
    public void init(Application application) throws BadDataException {}
}
