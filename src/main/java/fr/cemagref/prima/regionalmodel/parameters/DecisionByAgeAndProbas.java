/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.openide.util.lookup.ServiceProvider;

/**
 * Parameter that returns a boolean value according to the age of the individual
 * and a list of probas by age given as input.
 *
 * <p>Input format: a csv file with on each line the age range lower bounds included and the proba
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class DecisionByAgeAndProbas implements Value<Boolean, Individual>, ConfiguredByFile {

    private String filename;
    private Character separator;
    private transient SortedMap<Integer, Double> probaByAge;
    private transient Application application;

    public static DecisionByAgeAndProbas createInstance(SortedMap<Integer, Double> probaByAge) {
        DecisionByAgeAndProbas instance = new DecisionByAgeAndProbas();
        instance.probaByAge = probaByAge;
        return instance;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    /**
     * Constructor to create a decision table by providing only the name of the file containing probabilities
     * @param fileName  Name of file containing the probaiblity distribution
     */
    public  DecisionByAgeAndProbas (String fileName){
        super();
        this.filename = fileName;
    }
    
    public DecisionByAgeAndProbas(){
        super();
     }
    
    @Override
    public void init(Application application) throws BadDataException {
        this.application = application;
        try {
            probaByAge = new TreeMap<Integer, Double>();
            String[] data;
            CSVReader reader = CSV.getReader(application.getFileFromRelativePath(filename), separator);
            while ((data = CSV.readLine(reader)) != null) {
                probaByAge.put(Integer.parseInt(data[0]), Double.parseDouble(data[1]));
            }
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("Error while loading " + filename, ex);
        }
        /*int last = probaByAge.firstKey();
        for (Integer age : new ArrayList<Integer>(probaByAge.keySet())) {
            Double lastProba = probaByAge.get(last);
            for (int i = last + 1; i < age; i++) {
                probaByAge.put(i, lastProba);
            }
            last = age;
        }*/
    }

    @Override
    public String getFilename() {
        return filename;
    }

    public Boolean getValue(Individual ind, int age) {
        Double proba = Utils.getValueFromLowerBound(probaByAge, age);
        return application.getRandom().nextDouble() < proba;
    }

    public int getMinimumAge() {
        return probaByAge.firstKey();
    }

    public int getMaximumAge() {
        return probaByAge.lastKey();
    }

    @Override
    public Boolean getValue(Individual individual) {
        return getValue(individual, individual.getAge());
    }
}
