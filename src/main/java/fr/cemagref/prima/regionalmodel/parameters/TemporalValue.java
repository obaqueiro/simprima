/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.util.SortedMap;

/**
 * This class is designed for giving different parameter value among the simulation
 * time.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class TemporalValue<V, O extends Object> implements Value<V, O> {

    SortedMap<Integer, Value<V, O>> valuesAtYear;
    private transient Value<V, O> currentValue;

    /**
     * Update the current value according to the current time.
     * This methods is intended to avoid to retrieve continuously the current
     * value, but only at the begin of each step.
     *
     * @param time
     */
    public void updateCurrentValue(int time) {
        currentValue = Utils.getValueFromLowerBound(valuesAtYear, time);
    }

    @Override
    public V getValue(O object) {
        return currentValue.getValue(object);
    }

    @Override
    public void init(Application application) throws BadDataException {
    }
}
