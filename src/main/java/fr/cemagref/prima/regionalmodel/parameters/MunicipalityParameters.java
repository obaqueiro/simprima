/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Activity;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Parameters for one village
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 * @author Omar Baqueiro Espinosa <baqueiro@iamo.de>
 */
public class MunicipalityParameters {

    private int popsize;
    private String inputFileName;
    private String householdsFilename;
    private String residentsActivityFilename;
    private int totalSurface;
    private SortedMap<Activity, Integer>offeredActivities;    
    private int[] residenceOffer;
    private String municipalityName;
    private String proxJobFile;
    private double lon, lat;
    private double intercept, slope; // parameters of the function to compute the number of employments in the service sector

    public MunicipalityParameters(Parameters parentParameters, File municipalityDir) throws IOException {
        String municipalityDirName = municipalityDir.getAbsolutePath() + File.separator;
        municipalityName = municipalityDir.getName();
        // reading main input file found in municipality dir
        inputFileName = municipalityDirName + "village.txt";
        Scanner scanner = new Scanner(new File(inputFileName));
        scanner.useDelimiter("\\s+");
        scanner.nextLine();
        popsize = scanner.nextInt();
        householdsFilename = municipalityDirName + parentParameters.getPopulationFile();
        proxJobFile = municipalityDirName + parentParameters.getProxJobFile();
        residentsActivityFilename = municipalityDirName + parentParameters.getActivityFile();
        totalSurface = scanner.nextInt();
        offeredActivities = new TreeMap<Activity, Integer> ();
        
        for (int i=0;i<parentParameters.getNbSoA();i++){
            for (int j=0;j < parentParameters.getNbSPC();j++){                
                int val = scanner.nextInt();
                assert  val>=0: "Offered activities must be equal or greater than 0. Found "+Integer.toString(val);
                offeredActivities.put(new Activity(i,j), val);
                

            }
        }

        residenceOffer = new int[parentParameters.getNbSizeRes()];
        for (int i = 0; i < parentParameters.getNbSizeRes(); i++) {
            residenceOffer[i] = scanner.nextInt();
        }
        scanner.useLocale(Locale.US);
        lon = scanner.nextDouble();
        lat = scanner.nextDouble();
        if (parentParameters.getDynamicServiceEmployment() != null) {
            intercept = scanner.nextDouble();
            slope = scanner.nextDouble();
        }
        scanner.close();
    }

    public double getIntercept() {
        return intercept;
    }

    public double getSlope() {
        return slope;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public int getPopsize() {
        return popsize;
    }

    public String getHouseholdsFilename() {
        return householdsFilename;
    }

    public String getInputFileName() {
        return inputFileName;
    }
  
    public SortedMap<Activity, Integer> getOfferedActivities(){              
        return this.offeredActivities;
    }

    public String getResidentsActivityFilename() {
        return residentsActivityFilename;
    }

   
    public int[] getResidenceOffer() {
        return residenceOffer.clone();
    }

    public int getTotalSurface() {
        return totalSurface;
    }

    public String getMunicipalityName() {
        return municipalityName;
    }

    public String getProxJobFile() {
        return proxJobFile;
    }
}
