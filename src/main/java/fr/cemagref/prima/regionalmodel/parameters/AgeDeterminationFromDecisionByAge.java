/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class AgeDeterminationFromDecisionByAge implements Value<Integer, Individual>, ConfiguredByFile {

    private DecisionByAgeAndProbas decision;

    @Override
    public Integer getValue(Individual individual) {
        if (individual.getAge() >= decision.getMaximumAge()) {
            return Integer.valueOf(individual.getAge() + 1);
        } else {
            MunicipalitySet region = individual.getHousehold().getMyVillage().getMyRegion();
            Parameters params = region.getParameters();
            final int remainingYears = params.getStartStep() + (params.getNbStep() * params.getStep()) - region.getCurrentYear();            
            final int max = Math.min(decision.getMaximumAge(),  individual.getAge() + remainingYears);
            for (int age = Math.max(decision.getMinimumAge(), individual.getAge()); age <= max; age++) {
                if (decision.getValue(individual, age)) {
                    return age;
                }
            }
            return decision.getMaximumAge();
        }
    }

    @Override
    public void init(Application application) throws BadDataException {
        decision.init(application);
    }

    @Override
    public String getFilename() {
        return decision.getFilename();
    }
}
