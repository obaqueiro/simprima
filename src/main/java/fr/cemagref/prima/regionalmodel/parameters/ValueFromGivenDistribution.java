/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import org.openide.util.lookup.ServiceProvider;

/**
 * Parametrize a value with a given normalized cumulative distribution.
 * Exemple of configuration:
 * <pre>  &lt;probas>
 *    &lt;double>0.4&lt;/double>
 *    &lt;double>0.6&lt;/double>
 *    &lt;double>1&lt;/double>
 *  &lt;/probas>
 *  &lt;values>
 *    &lt;int>0&lt;/int>
 *    &lt;int>1&lt;/int>
 *    &lt;int>2&lt;/int>
 *  &lt;/values></pre>
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class ValueFromGivenDistribution<N extends Number, O extends Object> implements Value<N, O> {

    private transient Application application;
    private double[] probas;
    private N[] values;

    @Override
    public N getValue(O object) {
        double d = application.getRandom().nextDouble();
        int idx = -1;
        double lowBound = 0;
        while (d > lowBound) {
            idx++;
            lowBound = probas[idx];
        }
        return values[idx];
    }

    @Override
    public void init(Application application) throws BadDataException {
        this.application = application;
    }
}
