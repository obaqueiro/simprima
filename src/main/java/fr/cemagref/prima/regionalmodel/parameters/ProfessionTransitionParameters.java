/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import org.openide.util.lookup.ServiceProvider;

/**
 * This parameter will return a new profession depending on the actual profession
 * and possibly the age of an individual.
 * 
 * <p>The input file should be a csv file with for each row:
 * <ul><li>the age: if empty, the data will be independent of the age
 * <li>the actual profession
 * <li>a list of probabilities, one for each targeted profession
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class ProfessionTransitionParameters implements Value<Integer, Individual>, ConfiguredByFile {

    private String filename;
    private Character separator;
    protected transient SortedMap<Integer, List<Double[]>> dataByAge;

    @Override
    public void init(Application application) throws BadDataException {
        dataByAge = new TreeMap<Integer, List<Double[]>>();
        String[] data;
        try {
            CSVReader reader = CSV.getReader(application.getFileFromRelativePath(filename), separator);
            while ((data = CSV.readLine(reader)) != null) {
                int age = data[0].length() == 0 ? 0 : Integer.parseInt(data[0]);
                List<Double[]> probas = dataByAge.get(age);
                if (probas == null) {
                    probas = new ArrayList<Double[]>();
                    dataByAge.put(age, probas);
                }
                int targetProf = Integer.parseInt(data[1]);
                while (targetProf >= probas.size()) {
                    probas.add(null);
                }
                probas.set(targetProf, Utils.parseDoubleArray(data, 2));
            }
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("Error while loading " + filename, ex);
        }
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public Integer getValue(Individual ind) {
        Double[] probas = Utils.getValueFromLowerBound(dataByAge, ind).get(ind.getProfession());
        Double[] probas2 = new Double[probas.length] ;
        // Attention, the probas for already employed people don't sum to 1, the difference is the proba not to search another job
        double totProbas = 0; // that is the global proba to search
        for (int i = 0; i < probas.length; i++) {
            totProbas = totProbas + probas[i];
        }
        if (totProbas >= 0.999) { // that the case for unemployed, I use higher or equal to solve the rounding problems of the parameter value (same idea for 0.999
            return ind.getHousehold().getRandom().nextIndexWithDistribution(probas);
        } else {
            double rand = ind.getHousehold().getRandom().nextDouble();
            if (rand > totProbas) {
                return -1;
            } else {
                for (int i = 0; i < probas.length; i++) {
                    probas2[i] = probas[i] / totProbas;
                }
                return ind.getHousehold().getRandom().nextIndexWithDistribution(probas2);
            }
        }
    }
}
