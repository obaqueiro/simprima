/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import fr.cemagref.prima.regionalmodel.Individual;
import java.util.Map;
import java.util.SortedMap;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Utils {

    public static Double[] parseDoubleArray(String[] data) {
        return parseDoubleArray(data, 0);
    }

    public static Double[] parseDoubleArray(String[] data, int start) {
        return parseDoubleArray(data, start, data.length - 1);
    }

    public static Double[] parseDoubleArray(String[] data, int start, int end) {
        Double[] array = new Double[end - start + 1];
        for (int i = start; i <= end; i++) {
            array[i - start] = Double.parseDouble(data[i]);
        }
        return array;
    }

    public static Integer[] parseIntegerArray(String[] data) {
        return parseIntegerArray(data, 0);
    }

    public static Integer[] parseIntegerArray(String[] data, int start) {
        return parseIntegerArray(data, start, data.length - 1);
    }

    public static Integer[] parseIntegerArray(String[] data, int start, int end) {
        Integer[] array = new Integer[end - start + 1];
        for (int i = start; i <= end; i++) {
            array[i - start] = Integer.parseInt(data[i]);
        }
        return array;
    }
    
    /**
     * Return the value in a map where items contains values for age range represented
     * by their lower bound. The map should be sorted by its keys, so by the ages. The value
     * corresponding to the highest age that is less than the individual age will be returned.
     * 
     * @param <T>
     * @param map
     * @param ind
     * @throws ArrayIndexOutOfBoundsException if the index is less than the first lower bound
     * @return
     */
    public static <T> T getValueFromLowerBound(SortedMap<Integer, T> map, Individual ind) {
        return getValueFromLowerBound(map, ind.getAge());
    }

    /**
     * Return the value corresponding to the highest key that is less than the given index.
     * 
     * @param map A map containing values associated to a lower bound
     * @throws ArrayIndexOutOfBoundsException if the index is less than the first lower bound
     * @return 
     */
    public static <T> T getValueFromLowerBound(SortedMap<Integer, T> map, int index) {
        T value = null;
        if (map.firstKey() > index) {
            throw new ArrayIndexOutOfBoundsException("Given index is less than the first bound.\n Given index: " + index + "\n First bound: " + map.firstKey());
        }
        value = map.get(index);
        if (value == null) {
            for (Map.Entry<Integer, T> item : map.entrySet()) {
                if (item.getKey() > index) {
                    break;
                }
                value = item.getValue();
            }
        }
        return value;
    }
    
    /**
     * Return the value corresponding to the lowest key that is greater than the given index.
     * 
     * @param map A map containing values associated to a lower bound
     * @throws ArrayIndexOutOfBoundsException if the index is less than the first lower bound
     * @return 
     */
    public static <T> T getValueFromUpperBound(SortedMap<Integer, T> map, int index) {
        T value = null;
        if (map.lastKey() < index) {
            throw new ArrayIndexOutOfBoundsException("Given index is greater than the last bound.\n Given index: " + index + "\n Last bound: " + map.lastKey());
        }
        value = map.get(index);
        if (value == null) {
            for (Map.Entry<Integer, T> item : map.entrySet()) {
                value = item.getValue();
                if (index <= item.getKey()) {
                    break;
                }
            }
        }
        return value;
    }
}
