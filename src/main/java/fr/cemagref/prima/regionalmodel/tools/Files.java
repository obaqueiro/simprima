/*
 * Copyright (C) 2012 Baqueiro
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Baqueiro
 */
public class Files {
 
    /**
     * Read one line of the specified file skipping any comment or empty lines
     * @param file Input file to read
     * @return a string containing the read characters or an empty string if EOF 
     * has been reached     
     */
    public static String readLine(final BufferedReader reader) throws IOException {
          String line=null;
            do {                
                line = reader.readLine();                
                if (line==null) {break;}
                line = line.trim();               
            } while (line.charAt(0) =='#' || line.length()==0);
           return line;
    }
}
