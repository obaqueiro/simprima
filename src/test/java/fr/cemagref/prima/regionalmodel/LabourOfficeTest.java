/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.LabourOffice.JobProposition;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import org.apache.commons.lang.ArrayUtils;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class LabourOfficeTest {

    private String notFoundMessage = "Municipality not found in the result";
    private Municipality municipalityA;
    private Municipality municipalityB;
    private Municipality municipalityC;
    private Municipality municipalityD;
    private MunicipalitySet region;
    private LabourOffice labourOffice;

    public LabourOfficeTest() {
    }

    @Before
    public void setUp() throws Exception {
        // init a region with
        //  - 3 activities
        //  - 2 professions
        //  - all patterns for one or two activities
        //  - 4 municipalities with some availabilities
        SortedMap<Activity, Integer> act = new TreeMap<Activity, Integer> ();        
        act.put(new Activity(0,0),5);
        act.put(new Activity(0,1), 0);
        act.put(new Activity(1,0), 0);
        
        municipalityA = new Municipality("A", act);
        
        act = new TreeMap<Activity, Integer> ();        
        act.put(new Activity(0,0),0);
        act.put(new Activity(0,1), 5);
        act.put(new Activity(1,0), 0);        
        municipalityB = new Municipality("B", act);
        
        act = new TreeMap<Activity, Integer> ();        
        act.put(new Activity(0,0),5);
        act.put(new Activity(0,1), 0);
        act.put(new Activity(1,0), 5);        
        municipalityC = new Municipality("C", act);
        
        act = new TreeMap<Activity, Integer> ();        
        act.put(new Activity(0,0),0);
        act.put(new Activity(0,1), 5);
        act.put(new Activity(1,0), 5);        
        municipalityD = new Municipality("D", act);
        
        municipalityA.setProximityJob(municipalityB, municipalityC, municipalityD);
        municipalityA.setProximityJob(municipalityB, municipalityC, municipalityD);
        municipalityB.setProximityJob(municipalityA, municipalityC, municipalityD);
        municipalityC.setProximityJob(municipalityA, municipalityB, municipalityD);
        municipalityD.setProximityJob(municipalityA, municipalityB, municipalityC);
        region = RegionTest.buildARegion(municipalityA, municipalityB, municipalityC, municipalityD);
        labourOffice = region.getLabourOffice();
    }

    /**
     * Test of getMunicipalitiesForJob method, of class LabourOffice.
     */
    @Test
    public void testGetMunicipalitiesForJob() {
        Activity act1 = new Activity(0,0);
        Activity act2 = new Activity(0,1);
        Activity act3 = new Activity(1,0);
        
        JobProposition result = labourOffice.getMunicipalitiesForJob(act1);
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityA));
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityC));
        result = labourOffice.getMunicipalitiesForJob(act2);
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityB));
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityD));
        result = labourOffice.getMunicipalitiesForJob(act3);
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityC));
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityD));


    }

    /**
     * Test of getMunicipalitiesForActivityPattern method, of class LabourOffice.
     */
    @Test
    public void testGetMunicipalitiesForActivity() {
        Activity act = new Activity(0,0);
        List<JobProposition> result = labourOffice.getMunicipalitiesForActivity(act);
        assertEquals(
                "2 job propositions should have been found", 2, result.size());
        assertEquals(
                "1 municipality should be returned in each job propositions"
                + " (pattern contains 1 job)", 1, result.get(0).getJobsLocations().length);
        assertEquals(
                "1 municipality should be returned in each job propositions"
                + " (pattern contains 1 job)", 1, result.get(1).getJobsLocations().length);
        assertEquals(
                notFoundMessage, municipalityA, result.get(0).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityC, result.get(1).getJobsLocations()[0]);


    }

    /**
     * Test of getMunicipalitiesForProfession method, of class LabourOffice.
     */
    @Test
    public void testGetMunicipalitiesForProfession() {
        List<JobProposition> result = labourOffice.getMunicipalitiesForProfession(0);
        assertEquals(
                "4 job propositions should have been found", 4, result.size());
        assertEquals(
                notFoundMessage, municipalityA, result.get(0).getJobsLocations()[0]);
        // C is found two times, for the two different patterns that match the profession
        assertEquals(
                notFoundMessage, municipalityC, result.get(1).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityC, result.get(2).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityD, result.get(3).getJobsLocations()[0]);


    }

    /**
     * Test of getLocalMunicipalitiesForActivityPattern method, of class LabourOffice.
     */
    @Test
    public void testGetLocalMunicipalitiesForActivityPattern() {
        Activity pattern = new Activity(0,0);
        List<JobProposition> result = labourOffice.getLocalMunicipalitiesForActivity(pattern, municipalityA);
        assertEquals(
                "2 job propositions should have been found", 2, result.size());
        assertArrayEquals(
                notFoundMessage,
                new Municipality[]{municipalityA},
                result.get(0).getJobsLocations());
        assertArrayEquals(
                notFoundMessage,
                new Municipality[]{municipalityC},
                result.get(1).getJobsLocations());
    }

    /**
     * Test of getLocalMunicipalitiesForProfession method, of class LabourOffice.
     */
    @Test
    public void testGetLocalMunicipalitiesForProfession() {
        List<JobProposition> result = labourOffice.getLocalMunicipalitiesForProfession(municipalityA, 0);
        assertEquals(
                "4 job propositions should have been found", 4, result.size());
        assertEquals(
                notFoundMessage, municipalityA, result.get(0).getJobsLocations()[0]);
        // C is found two times, for the two different patterns that match the profession
        assertEquals(
                notFoundMessage, municipalityC, result.get(1).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityC, result.get(2).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityD, result.get(3).getJobsLocations()[0]);

    }
   
}
