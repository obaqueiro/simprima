/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.util.SortedMap;
import fr.cemagref.prima.regionalmodel.Individual;
import java.util.TreeMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class DecisionByAgeAndProbasTest {

    /**
     * Test of getValue method, of class DecisionByAgeAndProbas.
     */
    @Test
    public void testGetValue() throws BadDataException, ProcessingException {
        SortedMap<Integer, Double> map = new TreeMap<Integer, Double>();
        map.put(10, 0.01);
        map.put(20, 0.99);
        DecisionByAgeAndProbas instance = DecisionByAgeAndProbas.createInstance(map);
        Application application = new Application();
        instance.setApplication(application);
        application.getRandom().setSeed(new long[]{12345, 12345, 12345, 12345, 12345, 12345});
        Individual ind = new Individual(true, (byte) 10, Individual.Status.WORKER, null);
        assertFalse(instance.getValue(ind));
        ind = new Individual(true, (byte) 19, Individual.Status.WORKER, null);
        assertFalse(instance.getValue(ind));
        ind = new Individual(true, (byte) 20, Individual.Status.WORKER, null);
        assertTrue(instance.getValue(ind));
        ind = new Individual(true, (byte) 90, Individual.Status.WORKER, null);
        assertTrue(instance.getValue(ind));
    }
}
