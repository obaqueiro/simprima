/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import fr.cemagref.prima.regionalmodel.*;
import fr.cemagref.prima.regionalmodel.scenarios.period.PeriodDistributionMethod;
import fr.cemagref.prima.regionalmodel.scenarios.period.PartialTableUpdater;
import fr.cemagref.prima.regionalmodel.scenarios.period.ScenarioPeriod;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.io.File;
import java.net.URL;
import java.util.List;
import fr.cemagref.prima.regionalmodel.scenarios.period.distmethod.UniformDistributed;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class ScenarioPeriodTest {

    private ScenarioPeriod scenarioPeriod;
    private MunicipalitySet region;
    private Municipality mun1;
    private Municipality mun2;

    @Before
    public void setUp() throws Exception {
        URL inputURL = this.getClass().getResource("A");

        Application application = new Application(new File(inputURL.getFile()).getParentFile(), null);
        application.getRandom().setSeed(new long[]{42, 42, 42, 42, 42, 42});

        mun1 = new Municipality("A", TestUtilities.generateActivityMap(new int[]{0, 0, 0}));
        mun2 = new Municipality("B", TestUtilities.generateActivityMap(new int[]{0, 0, 0}));
        region = RegionTest.buildARegion(mun1, mun2);
        mun1.initVillageResidence(new int[]{5, 5, 5, 5});
        mun2.initVillageResidence(new int[]{7, 7, 7, 7});
        RegionTest.getParameters(region).setStartStep(2000);
        region.setMyApplication(application);

        PartialTableUpdater tableUpdater = new PartialTableUpdater("housingTest.csv", "updateAvailableResidence");
        List<PartialTableUpdater> scenarios = new ArrayList<PartialTableUpdater>();
        scenarios.add(tableUpdater);
        PeriodDistributionMethod method = new UniformDistributed();
        scenarioPeriod = new ScenarioPeriod(2000, 2001, scenarios, method);
    }

    /**
     * Test of init method, of class ScenarioPeriod.
     */
    @Test
    public void testInit() throws Exception {
        scenarioPeriod.init(region);
    }

    /**
     * Test of step method, of class ScenarioPeriod.
     */
    @Test
    public void testStep() throws Exception {
        scenarioPeriod.init(region);
        scenarioPeriod.step(region, 2000);
        scenarioPeriod.step(region, 2001);
        int[] t1 = {3, 3, 5, 5};
        int[] t2 = {7, 7, 9, 9};
        for (int i = 0; i < 4; i++) {
            assertEquals(t1[i], mun1.getResidenceOffer(i));
            assertEquals(t2[i], mun2.getResidenceOffer(i));
        }
    }
}
