/*
 * Copyright (C) 2012 IAMO
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Utility functions used for tests
 * @author Baqueiro
 * 2012.02.03
 */
public class TestUtilities {
        /**
     * Creates a map with activities to use for Municipality initialization from an 
     * array of activity availability.
     * @param activities Array of activities availability
     * @return  Map containing new activities with the provided availability
     */
    static public SortedMap<Activity,Integer> generateActivityMap(int[] activities){
        SortedMap<Activity,Integer> map = new TreeMap<Activity, Integer> ();
        int spc=0;
        for (int act : activities){
            map.put(new Activity(0,spc), act);
            spc++;
        }
        return map;
    }
}
