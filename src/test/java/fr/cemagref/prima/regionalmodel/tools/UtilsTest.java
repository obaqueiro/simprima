/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import fr.cemagref.prima.regionalmodel.Individual;
import java.util.SortedMap;
import java.util.TreeMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class UtilsTest {

    @Test
    public void testParseDoubleArray_3args() {
        String[] data = {"0", "1", "2", "3", "4"};
        Double[] expResult = {1.0, 2.0, 3.0};
        Double[] result = Utils.parseDoubleArray(data, 1, 3);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testParseIntegerArray_3args() {
        String[] data = {"0", "1", "2", "3", "4"};
        Integer[] expResult = {1, 2, 3};
        Integer[] result = Utils.parseIntegerArray(data, 1, 3);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testGetValueFromLowerBound_2args() {
        SortedMap<Integer, String> map = new TreeMap<Integer, String>();
        map.put(10, "10");
        map.put(20, "20");
        assertEquals("10", Utils.getValueFromLowerBound(map, 10));
        assertEquals("10", Utils.getValueFromLowerBound(map, 19));
        assertEquals("20", Utils.getValueFromLowerBound(map, 20));
        assertEquals("20", Utils.getValueFromLowerBound(map, 90));
        boolean throwed = false;
        try {
            Utils.getValueFromLowerBound(map, 0);
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace(System.err);
            throwed = true;
        }
        assertTrue("An ArrayIndexOutOfBoundsException was excepted.", throwed);
    }

    @Test
    public void testGetValueFromUpperBound_2args() {
        SortedMap<Integer, String> map = new TreeMap<Integer, String>();
        map.put(10, "10");
        map.put(20, "20");
        assertEquals("10", Utils.getValueFromUpperBound(map, 1));
        assertEquals("10", Utils.getValueFromUpperBound(map, 10));
        assertEquals("20", Utils.getValueFromUpperBound(map, 19));
        assertEquals("20", Utils.getValueFromUpperBound(map, 20));
        boolean throwed = false;
        try {
            Utils.getValueFromUpperBound(map, 30);
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace(System.err);
            throwed = true;
        }
        assertTrue("An ArrayIndexOutOfBoundsExceptionwas excepted.", throwed);
    }

}
