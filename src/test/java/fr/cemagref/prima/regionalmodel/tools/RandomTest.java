/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class RandomTest {

    private Random random;

    @Before
    public void setUp() throws ProcessingException {
        random = new Random();
        random.setSeed(new long[]{12345, 12345, 12345, 12345, 12345, 12345});
    }

    /**
     * Test of randomList method, of class Random.
     */
    @Test
    public void testRandomList() {
        int length = 10;
        int[] result = random.randomList(length);
        assertEquals(length, result.length);
        Arrays.sort(result);
        int[] expSortedResult = new int[length];
        for (int i = 0; i < expSortedResult.length; i++) {
            expSortedResult[i] = i;
        }
        assertArrayEquals(expSortedResult, result);
    }

    /**
     * Test of nextInt method, of class Random.
     */
    @Test
    public void testNextInt() {
        for (int i = 0; i < 100; i++) {
            int result = random.nextInt(0, 1);
            assertTrue(result == 0 || result == 1);
            assertEquals(5, random.nextInt(5, 5));
        }
    }

    /**
     * Test of nextDouble method, of class Random.
     */
    @Test
    public void testNextDouble() {
        for (int i = 0; i < 100; i++) {
            double result = random.nextDouble();
            assertTrue(result > 0 && result < 1);
        }
    }

    /**
     * Test of nextIndexWithDistribution method, of class Random.
     */
    @Test
    public void testNextIndexWithDistribution_floatArr() {
        float[] distribution = {0.01f, 0.99f};
        int[] count = {0, 0};
        for (int i = 0; i < 100; i++) {
            count[random.nextIndexWithDistribution(distribution)]++;
        }
        assertTrue(count[1] > count[0]);
        assertTrue(count[1] != 0);
    }

    /**
     * Test of nextIndexWithDistribution method, of class Random.
     */
    @Test
    public void testNextIndexWithDistribution_DoubleArr() {
        Double[] distribution = {0.01, 0.99};
        int[] count = {0, 0};
        for (int i = 0; i < 100; i++) {
            count[random.nextIndexWithDistribution(distribution)]++;
        }
        assertTrue(count[1] > count[0]);
        assertTrue(count[1] != 0);
    }

    /**
     * Test of nextIndexWithDistribution method, of class Random.
     */
    @Test
    public void testNextIndexWithDistribution_List() {
        List<Double> distribution = Arrays.asList(0.01, 0.99);
        int[] count = {0, 0};
        for (int i = 0; i < 100; i++) {
            count[random.nextIndexWithDistribution(distribution)]++;
        }
        assertTrue(count[1] > count[0]);
        assertTrue(count[1] != 0);
    }

    /**
     * Test of nextIndexWithDistribution method, of class Random.
     */
    @Test
    public void testNextIndexWithDistribution_doubleArr() {
        double[] distribution = {0.01, 0.99};
        int[] count = {0, 0};
        for (int i = 0; i < 100; i++) {
            count[random.nextIndexWithDistribution(distribution)]++;
        }
        assertTrue(count[1] > count[0]);
        assertTrue(count[1] != 0);
    }

    @Test
    public void testNextIndexWithDistribution_CollectionIterator() {
        double[] distribution = {0.0, 0.0, 0.0, 0.0};
        for (int i = 0; i < 100; i++) {
            assertEquals(-1, random.nextIndexWithDistribution(distribution, DoubleCollectionIterator.fromDoublePrimitive));
        }
    }

    @Test
    public void testNextMapObjectWithDistributionInKeys() {
        SortedMap<Double, String> map = new TreeMap<Double, String>();
        map.put(0.01, "first");
        map.put(0.99, "second");
        int countFirst=0, countSecond=0;
        for (int i =0;i<100;i++) {
            String value = random.nextMapObjectWithDistributionInKeys(map);
            if (value.equals("first")) {
                countFirst++;
            } else {
                countSecond++;
            }
        }
        assertTrue(countSecond > countFirst);
        assertTrue(countSecond > 0);
    }

    /**
     * Test of shuffle method, of class Random.
     */
    @Test
    public void testShuffle() {
        List list = Arrays.asList(1, 2, 3, 4, 5);
        random.shuffle(list);
        assertEquals(5, list.size());
        list = Arrays.asList(1);
        random.shuffle(list);
        assertEquals(1, list.get(0));
        assertEquals(1, list.size());
    }

    /**
     * Test of pickElements method, of class Random.
     */
    @Test
    public void testPickElements() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        int nb = 5;
        List<Integer> result = random.pickElements(list, nb);
        assertEquals(nb, result.size());
    }
}
