#!/bin/sh
[ $# -ne 1 ] && { echo "Usage: $0 <directories of explorations input files>"; } || {
    [ ! -d $1 ] && { echo "This directory doesn't exist!"; } || {
        for f in $1/*; do
            echo "cd ${PWD}; /usr/local/java/jdk1.6.0_18/bin/java -cp target/prima-regional-model-1.0-SNAPSHOT-jar-with-dependencies.jar fr.cemagref.prima.regionalmodel.Exploration ${f}" | qsub -l nodes=1:ppn=1 -m n -N ${f};
        done;
    }
}

