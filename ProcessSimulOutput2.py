#!/usr/bin/env python
from xml.etree import ElementTree as ET
import sys

municipalities = ["'15090005" ,"'15090245" ,"'15090515","'15090"] 
inputFactors = {}
def main():
	# read  output data from simulation
	outFile = open("output.txt","w")
	
	#read variables from the target data\
	targetFile = open("targetData-Employment.txt")
	targetValues=[]
	while True:
		line = targetFile.readline()
		if line=='':
			break;
		if line[0]=="#":
			continue
		line = line.split("\t")
		targetValues.append(line)
		

	# write file header 
	outFile.write("#Simulation data obtained from Altmark Model adaptation\n")
	outFile.write("Region\tVariable\tYear\tValue\tType\n")
	
	
	# Process PopulationAgeDistribution Files
	# Look for all the "Age" and "Population" data in the target values, make a list of the years
	popSet = [i for i in targetValues if ("Age" in i[1]) or ("Population" in i[1])]
	values = set(map(lambda x:x[2],popSet))
	newList = [[y for y in popSet if y[2] == x] for x in values]
	for subset in newList:
		fileName = "dataOut/HohenbergExploration/average/average-%s-PopulationAgeDistribution-.csv"%subset[0][2]
		inFile = open (fileName)
		# traverse input file 
		while True:
			line = inFile.readline()
			if line=='':
				break
			line = line.split(",")
			for item in subset:
				# if the data item corresponds to this municiaplity then set the corresponding value  from input file
				if line[1][1:] == item[0]:
					# Map input file columns to each age value
					itemMap = {"Age0":3, "Age6":4, "Age10":5, "Age18":6,"Age25":7,"Age45":8,"Age65":9,"Population":10}
					# replace value with input data value
					item[3] = line[itemMap[item[1]]]
					outFile.write("\t".join(item))
		inFile.close()
			
	
	# Process BirthDateCommuting Files
	# Look for all the "In-Migration","Out-Migration", "Births", "Deaths" data in the target values, make a list of the years
	popSet=[]
	for i in targetValues:
		if i[1] == "Out-Migration" or i[1] == "In-Migration" or i[1] == "Births" or i[1]=="Deaths":
			popSet.append(i)
	values = set(map(lambda x:x[2],popSet))
	newList = [[y for y in popSet if y[2] == x] for x in values]
	for subset in newList:
		fileName = "dataOut/HohenbergExploration/average/average-%s-BirthsDeathsCommuting-.csv"%subset[0][2]
		inFile = open (fileName)
		# traverse input file 
		while True:
			line = inFile.readline()
			if line=='':
				break
			line = line.split(",")
			for item in subset:
				# if the data item corresponds to this municiaplity then set the corresponding value  from input file
				if line[1][1:] == item[0]:
					# Map input file columns to each age value (instead of switch)
					itemMap = {"Births":3, "Deaths":4, "In-Migration":5, "Out-Migration":6}
					# replace value with input data value
					item[3] = line[itemMap[item[1]]]
					outFile.write("\t".join(item))
		inFile.close()
	
	# Process Employment Files
	# Look for all the "Employed*","Unemployed*" data in the target values, make a list of the years
	popSet=[]
	popSet = [i for i in targetValues if ("Employed" in i[1]) or ("Unemployed" in i[1])]
	values = set(map(lambda x:x[2],popSet))
	newList = [[y for y in popSet if y[2] == x] for x in values]
	for subset in newList:
		fileName = "dataOut/HohenbergExploration/average/average-%s-Employment-.csv"%subset[0][2]
		inFile = open (fileName)
		# traverse input file 
		while True:
			line = inFile.readline()
			if line=='':
				break
			line = line.split(",")
			for item in subset:
				# if the data item corresponds to this municiaplity then set the corresponding value  from input file
				if line[1][1:] == item[0]:
					# Map input file columns to each age value (instead of switch)
					itemMap = {"Unemployed15-25":6, "Employed15-25":5, 
					"Unemployed25-50":8, "Employed25-50":7, "Unemployed50-65":10,"Employed50-65":9}
					# replace value with input data value
					item[3] = line[itemMap[item[1]]]
					outFile.write("\t".join(item))
		inFile.close()
	
	
	# Process HouseholdStructure Files
	# Look for all the "Household*" data in the target values, make a list of the years
	popSet=[]
	popSet = [i for i in targetValues if ("Household" in i[1])]
	values = set(map(lambda x:x[2],popSet))
	newList = [[y for y in popSet if y[2] == x] for x in values]
	for subset in newList:
		fileName = "dataOut/HohenbergExploration/average/average-%s-HouseholdStructure-.csv"%subset[0][2]
		inFile = open (fileName)
		# Go to line Number 13 in file (containing the data about household percentages)
		i=0
		while i< 13:
			line = inFile.readline()
			i = i +1
			line = line.split(",")
		
		for item in subset:
			# if the data item corresponds to this municiaplity then set the corresponding value  from input file
			# Map input file columns to each age value (instead of switch)
			itemMap = {"Household1Person":2, "Household2Person":3,"Household3Person":4 ,
			"Household4Person":5, "Household5Person":6}
			# replace value with input data value
			item[3] = line[itemMap[item[1]]]
			outFile.write("\t".join(item))
		inFile.close()
		
		
	# Process Sector of Activity Files
	# Look for all the "Employed*","Unemployed*" data in the target values, make a list of the years
	popSet=[]
	popSet = [i for i in targetValues if ("SoA" in i[1])]
	values = set(map(lambda x:x[2],popSet))
	newList = [[y for y in popSet if y[2] == x] for x in values]
	for subset in newList:
		fileName = "dataOut/HohenbergExploration/average/average-%s-SectorOfActivity-.csv"%subset[0][2]
		inFile = open (fileName)
		# traverse input file 
		while True:
			line = inFile.readline()
			if line=='':
				break
			line = line.split(",")
			for item in subset:
				# if the data item corresponds to this municiaplity then set the corresponding value  from input file
				if line[1][1:] == item[0]:
					# Map input file columns to each age value (instead of switch)
					itemMap = {"SoA1":3, "SoA2":4,"SoA3":5, "SoA4":6, "SoA5":7,"SoA6":8,"SoA7":9}
					# replace value with input data value
					item[3] = line[itemMap[item[1]]]
					outFile.write("\t".join(item))
		inFile.close()

	# Process WorkPlace 
	# Look for all the "Workplace" data in the target values, make a list of the years
	popSet=[]
	popSet = [i for i in targetValues if ("Workplace" in i[1])]
	values = set(map(lambda x:x[2],popSet))
	newList = [[y for y in popSet if y[2] == x] for x in values]
	for subset in newList:
		fileName = "dataOut/HohenbergExploration/average/average-%s-WorkingPlace-.csv"%subset[0][2]
		inFile = open (fileName)
		# traverse input file 
		while True:
			line = inFile.readline()
			if line=='':
				break
			line = line.split(",")
			for item in subset:
				# if the data item corresponds to this municiaplity then set the corresponding value  from input file
				if line[1][1:] == item[0]:
					# Map input file columns to each age value (instead of switch)
					itemMap = {"Workplace":3}
					# replace value with input data value
					item[3] = line[itemMap[item[1]]]
					outFile.write("\t".join(item))
	inFile.close()

	outFile.close()
	
	
if __name__ == "__main__":
	main()
	
	